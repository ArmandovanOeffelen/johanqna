<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRangeIndicatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('range_indicator', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('range_color_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('range_color_id')->references('id')->on('range_color');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('range_indicator');
    }
}
