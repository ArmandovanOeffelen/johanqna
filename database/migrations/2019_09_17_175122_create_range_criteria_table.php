<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRangeCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('range_criteria', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('range_criteria_collection_id');
            $table->string('type');
            $table->string('value');
            $table->timestamps();

            $table->foreign('range_criteria_collection_id')->references('id')->on('range_criteria_collection');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('range_criteria');
    }
}
