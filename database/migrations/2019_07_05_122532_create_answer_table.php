<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_answered', function (Blueprint $table) {
            $table->increments('id');
            $table->string('interviewee_id');
            $table->unsignedInteger('question_id');
            $table->integer('answer');
            $table->tinyInteger('is_exported');
            $table->timestamps();

            $table->foreign('question_id')->references('id')->on('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_answered');
    }
}
