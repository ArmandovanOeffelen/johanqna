<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_value', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('result_id');
            $table->string('column_name');
            $table->string('value');
            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('result');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_value');
    }
}
