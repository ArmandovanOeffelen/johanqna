<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneCodeToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('code')->after('phonenumber')->nullable();
            $table->string('country_code')->after('phonenumber')->nullable();
            $table->tinyInteger('activated')->default(0);
            $table->tinyInteger('logged_success')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('country_code');
            $table->dropColumn('activated');
            $table->dropColumn('logged_success');
        });
    }
}
