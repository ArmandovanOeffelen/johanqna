<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEmployeeInviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_employee_invite', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id')->nullable();;
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('invite_token');
            $table->string('invite_link')->nullable();
            $table->tinyInteger('invite_accepted')->default(0);
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_employee_invite');
    }
}
