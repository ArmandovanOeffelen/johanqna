<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRangeCriteriaCollectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('range_criteria_collection', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('range_id');
            $table->unsignedInteger('range_indicator_id');
            $table->timestamps();

            $table->foreign('range_id')->references('id')->on('range');
            $table->foreign('range_indicator_id')->references('id')->on('range_indicator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('range_criteria_collection');
    }
}
