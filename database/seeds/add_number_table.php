<?php

use Illuminate\Database\Seeder;

class add_number_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'number' => 'adb54ee8',
            'balance' => 0.00,
        ]);
    }
}
