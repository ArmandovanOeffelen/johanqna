<?php

use Illuminate\Database\Seeder;

class add_company extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('company')->insert([
            'name' => 'Admin Company',
            'company_code' => 'ADC',
            'email' => 'dev@rmnddesign.com',

        ]);

        DB::table('company_logo')->insert([
            'company_id' => '1',
        ]);


    }
}
