<?php

use Illuminate\Database\Seeder;

class add_range_categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colorRanges = [
            [
                //Bad
                'name' => 'Fysieke vitaliteit',
            ],
            [
                'name' => 'Mentale vitaliteit',
            ],
            [
                'name' => 'Emotionele Vitaliteit',
            ]
        ];

        foreach($colorRanges as $cr){
            \App\Model\RangeCategory::create($cr);
        }
    }
}
