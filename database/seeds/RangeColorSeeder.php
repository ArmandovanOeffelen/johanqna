<?php

use Illuminate\Database\Seeder;

class RangeColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $colorRanges = [
                [
                    //Bad
                    'color_name' => 'Bad',
                    'color_code' => '#d51d12',
                ],
                [
                    //Semi-bad
                    'color_name' => 'Semi-bad',
                    'color_code' => '#e7910d'
                ],
                [
                    //Neutral
                    'color_name' => 'Neutral',
                    'color_code' => '#e7e00d'
                ],
                [
                    //Semi-good
                    'color_name' => 'Semi-good',
                    'color_code' => '#9be70d'
                ],
                [
                    //Good
                    'color_name' => 'Good',
                    'color_code' => '#59e70d'
                ]
            ];

            foreach($colorRanges as $cr){
                \App\Model\RangeColor::create($cr);
            }


    }

}
