    <?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */


    Route::get('/', 'HomeController@index')->middleware(['auth','web']);


    Route::post('/post-survey', 'SurveyController@create');
    Auth::routes();

    Route::get('/home', 'HomeController@index')->middleware('web')->name('home');


    //Manager invites
    Route::get('/signup/{token}/show-form', 'CoachProcessInviteController@registrationForm')->middleware('web');
    Route::post('/signup/{token}/show-form', 'CoachProcessInviteController@register')->name('custom_signup')->middleware('web');
    Route::get('/sign-up/{user}/token/{token}/new-code', 'CoachProcessInviteController@requestNewCode')->name('register_coach_request_new_code')->middleware('web');
    Route::post('/signup/{user}/token/{token}/complete', 'CoachProcessInviteController@completeRegistration')->name('coach_custom_signup_complete')->middleware('web');

    Route::get('/sign-up/{companytoken}/show','OpenInviteController@showForm')->name('show_open_invite_form')->middleware('web');
    Route::post('/sign-up/{companytoken}/show', 'OpenInviteController@register')->name('register_no_invite_custom_employee_form')->middleware('web');
    Route::get('/sign-up/{user}/company/{companytoken}/new-code', 'OpenInviteController@requestNewCode')->name('register_no_invite_request_new_code')->middleware('web');
    Route::post('/sign-up/{user}/company/{company}/complete', 'OpenInviteController@completeRegistration')
        ->name('register_no_invite_complete_custom_employee_form')->middleware('web');

    //Employee invites
    Route::get('/signup/{companytoken}/show-form/{token}', 'CompanyManagerEmployeeInviteController@registrationForm')->middleware('web');
    Route::post('/signup/{companytoken}/show-form/{token}', 'CompanyManagerEmployeeInviteController@register')->name('register_custom_employee_form')->middleware('web');
    Route::get('/sign-up/{user}/company/{companytoken}/token/{token}/new-code', 'CompanyManagerEmployeeInviteController@requestNewCode')->name('register_invite_new_code')->middleware('web');
    Route::post('/signup/{user}/company/{companytoken}/invite/{token}/complete', 'CompanyManagerEmployeeInviteController@completeRegistration')->name('register_complete_custom_employee_form')->middleware('web');

    Route::get('/login/{user}/new-code','SMSHelperController@requestNewCode')->middleware('web')->name('login_request_new_code');
    Route::post('/login/{user}/complete/','SMSHelperController@completeLogin')->middleware(['web'])->name('login_complete_login');

    Route::get('/re-authenticate/show','SMSHelperController@showAuthenticationForm')->middleware(['web'])->name('re_authenticate_show');
    Route::get('/re-authenticate/new-code','SMSHelperController@requestCode')->middleware(['web'])->name('re_authenticate_new_code');
    Route::post('/re-authenticate/complete/','SMSHelperController@reAuthenticate')->middleware(['web'])->name('re_authenticate');






    Route::group(['prefix' => 'survey', 'middleware' => ['auth','web']], function () {

        Route::get('/overview', 'SurveyController@index');

        Route::get('/questions/all', 'SurveyController@indexAllQuestions');
        Route::get('/csv-overview', 'CSVExportController@index');

        Route::get('/test/csv/{csv}/download', 'CSVExportController@downloadCSV')->name('downloadFile');


    });

    Route::group(['prefix' => '/ajax/template/', 'namespace' => 'Ajax\\Template', 'middleware' => ['auth','logged_success','web']], function () {

        Route::delete('/survey/question/{question}/delete', 'SurveyController@delete');
        Route::delete('/survey/csv/{csv}/delete', 'CSVController@deleteOnOverview');
        Route::delete('/survey/csv/overview/{csv}/delete', 'CSVController@delete');

        Route::get('/overview', 'SurveyController@index');
        Route::get('/overview', 'SurveyController@contentOverview');
        Route::get('/overview', 'CSVExportController@showMainDashboardContent');

        Route::get('/csv-overview', 'CSVExportController@index');


        Route::get('/survey/csv/{csv}/download', 'CSVExportController@downloadCSV');

        Route::put('/survey/generate/csv', 'CSVExportController@ExportCSV');

    });


    //Normal Admin middleware routes
    Route::group(['prefix' => '/admin', 'middleware' => ['is_admin','web']], function () {
        Route::get('/dashboard', 'AdminController@index')->name('admin_controller_index');

        //Companies
        Route::get('/all-companies', 'AdminController@showCompanyDashboard')->name('admin_controller_show_companies');

        //Single Company
        Route::get('company/{company}/show-content', 'CompanyController@showSingleCompany')->name('admin_controller_show_company_content');
        Route::get('/company/{company}/employees/overview','CompanyController@showAllEmployeesForCompany')->name('admin_controller_show_all_employees_for_company');

        //Single Company Results
        Route::get('/company/{company}/period/{year}/show-results','AdminController@showCompanyResults')->name('admin_controller_show_results');

        //Meting
        Route::get('/screening/overview', 'AdminController@showScreeningDashboard')->name('admin_controller_show_screening_overview');
        Route::get('/screening/ranges/overview', 'AdminController@showRangeOverview')->name('admin_controller_screening_show_ranges_overview');

        //Rangeconfig
        Route::get('/screening/range-config/dashboard', 'AdminController@showRangeConfigDashboard')->name('admin_controller_show_range_config_dashboard');
        Route::get('/screening/range-config/show-all-configs', 'AdminController@showRangeConfigOverview')->name('admin_controller_show_range_config_overview');
        Route::get('/screening/range-values/show-form', 'RangeController@form')->name('admin_controller_show_range_values_form');
        Route::post('/screening/range-values/post-form', 'RangeController@create')->name('admin_controller_create_range_values');
        Route::get('/test/foo', 'RangeController@criteriaList')->name('test_foo');

        Route::get('/screening/range', 'RangeController@form')->name('admin_range_form_create');
        Route::post('/screening/range', 'RangeController@save')->name('admin_range_form_create_save');
        Route::get('/screening/range/{range}', 'RangeController@form')->name('admin_range_form_update');
        Route::post('/screening/range/{range}', 'RangeController@save')->name('admin_range_form_update_save');

        //RangeConfigTexts
        Route::get('/screening/range-config/texts/overview', 'AdminController@showRangeConfigTextsOverview')->name('admin_controller_show_range_config_texts');

        //Excel
        Route::get('/screening/excel/upload/show-form', 'ResultController@showForm')->name('admin_controller_show_excel_upload_form');
        Route::post('/screening/excel/upload', 'ResultController@importExcel')->name('admin_controller_excel_upload');

        //Excel flagged results
        Route::get('/screening/results/flagged/overview', 'ResultController@showFlaggedResultsOverviewAdmin')->name("admin_controller_flagged_results");

        //User
        Route::get('/dashboard/users/overview', 'AdminController@showUserDasboard')->name('admin_controller_show_user_dashboard');
        Route::get('/dashboard/employee/overview', 'AdminController@showEmployeeDashboard')->name('admin_controller_show_user_employee_dashboard');
        //Coach
        Route::get('/dashboard/coach/overview', 'AdminController@showCoachDashboard')->name('admin_controller_show_all_coaches');

        Route::get('/dashboard/admin/overview','AdminController@showAdminDashboard')->name('admin_controller_show_all_admins');
        //Employee
        Route::get('employee/{user}/overview', 'AdminController@showEmployee')->name('admin_show_employee');
        Route::get('/employee/{user}/result/show/all','AdminController@showAllResults')->name('admin_controller_show_user_results');

        //Enquete
        Route::get('/survey/dashboard', 'SurveyController@showDashboard')->name('admin_show_survey_dashboard');
        Route::get('/survey/questions/overview', 'SurveyController@showAllQuestions')->name('admin_survey_show_all_questions');
        Route::get('survey/form/multiple', 'SurveyController@showMultipleQuestionsForm')->name('admin_controller_show_multiple_questions_form');
        Route::get('survey/form/single', 'SurveyController@showSingleQuestionForm')->name('admin_controller_show_singular_questions_form');
        Route::post('survey/add/multiple', 'SurveyController@createMultipleQuestion');
        Route::post('survey/add/single', 'SurveyController@createSingleQuestion');

    });

    Route::group(['prefix' => '/admin/log', 'middleware' => ['is_admin','web']], function () {

        Route::get('','LogController@index')->name('admin_log_dashboard');
        Route::get('/sms-overview','SmsLogController@index')->name('admin_sms_log_dashboard');
    });

    //Ajax Admin middleware routes
    Route::group(['prefix' => '/ajax/', 'namespace' => 'Ajax', 'middleware' => ['is_admin','web']], function () {
        Route::put('/range-criteria-collection/{criteriaCollection}', 'RangeCriteriaController@saveCollection');
        Route::delete('/range-criteria-collection/{criteriaCollection}', 'RangeCriteriaController@deleteCollection');
        Route::post('/range-criteria-collection', 'RangeCriteriaController@saveCollection');

        Route::get('/range/{range}/get-overlapping-criteria', 'RangeCriteriaController@getOverlapping');
    });

    //Ajax Admin middleware routes
    Route::group(['prefix' => '/ajax/template/', 'namespace' => 'Ajax\\Template', 'middleware' => ['is_admin','web']], function () {

        //Meting
        route::delete('/screening/results/flagged/{resultUnknownEmail}/delete', 'ResultUnkownEmailController@deleteResultUnkownEmail')->name('coach_ajax_delete_unknown_result');

        //Meting flagged results
        Route::get('/screening/results/flagged/overview', 'ResultUnkownEmailController@showFlaggedResultsOverview')->name("coach_ajax_controller_flagged_results");

        //Range indicator
        Route::post('/screening/ranges/create-ranges', 'RangeIndicatorController@create')->name('admin_ajax_create_range');
        Route::get('/screening/ranges/get-colors', 'RangeIndicatorController@showForm')->name('admin_ajax_get_range_colors_and_show_form');
        Route::get('/screening/ranges/overview', 'RangeIndicatorController@showContent')->name('admin_controller_screening_show_ranges_content');
        Route::delete('/screening/ranges/{range}/delete', 'RangeIndicatorController@delete')->name('admin_controller_screening_delete_range');
        Route::get('/screening/ranges/range/{range}/show-update-form', 'RangeIndicatorController@showUpdateForm')->name('admin_ajax_get_range_show_update_form');
        Route::put('/screening/ranges/range/{range}/update', 'RangeIndicatorController@update')->name('admin_ajax_get_range_update');

        //Range
        Route::delete('/screening/range/{range}/delete', 'RangeController@delete')->name('admin_controller_screening_delete_range');

        Route::get('/screening/range-config/texts/overview/range/{range}/show-form', 'RangeTextController@showForm')->name('admin_controller_show_range_config_texts_form');
        Route::get('/screening/range-config/texts/overview/range/{range}/show-update-form', 'RangeTextController@showUpdateForm')->name('admin_controller_show_range_config_texts_form');
        Route::post('/screening/range-config/texts/overview/range/{range}/post-form', 'RangeTextController@create')->name('admin_controller_post_range_config_texts_form');
        Route::post('/screening/range-config/texts/overview/range/{range}/post-update-form', 'RangeTextController@update')->name('admin_controller_post_range_config_texts_form');
        Route::get('/screening/range-config/texts/overview/range/{range}/text/show', 'RangeTextController@showText')->name('admin_controller_post_range_config_texts_form');
        Route::delete('/screening/range-config/texts/overview/range/{range}/text/delete', 'RangeTextController@delete')->name('admin_controller_delete_range_text');

        Route::get('/screening/range-config/tutorial','RangeController@showTutorial')->name('admin_ajax_controller_show_tutorial');

        //Companies
        Route::get('all-companies/create-company/show-form', 'CompanyController@showForm')->name('admin_ajax_companies_show_form');
        Route::get('/all-companies/company/{company}/show-update-form', 'CompanyController@showUpdateForm')->name('admin_ajax_companies_show_update_form');
        Route::post('all-companies/create-company', 'CompanyController@create')->name('admin_ajax_companies_post_form');
        Route::get('all-companies', 'CompanyController@showContent')->name('admin_ajax_companies_all_companies_content');
        Route::delete('/company/{company}/delete', 'CompanyController@delete')->name('admin_ajax_company_delete');
        Route::put('all-companies/company/{company}/update', 'CompanyController@update')->name('admin_ajax_company_upate');

        //Single Company
        Route::get('/company/{company}/company-logo/show-form', 'CompanyLogoController@showForm')->name('admin_ajax_company_get_logo_show_form');
        Route::post('/company/{company}/company-logo/upload', 'CompanyLogoController@uploadLogo')->name('admin_ajax_company_get_logo_upload');
        Route::get('company/{company}/show-content', 'CompanyController@showSingleCompany')->name('admin_ajax_show_company_content');
        Route::get('/company/{company}/employees/show-invite','CompanyController@showInviteForm')->name('admin_ajax_company_show_invite');
        Route::post('/company/{company}/employees/send-invite','CompanyController@sendInvite')->name('admin_ajax_company_send_invite');
        Route::get('/company/{company}/employees/show-content','CompanyController@showAllEmployeesForCompany')->name('admin_controller_show_content_employees');
        Route::delete('/company/{company}/employees/{user}/delete','CompanyController@deleteEmployeeAdmin')->name('admin_controller_delete');

        //coach
        Route::get('user/coach/show-invite-form', 'AdminController@showCoachInviteForm')->name('admin_ajax_show_invite_form');
        Route::get('user/overview/show-invite-form', 'AdminController@showCoachInviteFormUserDashboard')->name('admin_ajax_show_invite_form_user_dashboard');
        Route::post('/user/coach/send-invite', 'AdminController@processInvite')->name('admin_ajax_send_coach_invite');
        Route::post('/user/overview/send-invite', 'AdminController@processInviteUserDashboard')->name('admin_ajax_send_coach_invite_user_dashboard');
        Route::delete('user/coach/{user}/delete', 'AdminController@deleteCoach')->name('admin_ajax_delete_coach');
        Route::delete('users/{user}/delete', 'AdminController@deleteEmployee')->name('admin_ajax_delete_user');
        Route::get('users/overview', 'AdminController@showUsersDashboard')->name('admin_ajax_show_user_overview');
        Route::get('users/user/show-invite-form', 'AdminController@showEmployeeInviteForm')->name('admin_ajax_show_employee_invite_form');
        Route::post('users/user/send-invite', 'AdminController@processEmployeeInvite')->name('admin_ajax_show_employee_invite_form');
        Route::post('users/user/send-invite', 'AdminController@processEmployeeInviteUserDashboard')->name('admin_ajax_show_employee_invite_form');

        //Adminpromotion
        Route::post('users/user/{user}/promote', 'AdminController@promoteUserToAdmin')->name('admin_ajax_promote_user_to_admin');
        Route::post('users/coach/{user}/promote', 'AdminController@promoteCoachToAdmin')->name('admin_ajax_promote_coach_to_admin');
        Route::get('/dashboard/employee/overview', 'AdminController@showEmployeeDashboard')->name('admin_ajax_controller_show_user_employee_dashboard');
    });


    //Normal coach middleware routes
    Route::group(['prefix' => '/coach', 'middleware' => ['is_manager','web']], function () {

        //Dashboard
        Route::get('/dashboard', 'CoachController@dashboard')->name('coach_dashboard_view');

        //Employees
        Route::get('company/{company}/employee/overview', 'CoachController@showCompanyEmployees')->name('coach_company_employee_overview');
        Route::get('employee/{user}/overview', 'CoachController@showEmployee')->name('coach_show_employee');
        Route::get('employees/overview', 'CoachController@showEmployees')->name('coach_show_employees');


        //Invites
        Route::get('company/{company}/employee/invites/show-content', 'ManagerEmployeeController@showAllInvites')->name('manager_employee_invite_overview');

        //singleCompany
        Route::get('/company/{company}', 'CoachController@showCompany')->name('manager_show_company');
        Route::get('/company/{company}/period/{year}/show-results','CoachController@showCompanyResults')->name('manager_controller_show_results');

        //All companies
        Route::get('companies/overview', 'CoachController@showCompanies')->name('manager_show_companies');

        //Meting
        Route::get('/screening/dashboard', 'CoachController@showScreeningDashboard')->name('coach_controller_show_dashboard');



        //Excel resultsn no advice
        Route::get('/results/no-advice','ResultController@showResultsNoAdvice')->name('coach_controller_results_no_advice');

        //Enquete
        Route::get('/survey/dashboard', 'SurveyController@showDashboardForCoach')->name('coach_show_survey_dashboard');
        Route::get('/survey/questions/overview', 'SurveyController@showAllQuestionsForCoach')->name('coach_survey_show_all_questions');
        Route::get('survey/form/multiple', 'SurveyController@showMultipleQuestionsForm')->name('coach_controller_show_multiple_questions_form');
        Route::get('survey/form/single', 'SurveyController@showSingleQuestionForm')->name('coach_controller_show_singular_questions_form');

        Route::post('survey/add/multiple', 'SurveyController@createMultipleQuestion');
        Route::post('survey/add/single', 'SurveyController@createSingleQuestion');


        //results
        Route::get('/employee/{user}/result/show/all','CoachController@showAllResults')->name('coach_controller_show_all_results');


    });

    Route::group(['prefix' => '/ajax/template/coach', 'namespace' => 'Ajax\\Template\\Manager', 'middleware' => ['is_manager','web']], function () {


        //Company
        Route::get('/company/{company}/company-logo/show-form', 'CompanyLogoController@showForm')->name('manager_ajax_company_get_logo_show_form');
        Route::post('/company/{company}/company-logo/upload', 'CompanyLogoController@uploadLogo')->name('manager_ajax_company_get_logo_upload');
        Route::get('/company/{company}/show-update-form', 'CompanyController@showForm')->name('manager_ajax_company_show_update_form');
        Route::get('/company/{company}/show-content', 'CompanyController@showContent')->name('manager_ajax_show_content');
        Route::put('/company/{company}/update-company', 'CompanyController@update')->name('manager_ajax_company_update');


        //Employee
        Route::get('company/{company}/employee/dashboard', 'ManagerEmployeeController@dashboard')->name('manager_employee_dashboard');
        Route::get('company/{company}/employee/invites/show-content', 'ManagerEmployeeController@inviteDashboard')->name('manager_employee_invite_dashboard');
        Route::get('company/{company}/employee/invite/show-form', 'ManagerEmployeeController@showInviteForm')->name('manager_ajax_show_invite_form');
        Route::post('company/{company}/employee/invite/send', 'ManagerEmployeeController@sendInvite')->name('manager_ajax_send_invite_form');
        Route::post('company/{company}/employee/invite/overview/send', 'ManagerEmployeeController@sendInviteFromOverviewPage')->name('manager_ajax_send_invite_form_from_overview');
        Route::delete('company/{company}/employee/invite/{invite}/delete', 'ManagerEmployeeController@deleteInvite')->name('manager_ajax_delete_invite');
        Route::delete('company/{company}/employee/{employee}/delete', 'ManagerEmployeeController@deleteEmployee')->name('manager_ajax_delete_employee');

        //Employee-personal-advice
        Route::get('/employee/{user}/result/{result}/advice/show-form','ResultPersonalAdviceController@showForm')->name('coach_ajax_show_result_advice_form');
        Route::post('/employee/{user}/result/{result}/advice/save','ResultPersonalAdviceController@saveAdvice')->name('coach_ajax_post_result_advice_form');
        Route::put('/employee/{user}/result/{result}/advice/update','ResultPersonalAdviceController@updateAdvice')->name('coach_ajax_post_result_advice_form');
        Route::get('/employee/{user}/result/{result}/advice/show-advice','ResultPersonalAdviceController@showAdvice')->name('coach_ajax_show_advice');

        Route::delete('employees/overview/{user}/delete', 'EmployeeController@delete')->name('coach_ajax_delete_employee');
        Route::get('employees/overview', 'EmployeeController@showEmployees')->name('coach_ajax_show_all_employees');
        Route::get('employee/invite/show-form', 'EmployeeController@showInviteForm')->name('coach_ajax_show_invite_form');
        Route::post('employee/invite/send', 'EmployeeController@sendInvite')->name('coach_ajax_send_invite');


        //Result-No-Personal-advice
        Route::get('result/{result}/advice/show-form','ResultController@showNoAdviceForm')->name('coach_ajax_show_no_advice_form');
        Route::post('result/{result}/advice/post-form','ResultController@createAdvice')->name('coach_ajax_post_no_advice_form');

        //Meting
        route::delete('/screening/results/flagged/{resultUnknownEmail}/delete', 'ResultUnkownEmailController@deleteResultUnkownEmail')->name('coach_ajax_delete_unknown_result');

        //Meting flagged results
        Route::get('/screening/results/flagged/overview', 'ResultUnkownEmailController@showFlaggedResultsOverview')->name("coach_ajax_controller_flagged_results");

        //Enquete
        Route::delete('survey/question/{question}/delete','SurveyController@deleteQuestion')->name('admin_ajax_delete_question');
        Route::get('/survey/questions/overview','SurveyController@index')->name('coach_ajax_show_survey_index');

        //result
        Route::get('results/range/{range}/show-info','EmployeeController@showText');
        Route::get('results/result/{result}/show-advice','EmployeeController@showAdvice');


    });

    Route::group(['prefix' => '/employee', 'middleware' => ['is_default','web']], function () {

        Route::get('dashboard', 'DefaultController@dashboard')->name('default_dashboard');

        //Survey
        Route::get('survey/show-survey', 'DefaultController@showSurvey')->name('default_show_survey');
        Route::post('{user}/survey/post-survey', 'SurveyController@finishSurvey')->name('default_post_survey');

    });

    Route::group(['prefix' => '/employee/appointment', 'middleware' => ['is_default','web']], function () {


        Route::get('/show-content','DefaultController@showAppointment')->name('default_show_appointment_screen');

    });

    Route::group(['prefix' => '/ajax/template/employee/', 'namespace' => 'Ajax\\Template\\Employee', 'middleware' => ['is_default','web']], function () {


        Route::get('results/range/{range}/show-info','EmployeeController@showText');
        Route::get('results/result/{result}/show-advice','EmployeeController@showAdvice');

        Route::get('results/compare/show-form','EmployeeController@showCompareForm')->name('employee_ajax_show_compare_form');
        Route::post('results/compare/post-form','EmployeeController@ShowComparisons')->name('employee_ajax_show_comparinsons');

    });

    Route::group(['prefix' => '/employee','middleware' => ['is_profile_owner','web']], function () {

        Route::get('{id}/results','DefaultController@showResults')->name('default_my_results');

    });

//
//    Route::post('/2fa', function () {
//        return response()->redirectToAction("HomeController@index");
//    })->name('2fa')->middleware('2fa');
//
//
//    Route::get('/re-authenticate', 'HomeController@reauthenticate');

