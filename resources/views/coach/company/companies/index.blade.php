@extends('layout.app')

@section('title', 'Companies overview')

@section('content')
    @include('coach.company.companies.template.index')
@endsection