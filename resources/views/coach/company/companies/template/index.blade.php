@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        {{__('dennis.company_overview')}}
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>{{__('dennis.company_overview')}}</h2>
    </div>
    <div class="col-lg-2 pull-right" style="padding-top:25px;">
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Name
                </td>
                <td>
                    Company code
                </td>
                <td>
                    E-mail
                </td>
                <td class="text-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($companies->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'Companies'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($companies as $index=>$company)
                    <tr>
                        <td>{{ ($companies->perPage() * ($companies->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$company->name}}</td>
                        <td>{{$company->company_code}}</td>
                        <td>{{$company->email}}</td>
                        <td class="text-right">
                            <a class="btn btn-primary" href="{{route('coach_company_employee_overview',['company' => $company])}}">{{__('buttons.check_model',['model' => 'employee'])}}</a>
                            <a class="btn btn-primary" href="{{route('manager_show_company',['company' => $company])}}">{{__('buttons.check_model',['model' => 'company'])}}</a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $companies->links() !!}
        </div>
    </div>
</div>