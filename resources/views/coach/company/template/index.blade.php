@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('manager_show_companies')}}">Company overview</a> &nbsp;\&nbsp;{{$company->name}}
    </div>
    <div class="row">
        <div class="col-lg-10">
            <h2>{{$company->name}} overview</h2>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <h4>General information</h4>
    </div>
    <div class="col-md-4">
        <div class="pull-right">
            <a  class="btn btn-primary" a href="{{route('coach_company_employee_overview',['company' => $company])}}">{{__('buttons.check_model',['model' => 'employees'])}}</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img class="logo-img" src="{{asset($company->CompanyLogo->logo_path)}}"/></td>
                <td class="bold"></td>
            </tr>
            <tr>
                <td class="bold">Company name</td>
                <td>{{$company->name}}</td>
            </tr>
            <tr>
                <td class="bold">Company code</td>
                <td>{{$company->company_code}}</td>
            </tr>
            <tr>
                <td class="bold">Company Email</td>
                <td>{{$company->email}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row" style="padding-bottom: 15px;">
    <div class="col-md-12">
        <h4>Check company results</h4>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Year</td>

                <td class="pull-right"></td>
            </tr>
            </thead>
            <tbody>
                @foreach($years as $year)
                    <tr>
                        <td>{{$year}}</td>
                        <td class="text-right">
                            <a class="btn btn-primary" href="{{route('manager_controller_show_results',['company' => $company, 'year' => $year])}}">{{__('buttons.check_model',['model' => 'results'])}}</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>