@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a>&nbsp;\&nbsp;
        <a href="{{route('manager_show_companies')}}">Bedrijven overzicht</a>&nbsp;\&nbsp;
        <a href="{{route('manager_show_company',['company'=>$company])}}">{{$company->name}}</a>&nbsp;\&nbsp; Results for {{$year}}
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <h3>Results for <i>{{$company->name}}</i> for the year {{$year}}</h3>
    </div>
    <div class="col-md-5">
        <div class="text-right btn-padding-top">
            <a href="{{route('manager_show_company',[$company])}}" class="btn btn-primary">
                Company overview
            </a>
        </div>
    </div>
</div>
@if(is_null($chartData))

@else
    <script id="chart_data" type="application/json">
        @json($chartData)
    </script>
    <div id="page">
        <foo-chart />
    </div>
@endif