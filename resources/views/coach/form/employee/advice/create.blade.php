<div class="row">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <h3>Results</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Visit Date</td>
                    <td>{{$result->visit_date->format('D-m-Y')}}</td>
                </tr>
                @foreach($result->ResultValue as $resultValue)
                    <tr>
                        <td>{{$resultValue->column_name}}</td>
                        <td>{{$resultValue->value}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div id="advice_div" class="form-group {{$errors->has('advice') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>{{__('result.advice')}}</span>
                    <textarea id="advice" class="form-control" name="advice">{!! old('advice') ??  $result->ResultPersonalAdvice->advice ?? "" !!}</textarea>
                </label>
                @if ($errors->has('short_desc'))
                    <span class="help-block">{{ $errors->first('advice') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<script>
    CKEDITOR.replace( 'advice',{
        removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
            'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
            'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
            'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
            'About,Smiley,Blockquote'
    });
</script>