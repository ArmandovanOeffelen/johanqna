<div class="row" style="padding-bottom: 10px;">
    @if(Auth::user()->isAdmin())
        <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
            <a href="{{route('admin_controller_show_screening_overview')}}">Meting dashboard</a>&nbsp;\&nbsp;Results no advice
        </div>
    @else
        <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a>&nbsp;\&nbsp;Results no advice
        </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Results no advice</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    {{__('dashboard.email')}}
                </td>
                <td>
                    {{__('dashboard.visit_date')}}
                </td>
                <td class="">
                </td>
            </tr>
            </thead>
            <tbody>
            @if($results->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'Results with no advice'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($results as $index=>$result)
                    <tr>
                        <td>{{$index + 1 }}</td>
                        <td>{{$result->email}}</td>
                        <td>{{$result->visit_date->format('d-m-Y')}}</td>
                        <td class="text-right">
                            <button id="btn-addAdvice" class="btn btn-success" data-result="{{$result}}" >{{__('buttons.add',['model' => 'advice'])}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
        </div>
    </div>
</div>

