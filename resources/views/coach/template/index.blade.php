@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-3"> <i class="fa fa-home"></i></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-building fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Bedrijven overzicht</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('manager_show_companies')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Gebruiker overzicht</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('coach_show_employees')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-times fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Results without advice</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('coach_controller_results_no_advice')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>