@extends('layout.app')

@section('title',__('survey.dashboard'))

@section('content')
    @include('coach.survey.template.index')
@endsection
