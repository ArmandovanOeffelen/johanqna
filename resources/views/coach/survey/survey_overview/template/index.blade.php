@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('coach_show_survey_dashboard')}}">{{__('survey.dashboard')}}</a>&nbsp;\&nbsp;{{__('survey.question_overview')}}
    </div>
</div>
<div class="row">
    <div class="col-lg-7">
        <h2>{{__('survey.dashboard')}}</h2>
    </div>
    <div class="col-lg-5 pull-right" style="padding-top:25px;">
        <div class="text-right">
            <a href="{{route('coach_controller_show_singular_questions_form')}}"
                    class="btn btn-primary">{{__('buttons.add',['model' => 'question'])}}</a>
            <a href="{{route('coach_controller_show_multiple_questions_form')}}"
                    class="btn btn-primary">{{__('buttons.add_multiple',['model' => 'questions'])}}</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    {{__('survey.question')}}
                </td>
                <td>
                    {{__('survey.is_active')}}
                </td>
                <td>
                    {{__('survey.in_queue')}}
                </td>
                <td class="text-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($questions->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model'=>'questions'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($questions as $index=>$question)
                    <tr>
                        <td>{{ ($questions->perPage() * ($questions->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$question->question}}</td>
                        <td>{{$question->is_active}}</td>
                        <td>{{$question->in_queue}}</td>
                        <td class="text-right">
                            <button id="btn-deleteQuestion" class="btn btn-danger"
                                    data-question="{{$question}}">{{__('buttons.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $questions->links() !!}
        </div>
    </div>
</div>