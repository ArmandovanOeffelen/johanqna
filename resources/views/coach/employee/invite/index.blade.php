@extends('layout.app')

@section('title', 'Uitnodiginen voor '.$company->name .' - Overzicht')

@section('content')
    @include('coach.employee.invite.template.index')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze uitnodiging wilt intrekken?</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
                .on('click', '#btn-uploadLogo', function () {
                    var objectData = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Upload logo',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/company/{0}/company-logo/show-form') }}'.format(objectData.id),
                                function (response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                            let loadFile = function(event) {
                                let reader = new FileReader();
                                reader.onload = function(){
                                    let output = document.getElementById('preview_image');
                                    output.src = reader.result;
                                };
                                reader.readAsDataURL(event.target.files[0]);
                            };
                        },
                        buttons: [{
                            label: 'cancel',
                            cssClass: 'btn-danger',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var formData = new FormData();

                                formData.append('logo', $('#company-logo')[0].files[0]);
                                formData.append('_token', '{{csrf_token()}}');

                                $.ajax({
                                    type: 'post',
                                    contentType: false,
                                    processData: false,
                                    cache: false,
                                    url: '{{url('ajax/template/company/{0}/company-logo/upload') }}'.format(objectData.id),
                                    data: formData,
                                    success: function (data) {
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    })
                })
                //sendInvite
                .on('click', '#btn-sendInvite', function() {
                    var objectData1 = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Verstuur werknemer uitnodiging',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/manager/company/{0}/employee/invite/show-form') }}'.format(objectData1.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/manager/company/{0}/employee/invite/overview/send')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        last_name: dialog.getModalContent().find('input[name="last_name"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                .on('click', '#btn-editCompany', function() {
                    var objectData1 = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Bedrijf toevoegen',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/all-companies/company/{0}/show-update-form') }}'.format(objectData1.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('ajax/template/all-companies/company/{0}/update')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        code: dialog.getModalContent().find('input[name="code"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                /* Delete organisation */
                .on('click', '#btn-deleteInvite', function () {
                    var objectData1 = $(this).data('invite');
                    var objectData = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Uitnodiging intrekken',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/manager/company/{0}/employee/invite/{1}/delete')}}'.format(objectData.id,objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
