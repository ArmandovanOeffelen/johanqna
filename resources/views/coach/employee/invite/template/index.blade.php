@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('manager_employee_dashboard',[$company])}}">All werknemers</a> &nbsp;\&nbsp; Alle uitnodigingen
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Alle verstuurde uitnodigingen</h2>
        <small>Hier vind u alle werkenemers die uitgenodigd zijn. Hier kunt u de uitnodigingen versturen en verwijderen.</small>
    </div>
    <div class="col-lg-2 pull-right" style="padding-top:25px;">
        <button id="btn-sendInvite" class="btn btn-primary" data-company="{{$company}}">Verstuur uitnodiging</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Naam
                </td>
                <td>
                    Email
                </td>
                <td class="pull-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($invites->isEmpty())
                <tr class="warning">
                    <td>Er zijn nog geen uitnodigingen verstuurd voor het bedrijf binnen het systeem.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($invites as $index=>$invite)
                    <tr>
                        <td>{{ ($invites->perPage() * ($invites->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$invite->first_name}} {{$invite->last_name}}</td>
                        <td>{{$invite->email}}</td>
                        <td class="pull-right">
                            <button id="btn-deleteInvite" class="btn btn-danger" data-company="{{$company}}" data-invite="{{$invite}}">Uitnodiging intrekken</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $invites->links() !!}
        </div>
    </div>
</div>