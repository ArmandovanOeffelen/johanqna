<div class="row">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <h3>Results</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Visit Date</td>
                        <td>{{$result->visit_date->format('D-m-Y')}}</td>
                    </tr>
                    @foreach($result->ResultValue as $resultValue)
                        <tr>
                            <td>{{$resultValue->column_name}}</td>
                            <td>{{$resultValue->value}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div id="advice_div" class="form-group">
                <label class="control-label checkbox">
                    <h3>{{__('result.advice')}}</h3>
                </label>
            </div>
        </div>
        <div class="row">
            {!!  $result->ResultPersonalAdvice->advice!!}
        </div>
    </div>
    <div class="col-md-1"></div>
</div>