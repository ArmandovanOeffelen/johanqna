@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    @if(!Auth::user()->isAdmin())
        <div class="col-md-12">
            <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp; {{$user->name}}'s results
        </div>
    @else
        <div class="col-md-12">
            <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp; {{$user->name}}'s results
        </div>
    @endif
</div>
<div class="row">
    <div class="col-md-8">
        <h4>All results</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-2 col-sm-3">
            <ul class="nav nav-pills nav-stacked nav-static">
                @foreach($years as $year)
                    <li class="disabled" style="font-size: 17px;">
                        <b>{{$year}}</b>
                    </li>
                    <li><i>Screening results</i></li>
                    @foreach ($allResults as $indexResults => $results)
                        @if($results[0]['visit_date']->format('Y') === $year)

                            <li class="@if($indexResults === 0) active @endif">
                                <a href="{{"#screening-".$results[0]['visit_date']->format('d-m-Y')}}" data-toggle="tab">
                                    {{$results[0]['visit_date']->format('d-m-Y')}}
                                </a>
                            </li>
                        @endif
                    @endforeach
                        <li><i>Survey results</i></li>
                    @foreach($groupByDate as  $index => $date)
                        @if(\Carbon\Carbon::parse($index)->format('Y') === $year)
                            <li>
                                <a href="{{"#survey-".$index}}" data-toggle="tab">
                                    {{$index}}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
        <div class="col-md-10 col-sm-9">
            <div class="tab-content" class="tab-pane">
                @foreach ($allResults as $indexResults => $results)
                    <div id="screening-{{$results[0]['visit_date']->format('d-m-Y')}}"
                         class="tab-pane @if($indexResults === 0) active in @endif">
                        <table class="table table-responsive">
                            <tbody>
                            @foreach($rangeCategories as $indexCat => $category)
                                <tr style="font-size: 17px;">
                                    <td><b>{{$category->name}}</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($results as $indexSR => $result)
                                    @if(!is_null($result['indicator']))
                                        @if($result['range_category']['id'] === $category->id)
                                            <tr style="height: 35px;">
                                                <td><i class="fa fa-{{$result['icon']}}"></i>
                                                    &nbsp; {{$result['range']}}</td>
                                                @foreach($result['result_value'] as $value)
                                                    @if($result['range'] === $value['column_name'])
                                                        <td>{{$value['value']}}</td>
                                                    @endif
                                                @endforeach
                                                <td>{{$result['indicator']['name']}}</td>
                                                <td>
                                                    @if(!is_null($result['indicator_color']))
                                                        <span style="color:{{$result['indicator_color']}}">
                                                            <i class="fa fa-map-marker fa-2x"></i>
                                                        </span>
                                                    @else
                                                        &nbsp;
                                                    @endif
                                                </td>
                                                <td>
                                                    <a id="btn-ReadText" data-range_text="{{$result['range_id']}}"
                                                       style="font-size: 10px;">Read
                                                        information...</a>
                                                </td>
                                            </tr>
                                        @else
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                            @if(!is_null($result['personal_advice']))
                                <tfooter>
                                    <tr style="font-size: 17px;">
                                        <td><b>Personal advice</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><a id="btn-readPersonalAdvice"
                                               data-advice="{{$result['result_id']}}"
                                               style="font-size: 10px;">Read information...</a></td>
                                    </tr>
                                </tfooter>
                            @endif
                            </tbody>
                        </table>
                    </div>
                @endforeach
                    @foreach($groupByDate as  $index => $date)
                        <div id="survey-{{$index}}"
                             class="tab-pane @if($index === 0) active in @endif">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <td>Question</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Answer 1-10</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($groupByDate as $answers)
                                    @foreach($answers as $indexAnswer => $answer)
                                        @if($answer['created_at']->format('d-m-Y') === $index)
                                            <tr style="font-size: 17px;">
                                                <td><b>{{$answer->Survey->question}}</b></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>{{$answer->answer}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
</div>
</div>
