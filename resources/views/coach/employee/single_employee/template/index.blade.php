@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-md-12"><a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a>&nbsp;\&nbsp;
            <a href="{{route('manager_show_companies')}}">Company overview</a>&nbsp;\&nbsp;<a
                    href="{{route('coach_show_employees')}}">Employee overview</a>&nbsp;\&nbsp;{{$user->name}} {{$user->lastname}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <h2>Overview of: {{$user->name}} {{$user->lastname}}</h2>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <h4>General information</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="bold">Name</td>
                <td>{{$user->name}} {{$user->lastname}}</td>
            </tr>
            <tr>
                <td class="bold">E-mail</td>
                <td>{{$user->email}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-5"><h4>Results of scans</h4></div>
    <div class="col-md-7">
        <div class="text-right" style="padding-top: 10px;">
            <a href="{{route('coach_controller_show_all_results',[$user])}}" class="btn-primary btn">
                check all results
            </a>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 15px;">
    <div class="col-md-12">
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td>Visit date</td>

                <td class="text-right"></td>
            </tr>
            </thead>
            <tbody>
            @if($results->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'results'])}}</td>
                    <td></td>
                </tr>
            @else
                @foreach($results as $index => $result)
                    <tr>
                        <td>
                            {{$result->visit_date->format('D-m-Y')}}
                        </td>
                        <td class="text-right">
                            @if(Auth::user()->isManager())
                                @if($result->result_personal_advice_count === 0)
                                    <button id="btn-addAdvice" data-result="{{$result}}" data-employee="{{$user}}"
                                            class="btn btn-success">{{__('buttons.add',['model' => 'advice'])}}</button>
                                @else
                                    <button id="btn-showAdvice" data-result="{{$result}}" data-employee="{{$user}}"
                                            class="btn btn-success">{{__('buttons.check_model',['model' => 'advice'])}}</button>
                                    <button id="btn-editAdvice" data-result="{{$result}}" data-employee="{{$user}}"
                                            class="btn btn-primary">{{__('buttons.edit',['model' => 'advice'])}}</button>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
