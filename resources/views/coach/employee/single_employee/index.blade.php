@extends('layout.app')

@section('title', 'Werknemer : '.$user->name .' - overzicht')

@section('content')
    @include('coach.employee.single_employee.template.index')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze gebruiker wilt verwijderen?</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>{{__('messages.loading')}}</span>'
            };

            $('#content')
            //add personal advice
                .on('click', '#btn-addAdvice', function() {
                    var result = $(this).data('result');
                    var employee = $(this).data('employee');
                    BootstrapDialog.show({
                        title: "{{__('result.advice_add')}}",
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/coach/employee/{0}/result/{1}/advice/show-form') }}'.format(employee.id,result.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: "{{__('buttons.cancel')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: "{{__('buttons.submit')}}",
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                let rawHTML = CKEDITOR.instances.advice.getData();

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/coach/employee/{0}/result/{1}/advice/save')}}'.format(employee.id,result.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        advice: rawHTML,

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                //Edit advice
                .on('click', '#btn-editAdvice', function() {
                    var result = $(this).data('result');
                    var employee = $(this).data('employee');
                    BootstrapDialog.show({
                        title: "{{__('result.advice_add')}}",
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/coach/employee/{0}/result/{1}/advice/show-form') }}'.format(employee.id,result.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: "{{__('buttons.cancel')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: "{{__('buttons.submit')}}",
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                let rawHTML = CKEDITOR.instances.advice.getData();

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('ajax/template/coach/employee/{0}/result/{1}/advice/update')}}'.format(employee.id,result.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        advice: rawHTML,

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                //Show advice
                .on('click', '#btn-showAdvice', function() {
                    var result = $(this).data('result');
                    var employee = $(this).data('employee');
                    BootstrapDialog.show({
                        title: "{{__('result.advice_show')}}",
                        width: 'modal-lg',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/coach/employee/{0}/result/{1}/advice/show-advice') }}'.format(employee.id,result.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: "{{__('buttons.close')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                })
                /* Delete employee */
                .on('click', '#btn-deleteEmployee', function () {
                    var objectData = $(this).data('employee');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'Verwijder gebruiker',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Verwijderen',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/coach/employees/overview/{0}/delete')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
