@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        {{__('dennis.employee_overview')}}
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <h2>{{__('dennis.employee_overview')}}</h2>
    </div>
    <div class="col-md-4">
        <div class="pull-right">
            <button id="btn-sendInvite" class="btn btn-primary">Send invitation</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Name
                </td>
                <td>
                    Company
                </td>
                <td>
                    E-mail
                </td>
                <td class="text-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($employees->isEmpty())
                <tr class="warning">
                    <td>{{__('messages'.'no_data',['model' => 'employees'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($employees as $index=>$employee)
                    <tr>
                        <td>{{ ($employees->perPage() * ($employees->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$employee->name}} {{$employee->lastname}}</td>
                        <td>{{$employee->Company->name}}</td>
                        <td>{{$employee->email}}</td>
                        <td class="text-right">
                            <a href="{{route('coach_show_employee',['user' => $employee])}}" class="btn btn-primary">{{__('buttons.check_model', ['model' => 'profile'] )}}</a>
                            <button id="btn-deleteEmployee" class="btn btn-danger" data-employee="{{$employee}}">{{__('buttons.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $employees->links() !!}
        </div>
    </div>
</div>