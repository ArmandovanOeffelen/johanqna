@extends('layout.app')

@section('content')
    <script id="chart_data" type="application/json">
        @json($chartData)
    </script>
    <div id="page">
        <foo-chart />
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ mix('js/pages/foo_test.js') }}"></script>
@endsection
