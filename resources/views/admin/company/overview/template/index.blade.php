@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        {{__('dashboard.company_dashboard')}}
    </div>
</div>
    <div class="row">
        <div class="col-lg-10">
            <h2>Companies overview</h2>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <button id="btn-addCompany" class="btn btn-primary">Add company</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Naam
                    </td>
                    <td>
                        Code
                    </td>
                    <td>
                        Email
                    </td>
                    <td class="text-right">

                    </td>
                </tr>
                </thead>
                <tbody>
                @if($company->isEmpty())
                    <tr class="warning">
                        <td>{{__('messages.no_data',['model' => 'company'])}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @else
                    @foreach($company as $index=>$companies)
                        <tr>
                            <td>{{ ($company->perPage() * ($company->currentPage() - 1)) + $index + 1 }}</td>
                            <td>{{$companies->name}}</td>
                            <td>{{$companies->company_code}}</td>
                            <td>{{$companies->email}}</td>
                            <td class="text-right">
                                <a class="btn btn-primary" href="{{route('admin_controller_show_all_employees_for_company',[$companies])}}">{{__('dennis.check_employees')}}</a>
                                <a class="btn btn-primary" href="{{route('admin_controller_show_company_content',[$companies])}}">Check company</a>
                                <button id="btn-editCompany" class="btn btn-primary" data-company="{{$companies}}">Edit company</button>
                                <button id="btn-deleteCompany" class="btn btn-danger" data-company="{{$companies}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="pull-right">
                {!! $company->links() !!}
            </div>
        </div>
    </div>