@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_companies')}}">Company overview</a>&nbsp;\&nbsp;{{$company->name}}
    </div>
    <div class="row">
        <div class="col-lg-10">
            <h2>{{$company->name}} overview</h2>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <h4>General information</h4>
    </div>
    <div class="col-md-7">
        <div class="pull-right">
            <button id="btn-uploadLogo" class="btn btn-primary" data-company="{{$company}}">Upload logo</button>
            <button id="btn-editCompany" data-company="{{$company}}" class="btn btn-primary">Edit company</button>
            <a class="btn btn-primary" href="{{route('admin_controller_show_all_employees_for_company',[$company])}}">Check
                employees</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img class="logo-img" src="{{asset($company->CompanyLogo->logo_path)}}"/></td>
                <td class="bold"></td>
            </tr>
            <tr>
                <td class="bold">Company name</td>
                <td>{{$company->name}}</td>
            </tr>
            <tr>
                <td class="bold">Company code</td>
                <td>{{$company->company_code}}</td>
            </tr>
            <tr>
                <td class="bold">Company E-mail</td>
                <td>{{$company->email}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Company sign up link</h4>
        <table class="table table-responsive">
            <tbody>
            <tr>
                <td>
                    <p id="signup_link">www.pio-barabaz.com/sign-up/{{$company->CompanyToken->token}}/show</p>
                </td>
                <td class="text-right">

                    <button onclick="copyToClipboard('signup_link')" class="btn btn-primary">
                        Copy to clipboard
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row" style="padding-bottom: 15px;">
    <div class="col-md-12">
        <h4>Results</h4>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td class="pull-right"></td>
            </tr>
            </thead>
            <tbody>
            @foreach($years as $year)
                <tr>
                    <td>
                        {{$year}}
                    </td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('admin_controller_show_results',['company' => $company,'year' => $year])}}"
                               class="btn btn-primary">Check results</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row" style="padding-bottom: 15px;">
    <div class="col-md-12">
        <h4>SMS</h4>
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td class="pull-right"></td>
            </tr>
            </thead>
            <tbody>
                @foreach($smsYears as $years)
                    @foreach ($smsPerMonths as $item => $month)

                        @if($years == $item)
                            <tr>
                                <td class="bold">
                                    {{$years}}
                                </td>
                                <td>
                            @foreach ($month as $monthNr => $totalPerMonth)
                                <tr>
                                    <td>{{$monthNr}}</td>
                                    <td>{{$totalPerMonth}}</td>
                                </tr>
                            @endforeach
                                </tr>
                            @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>
</div>