@extends('layout.app')

@section('content')
    @include('admin.company.company.results.template.index')
@endsection

@section('scripts')
    @parent
    <script src="{{ mix('js/pages/foo_test.js') }}"></script>
@endsection
