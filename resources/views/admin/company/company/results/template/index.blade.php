@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a>&nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_companies')}}">Bedrijven overzicht</a>&nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_company_content',['company'=>$company])}}">{{$company->name}}</a>&nbsp;\&nbsp; Results for {{$year}}
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h3>Results for {{$company->name}} for the year {{$year}}</h3>
    </div>
    <div class="col-md-8">
        <div class="text-right btn-padding-top">
            <a href="{{route('admin_controller_show_company_content',[$company])}}" class="btn btn-primary">
                Company overview
            </a>
        </div>
    </div>
</div>

@if(is_null($chartData))

@else
    <script id="chart_data" type="application/json">
        @json($chartData)
    </script>
    <div id="page">
        <foo-chart />
    </div>
@endif