<nav>
        <div id="Navigation" class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    @guest
                        <a class="navbar-brand" href="{{view('auth.login')}}">
                            <img class="logo-nav" src="{{asset("storage/logo/logoBarabaz.png")}}" height="40"/>
                        </a>
                    @else
                        <a class="navbar-brand" href="{{route('admin_controller_index')}}">
                            <img class="logo-nav" src="{{asset("storage/logo/logoBarabaz.png")}}" height="40"/>
                        </a>
                    @endguest
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li class="nav-item"><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="nav-item"><a href="mailto:info@barabaz.com">Contact</a></li>
                            <li class="nav-item"><a href="{{route('admin_controller_index')}}">Dashboard</a></li>
                            <li class="nav-item">
                                @include('partials.logout')
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
</nav>