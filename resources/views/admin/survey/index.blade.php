@extends('layout.app')

@section('title',__('survey.dashboard'))

@section('content')
    @include('admin.survey.template.index')
@endsection
