<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Voeg dependency toe</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div class="text-justify">
                <h5>Selecteer type dependency</h5>
            </div>
            <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-check {{$errors->has('range_name') ? 'has-error': '' }}">
                                <input type="radio" name="select_dependency" class="form-check-input" id="different_choice" value="different" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Verzin naam</label>
                                @if ($errors->has('range_name'))
                                    <span class="help-block">{{ $errors->first('range_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check {{$errors->has('range_name') ? 'has-error': '' }}">
                                <input type="radio" name="select_dependency" class="form-check-input" value="choice" id="select_choice">
                                <label class="form-check-label" for="select_choice">Keuze</label>
                                @if ($errors->has('range_name'))
                                    <span class="help-block">{{ $errors->first('range_name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
{{--<div class="row">--}}
    {{--<div class="col-md-1"></div>--}}
    {{--<div class="col-md-10">--}}
        {{--<div class="form-group{{$errors->has('dependency_title') ? 'has-error': '' }}">--}}
            {{--<label class="control-label">--}}
                {{--<span>dependency titel</span>--}}
                {{--<input class="form-control" name="dependency_title" type="text" value="{{ old('dependency_title') ?? ''  }}">--}}
            {{--</label>--}}
            {{--@if ($errors->has('dependency_title'))--}}
                {{--<span class="help-block">{{ $errors->first('dependency_title') }}</span>--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-1"></div>--}}
{{--</div>--}}
<div class="row">
    <div id="template_destination">
    </div>
</div>
<template id="template_choice">
    <div id="template_choice_appended" class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <div class="form-group{{$errors->has('choice_2') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span id="">Keuze 1</span>
                    <input class="form-control" name="choice_2" type="text" value="{{ old('choice_2') ?? ''  }}">
                </label>
                @if ($errors->has('choice_2'))
                    <span class="help-block">{{ $errors->first('choice_2') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group{{$errors->has('choice_1') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Keuze 2</span>
                    <input class="form-control" name="choice_1" type="text" value="{{ old('choice_1') ?? ''  }}">
                </label>
                @if ($errors->has('choice_1'))
                    <span class="help-block">{{ $errors->first('choice_1') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</template>
<template id="template_different">
    <div id="template_different_appended" class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            TBD
        </div>
        <div class="col-md-5">
            TBD
        </div>
        <div class="col-md-1"></div>
    </div>
</template>