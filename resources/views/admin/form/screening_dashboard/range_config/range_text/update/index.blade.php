<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div id="range_text_div" class="form-group {{$errors->has('range_title') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>{{__('range.range_title')}}</span>
                    <input type="text" id="range_title" class="form-control" name="range_title" value="{!! old('range_title') ?? $range->RangeText->title ?? "" !!}"/>
                </label>
                @if ($errors->has('range_title'))
                    <span class="help-block">{{ $errors->first('range_title') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div id="range_text_div" class="form-group {{$errors->has('range_text') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>{{__('range.range_text')}}</span>
                    <textarea id="range_text" class="form-control" name="range_text">{!! old('range_text') ?? $range->RangeText->text ??"" !!}</textarea>
                </label>
                @if ($errors->has('range_text'))
                    <span class="help-block">{{ $errors->first('range_text') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<script>
    CKEDITOR.replace( 'range_text',{
        removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
            'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
            'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
            'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
            'About,Smiley,Blockquote'
    });
</script>