@include('partials.flash-message')

<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12">
        <a href="{{route('admin_controller_index')}}">
            <i class="fa fa-home"></i>
        </a>
        <span>&nbsp;\&nbsp;</span>
        <a href="{{route('admin_controller_show_screening_overview')}}">
            <span>Meting dashboard</span>
        </a>
        <span>&nbsp;\&nbsp;</span>
        <a href="{{route('admin_controller_show_range_config_overview')}}">
            <span>Range overview</span>
        </a>
        <span>&nbsp;\&nbsp;</span>
        <span>Range configurator</span>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <h2>Add result range</h2>
    </div>
    <div class="col-lg-3">
        <div class="text-right btn-padding-top">
            <button id="btn-showTutorial" class="btn btn-primary">Tutorial</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <h4>General information</h4>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-5">
        <h4>Range text</h4>
    </div>
</div>

<div class="row">
    <form action="{{  $range->id ? route('admin_range_form_update_save',  ['range' => $range->id]) : route('admin_range_form_create_save') }}"
          method="POST"
    >
        <div class="col-md-5">

            {!! csrf_field() !!}
            <div class="form-group {{ $errors->has('name') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Result name</span>
                    <input class="form-control" name="name" type="text"
                           value="{{ old('name') ?? $range->name  }}">
                </label>
                @if ($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('column_name') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Column name (Excel)</span>
                    <input class="form-control" name="column_name" type="text"
                           value="{{ old('column_name') ?? $range->column_name  }}">
                </label>
                @if ($errors->has('column_name'))
                    <span class="help-block">{{ $errors->first('column_name') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('range_category') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Range Category</span>
                    <select class="form-control" name="range_category"
                            @if(!is_null($range->range_category_id))disabled @endif>
                        @foreach($rangeCategories as $rangeCategory)
                            @if($rangeCategory->id ===$range->range_category_id)
                                <option value="{{$rangeCategory->id}}" selected>
                                {{$rangeCategory->name}}
                                </option>
                            @else
                                <option value="{{$rangeCategory->id}}">
                                    {{$rangeCategory->name}}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </label>
                @if ($errors->has('range_category'))
                    <span class="help-block">{{ $errors->first('range_category') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('icon') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Font Awesome icon</span>
                    <input class="form-control" name="icon" type="text" placeholder="adress-card"
                           value="{{ old('icon') ?? $range->icon  }}">
                </label>
                @if ($errors->has('icon'))
                    <span class="help-block">{{ $errors->first('icon') }}</span>
                @endif
            </div>
            <div class="pull-right">
                <button class="btn btn-primary" type="submit">Save changes</button>
            </div>

        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5">
            <div id="range_text_div" class="form-group {{$errors->has('range_text_title') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>{{__('range.range_title')}}</span>
                    <input type="text" id="range_title" class="form-control" name="range_text_title"
                           value="{!! old('range_title') ?? $range->range_text_title ?? "" !!}"/>
                </label>
                @if ($errors->has('range_text_title'))
                    <span class="help-block">{{ $errors->first('range_text_title') }}</span>
                @endif
            </div>
            <div id="range_text_div" class="form-group {{$errors->has('range_text') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>{{__('range.range_text')}}</span>
                    <textarea id="range_text_test" class="form-control"
                              name="range_text">{!! old('range_text') ?? $range->range_text ??  "" !!}</textarea>
                </label>
                @if ($errors->has('range_text'))
                    <span class="help-block">{{ $errors->first('range_text') }}</span>
                @endif
            </div>
        </div>
    </form>
</div>

<br/>

@if($range->id !== null)
    <div class="row">
        <div class="col-md-2">
            <h3>Range criteria</h3>
        </div>
        <div class="col-md-10">
            <ul class="nav nav-tabs nav-justified" style="margin-top: 15px">
                @foreach($rangeIndicators as $index => $rangeIndicator)
                    <li class="{{ 0 === $index ? 'active' : '' }}">
                        <a href="#tab_{{ $rangeIndicator->id }}"
                           data-toggle="tab"
                        >
                            <strong>{{$rangeIndicator->name}}</strong>
                            &nbsp;
                            <span style="color:{{ $rangeIndicator->RangeColor->color_code }} ">
                                <i class="fa fa-square"></i>
                            </span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-12">
            <div class="tab-content">
                @foreach($rangeIndicators as $index => $rangeIndicator)
                    <div id="tab_{{ $rangeIndicator->id }}"
                         class="tab-pane {{ 0 === $index ? 'in active' : '' }}"
                         style="margin-top: 25px; margin-bottom: 15px"
                    >
                        <range-table-component
                                v-bind:range-id="{{ $range->id  }}"
                                v-bind:range-indicator-id="{{ $rangeIndicator->id }}"
                                v-bind:criteria-collections="{{ json_encode($criteriaCollectionsData[$rangeIndicator->id] ?? []) }}"
                        />
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-12">
            <div id="range_collection_overlap_message"></div>
        </div>
    </div>
@endif

