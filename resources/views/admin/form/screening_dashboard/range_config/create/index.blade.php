@extends('layout.app')

@section('title', 'RangeIndicator')

@section('content')
    @include('admin.form.screening_dashboard.range_config.create.template.create')
@endsection

@section('scripts')
    @parent
    <script>
        window.updateOverlappingMessage = function (rangeId) {
            axios.request({
                url: '/ajax/range/' + rangeId + '/get-overlapping-criteria',
                method: 'GET',
            }).then(({data}) => {
                $('#range_collection_overlap_message').html(data.html);
            });
        };
        window.updateOverlappingMessage({{ $range->id }});
    </script>
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'range_text_test',{
            removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
                'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
                'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
                'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
                'About,Smiley,Blockquote'
        });

        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u dit bedrijf wilt verwijderen?</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>Even geduld...</span>'
            };

            ///screening/range-config/tutorial
            $('#content')


            //Show advice
                .on('click', '#btn-showTutorial', function() {
                    BootstrapDialog.show({
                        title: "{{__('tutorial.title')}}",
                        class: 'modal-lg',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/screening/range-config/tutorial') }}',
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: "{{__('buttons.close')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                })
                /* Delete organisation */
                .on('click', '#btn-deleteCompany', function () {
                    var objectData1 = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Verwijder range',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/company/{0}/delete')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
