<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Add range indicator</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('range_name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Range indicator name</span>
                <input class="form-control" name="range_name" type="text" value="{{ old('range_name') ?? ''  }}">
            </label>
            @if ($errors->has('range_name'))
                <span class="help-block">{{ $errors->first('range_name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('range_name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Rank</span>
                <select id="" name="rank" class="form-control mdb-select md-form">
                    @for($i=1; $i<=15; $i++)
                           <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </label>
            @if ($errors->has('range_name'))
                <span class="help-block">{{ $errors->first('range_name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group {{$errors->has('range_color') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Color</span>
                <select id="color-select" name="range_color" class="form-control mdb-select md-form">
                    @foreach($rangecolors as $rColor)
                        <option value="{{$rColor}}">{{$rColor->color_name}}</option>
                    @endforeach
                </select>
            </label>
            @if ($errors->has('range_color'))
                <span class="help-block">{{ $errors->first('range_color') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group" style="padding-top: 5px;">
            <label class="control-label">Selected color</label>
            <div id="selected-color" class="form-control"></div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>