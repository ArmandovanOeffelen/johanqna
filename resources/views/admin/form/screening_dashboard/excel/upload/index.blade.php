@extends('layout.app')

@section('title', 'Excel upload')

@section('content')
    @include('admin.form.screening_dashboard.excel.upload.template.upload')
@endsection
@section('scripts')
    @parent
        <script>
            $(document).ready( function() {
                $(document).on('change', '.btn-file :file', function() {
                    var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [label]);
                });

                $('.btn-file :file').on('fileselect', function(event, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });
        </script>
@endsection
