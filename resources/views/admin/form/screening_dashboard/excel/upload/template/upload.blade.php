@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    @if(Auth::user()->isAdmin())
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_screening_overview')}}">Meting dashboard</a>&nbsp;\&nbsp;Excel bestand uploaden
    </div>
    @else
        <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
            <a href="{{route('coach_controller_show_dashboard')}}">Meting dashboard</a>&nbsp;\&nbsp;Excel bestand uploaden
        </div>
    @endif
</div>

<div class="row">
    <div class="col-lg-10">
        <h2>Excel bestand uploaden</h2>
    </div>
    <div class="col-lg-2 pull-right" style="padding-top:25px;">

    </div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if($ranges === 0)
            <div class="alert alert-warning alert-block text-center">
                <button style="padding-left: 15px;" type="button" class="close" data-dismiss="alert">×</button>
                <strong>There are no ranges found in the system. The only columns which will be read are
                    These are base columns.<br />
                    Columns:
                    @foreach($base_columns as $index => $columns)
                       <i>{{$columns}}</i> &nbsp;
                    @endforeach
                </strong> <br />
                @if(Auth::user()->isAdmin())
                    <a href="{{route('admin_range_form_create')}}">Add Range</a>
                @elseif (Auth::user()->isManager())
                    Please contact the administators that there are no Ranges found.
                @endif
            </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <form action="{{action('ResultController@importExcel')}}" enctype="multipart/form-data" method="POST">
            <div class="form-group">
                <h4>Excel bestand uploaden</h4>
                <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        Selecteer bestand… <input type="file" name="excel" id="imgInp" />
                    </span>
                </span>
                    <input type="text" class="form-control" readonly>
                </div>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Verstuur</button>
            </div>
            {!! csrf_field() !!}
        </form>
    </div>
    <div class="col-md-4"></div>
</div>
