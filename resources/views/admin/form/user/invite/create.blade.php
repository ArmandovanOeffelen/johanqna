<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Verstuur coach invite</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Voornaam</span>
                <input class="form-control" name="name" type="text" value="{{ old('name') ?? ''  }}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('last_name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Achternaam</span>
                <input class="form-control" name="last_name" type="text" value="{{ old('last_name') ?? ''  }}">
            </label>
            @if ($errors->has('last_name'))
                <span class="help-block">{{ $errors->first('last_name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>E-mail</span>
                <input class="form-control" name="email" type="text" value="{{ old('email') ?? ''  }}">
            </label>
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-1"></div>
</div>