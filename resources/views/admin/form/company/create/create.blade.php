<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Voeg Bedrijf toe</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-7">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs naam</span>
                <input class="form-control" name="name" type="text" value="{{ old('name') ?? ''  }}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{$errors->has('code') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs code</span>
                <input class="form-control" name="code" type="text" value="{{ old('code') ?? ''  }}">
            </label>
            @if ($errors->has('code'))
                <span class="help-block">{{ $errors->first('code') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs email</span>
                <input class="form-control" name="email" type="text" value="{{ old('email') ?? ''  }}">
            </label>
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-1"></div>
</div>