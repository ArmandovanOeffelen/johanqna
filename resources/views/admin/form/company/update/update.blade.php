<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Bewerk {{$company->name}}</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-7">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs naam</span>
                <input class="form-control" name="name" type="text" value="{{ old('name') ?? $company->name ?? ''  }}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{$errors->has('code') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs code</span>
                <input class="form-control" name="code" type="text" value="{{ old('code') ?? $company->company_code ?? ''  }}">
            </label>
            @if ($errors->has('code'))
                <span class="help-block">{{ $errors->first('code') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Bedrijfs email</span>
                <input class="form-control" name="email" type="text" value="{{ old('email') ?? $company->email ?? ''  }}">
            </label>
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('appointment_frame_name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Appointment frame name</span>
                <input class="form-control" name="appointment_frame_name" type="text" value="{{ old('appointment_frame_name') ?? $company->appointment_frame_name ?? ''  }}">
            </label>
            @if ($errors->has('appointment_frame_name'))
                <span class="help-block">{{ $errors->first('appointment_frame_name') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-1"></div>
</div>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('appointment') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Appointment frame</span>
                <textarea class="form-control" name="appointment" type="text">{{ old('appointment') ?? $company->appointment_frame ?? ''  }}</textarea>
            </label>
            @if ($errors->has('appointment'))
                <span class="help-block">{{ $errors->first('appointment') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-1"></div>
</div>