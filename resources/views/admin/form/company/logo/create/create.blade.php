<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Upload logo</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="text-center"><h4>Huidig logo</h4></div>
        @if(is_null($companyLogo->logo_name))
            <div class="text-center warning"><p>Dit bedrijf heeft nog geen logo.</p></div>
        @else
        <div class="text-center"><img src="{{asset($company->CompanyLogo->logo_path)}}"/></div>
        @endif
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="text-center"><h4>Logo die u probeert te uploaden</h4></div>
            <div class="text-center"><img id="preview_img"/></div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div id="file-input" class="form-group{{$errors->has('logo') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <input id="company-logo" class="form-control" name="logo" type="file" value="{{ Request::input('logo')}}">
            </label>
            @if ($errors->has('logo'))
                <span class="help-block">{{ $errors->first('logo') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>