@extends('layout.app')

@section('title', 'Screening overview')

@section('content')
    @include('admin.screening_dashboard.template.index')
@endsection
