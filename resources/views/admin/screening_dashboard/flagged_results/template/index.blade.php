@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    @if(Auth::user()->isAdmin())
        <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
            <a href="{{route('admin_controller_show_screening_overview')}}">Meting dashboard</a>&nbsp;\&nbsp;Flagged excel resultaten
        </div>
    @else
        <div class="col-md-12"> <a href="{{route('coach_dashboard_view')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
            <a href="{{route('coach_controller_show_dashboard')}}">Meting dashboard</a>&nbsp;\&nbsp;Flagged excel resultaten
        </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Flagged excel resultaten</h2>
        <small>Hier kun je de E-mail adressen vinden met de excel bestandsnaam.</small>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    email
                </td>
                <td>
                    Excel bestandsnaam
                </td>
                <td class="">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($results->isEmpty())
                <tr class="warning">
                    <td>Er zijn geen flagged resultaten gevonden in het systeem.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($results as $index=>$result)
                    <tr>
                        <td>{{ ($results->perPage() * ($results->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$result->email}}</td>
                        <td>{{$result->excel_name}}</td>
                        <td class="text-right">
                            <button id="btn-markResolved" class="btn btn-success" data-result="{{$result}}" >Opgelost</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $results->links() !!}
        </div>
    </div>
</div>