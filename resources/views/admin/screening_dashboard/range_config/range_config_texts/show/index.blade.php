<div class="row">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <h3>Range - {{$range->name}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div id="advice_div" class="form-group">
                <label class="control-label checkbox">
                    <h4>{{$range->RangeText->title}}</h4>
                </label>
            </div>
        </div>
        <div class="row">
            {!!  $range->RangeText->text !!}
        </div>
    </div>
    <div class="col-md-1"></div>
</div>