@extends('layout.app')

@section('title', 'Range texts')

@section('content')
    @include('admin.screening_dashboard.range_config.range_config_texts.template.index')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script>
        $(function () {

            var templates = {
                delete: '<span>{{__("messages.delete",['model'=> 'range text'])}}</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>{{__('messages.loading')}}</span>'
            };

            $('#content')
            //Add Range text
                .on('click', '#btn-addText', function() {
                    var range = $(this).data('range');
                    BootstrapDialog.show({
                        title: "{{__('range.range_add')}}",
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/show-form') }}'.format(range.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: "{{__('buttons.cancel')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: "{{__('buttons.submit')}}",
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                let rawHTML = CKEDITOR.instances.range_text.getData();

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/post-form')}}'.format(range.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        range_text: rawHTML,
                                        range_title: dialog.getModalContent().find('input[name="range_title"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                //update Range text
                .on('click', '#btn-updateText', function() {
                    var range = $(this).data('range');
                    BootstrapDialog.show({
                        title: "{{__('range.range_add')}}",
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/show-update-form') }}'.format(range.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: "{{__('buttons.cancel')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: "{{__('buttons.submit')}}",
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                let rawHTML = CKEDITOR.instances.range_text.getData();

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/post-update-form')}}'.format(range.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        range_text: rawHTML,
                                        range_title: dialog.getModalContent().find('input[name="range_title"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                //Show advice
                .on('click', '#btn-showText', function() {
                    var range = $(this).data('range');
                    BootstrapDialog.show({
                        title: "{{__('range.range_show_text')}}",
                        width: 'modal-lg',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/text/show') }}'.format(range.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: "{{__('buttons.close')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                })
                /* Delete employee */
                .on('click', '#btn-deleteText', function () {
                    var objectData = $(this).data('range');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'Verwijder gebruiker',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: "{{__('buttons.cancel')}}",
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: "{{__('buttons.delete')}}",
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/screening/range-config/texts/overview/range/{0}/text/delete')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
