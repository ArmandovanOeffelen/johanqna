@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_screening_overview')}}">Screening instellingen</a>&nbsp;\&nbsp; <a href="{{route('admin_controller_show_range_config_dashboard')}}">Range Settings</a> &nbsp;\&nbsp;Range texts
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>{{__('range.range')}}</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    {{__('dashboard.name')}}
                </td>
                <td class="text-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($ranges->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'ranges'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($ranges as $index=>$range)
                    <tr>
                        <td>{{ ($ranges->perPage() * ($ranges->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$range->name}}</td>
                        <td class="text-right">
                            @if($range->range_text_count === 0)
                                <button id="btn-addText" data-range="{{$range}}" class="btn btn-success">{{__('buttons.add',['model' => 'text'])}}</button>
                            @else
                                <button id="btn-showText" data-range="{{$range}}" class="btn btn-success">{{__('buttons.check_model',['model' => 'text'])}}</button>
                                <button id="btn-updateText" data-range="{{$range}}" class="btn btn-primary">{{__('buttons.edit',['model' => 'text'])}}</button>
                            @endif
                                <button id="btn-deleteText" data-range="{{$range}}" class="btn btn-danger">{{__('buttons.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="text-right">
            {!! $ranges->links() !!}
        </div>
    </div>
</div>