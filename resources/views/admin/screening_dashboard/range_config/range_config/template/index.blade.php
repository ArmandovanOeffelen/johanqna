@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_screening_overview')}}">Meting dashboard</a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_range_config_dashboard')}}">Range config dashboard</a> &nbsp;\&nbsp;Range overview
    </div>
</div>
{{--TODO:: THIS IS THE RANGE CONFIG OVERVIEW--}}
<div class="row">
    <div class="col-md-8 col-lg-8">
        <h2>Range overview</h2>
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="btn-padding-top text-right">
            <a class="btn btn-primary" href="{{route('admin_controller_show_range_values_form')}}">{{__('buttons.add',['model' => 'Range'])}}</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Name
                </td>
                <td>
                    Column
                </td>
                <td class="pull-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($ranges->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'ranges'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($ranges as $index=>$range)
                    <tr>
                        <td>{{ ($ranges->perPage() * ($ranges->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$range->name}}</td>
                        <td>{{$range->column_name}}</td>
                        <td class="text-right">
                            <a href="{{route('admin_range_form_update',[$range])}}" id="btn-editCompany" class="btn btn-primary" data-range="{{$range}}">{{__('buttons.edit',['model' => 'range'])}}</a>
                            <button id="btn-deleteRange" class="btn btn-danger" data-range="{{$range}}">{{__('buttons.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $ranges->links() !!}
        </div>
    </div>
</div>