<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        Meting dashboard
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-cogs fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Range settings</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_range_config_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-flag fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Flagged results</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_flagged_results')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-upload fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Upload Excel</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_excel_upload_form')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
