@if(count($items) > 0)
    <div class="alert alert-warning">
        <h4><i class="fa fa-exclamation-triangle"></i> There are some rows that are overlapping</h4>
        <ul>
            @foreach($items as list($a, $b))
                <li>
                    {{ __('messages.range_collection_overlaps', ['a' => $a, 'b' => $b]) }}
                </li>
            @endforeach
        </ul>
    </div>
@endif