@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_screening_overview')}}">Screening settings</a>&nbsp;\&nbsp;<a
                href="{{route('admin_controller_show_range_config_dashboard')}}">Range config dashboard</a>&nbsp;\&nbsp;
                Range indicators
    </div>
</div>
    <div class="row">
        <div class="col-lg-10">
            <h2>Range indicator overview</h2>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <button id="btn-addRange" class="btn btn-primary">Add Range indicator</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Name
                    </td>
                    <td>
                        Rank
                    </td>
                    <td>
                        Color
                    </td>
                    <td class="text-right">

                    </td>
                </tr>
                </thead>
                <tbody>
                @if($range->isEmpty())
                   <tr class="warning">
                       <td>{{__('messages.no_data',['model' => 'Range indicatoren'])}}</td>
                       <td></td>
                       <td></td>
                       <td></td>
                   </tr>
                @else
                    @foreach($range as $index=>$ranges)
                        <tr>
                            <td>{{ ($range->perPage() * ($range->currentPage() - 1)) + $index + 1 }}</td>
                            <td>{{$ranges->name}}</td>
                            <td>{{$ranges->rank}}</td>
                            <td>
                                <span class="" style="color: {{ $ranges->RangeColor->color_code }}"><i class="fa fa-square fa-2x"></i></span>
                            </td>
                            <td class="text-right">
                                <button id="btn-updateRange" class="btn btn-primary" data-range="{{$ranges}}">Edit</button>
                                <button id="btn-deleteRange" class="btn btn-danger" data-range="{{$ranges}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="pull-right">
                {!! $range->links() !!}
            </div>
        </div>
    </div>
</div>