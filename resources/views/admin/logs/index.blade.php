@extends('layout.app')

@section('title', 'Log overview')

@section('content')
    @include('admin.logs.template.index')
@endsection