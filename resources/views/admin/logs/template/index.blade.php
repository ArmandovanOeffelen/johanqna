@include('partials.flash-message')

<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-3"> <i class="fa fa-home"></i></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">{{__('dashboard.sms_dashboard')}}</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_sms_log_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>