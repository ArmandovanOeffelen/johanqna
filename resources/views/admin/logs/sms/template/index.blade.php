<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <nav style="padding-bottom: 10px;" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('admin_log_dashboard')}}">Log dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sms overview</li>
            </ol>
        </nav>
    </div>
</div>
<div class="row" style="padding-top:55px;">
    <div class="col-lg-10">
        <h2>SMS overview</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-lg-12">
        <table class="table table-responsive table-striped">
            <tr>
                <td>balance</td>
                <td class="bold">{{"€ $sms->balance"}}</td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h4>Latest 15 sent sms</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-responsive">
            <thead>
            <tr class="bold">
                <td>Company name</td>
                <td>Action</td>
                <td>Date</td>
            </tr>
            </thead>
            @foreach($smsLogs as $log)
                <tr>
                    <td>
                        {{$log->User->Company->name}}
                    </td>
                    <td>
                        {{$log->action}}
                    </td>
                    <td>
                        {{$log->created_at->format('d-m-Y')}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>