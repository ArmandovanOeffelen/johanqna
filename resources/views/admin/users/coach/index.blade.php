@extends('layout.app')

@section('title', 'Coach overview')

@section('content')
    @include('admin.users.coach.template.index')
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze coach wilt verwijderen?</span>',
                promote: '<span>Weet u zeker dat u deze coach wilt promoveren tot admin?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
                //sendInvite
                .on('click', '#btn-inviteCoach', function() {
                    BootstrapDialog.show({
                        title: 'Verstuur uitnodiging',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/user/coach/show-invite-form') }}',
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/user/coach/send-invite')}}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        last_name: dialog.getModalContent().find('input[name="last_name"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                /* promote coach to admin*/
                .on('click', '#btn-promoteCoach', function() {
                    var objectData1 = $(this).data('coach');
                    BootstrapDialog.show({
                        title: 'Promoveer gebruiker',
                        message: templates.promote,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Promoveer',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/users/coach/{0}/promote')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                /* Delete organisation */
                .on('click', '#btn-deleteCoach', function () {
                    var objectData1 = $(this).data('coach');
                    BootstrapDialog.show({
                        title: 'Verwijder coach',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/user/coach/{0}/delete')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection