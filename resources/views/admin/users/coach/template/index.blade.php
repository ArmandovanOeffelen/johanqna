@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_user_dashboard')}}">User dashboard</a> &nbsp;\&nbsp; Coach overview
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Coach overview</h2>
    </div>
    <div class="col-lg-2 pull-right" style="padding-top:25px;">
        <button id="btn-inviteCoach" class="btn btn-primary">Invite coach</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Naam
                </td>
                <td>
                    E-mail
                </td>
                <td class="pull-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($coaches->isEmpty())
                <tr class="warning">
                    <td>Nog geen één coach heeft zich geregistreerd.</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($coaches as $index=>$coach)
                    <tr>
                        <td>{{ ($coaches->perPage() * ($coaches->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$coach->name}} {{$coach->lastname}} </td>
                        <td>{{$coach->email}}</td>
                        <td class="text-right">
                            <button id="btn-promoteCoach" class="btn btn-success" data-coach="{{$coach}}">Promote to Admin</button>
                            <button id="btn-deleteCoach" class="btn btn-danger" data-coach="{{$coach}}">Delete</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $coaches->links() !!}
        </div>
    </div>
</div>