<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        User overview</a>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-user-md fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Coach overview</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_all_coaches')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">User overview</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_user_employee_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-user-secret fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">Admin overview</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_all_admins')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
