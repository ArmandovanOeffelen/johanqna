@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">

    <div class="row" style="padding-bottom: 10px;">
        @if(!Auth::user()->isAdmin())
            <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a>&nbsp;\&nbsp;
                <a href="{{route('admin_controller_show_companies')}}">Bedrijven overzicht</a>&nbsp;\&nbsp;<a href="{{route('admin_controller_show_user_employee_dashboard')}}">Werknemer overzicht</a>&nbsp;\&nbsp;{{$user->name}} {{$user->lastname}}
            </div>
        @else
            <small>No breadcrumb generated.</small>
        @endif

    </div>
    <div class="row">
        <div class="col-lg-10">
            <h2>Overview of {{$user->name}} {{$user->lastname}}</h2>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <h4>General information</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="bold">Name</td>
                <td>{{$user->name}} {{$user->lastname}}</td>
            </tr>
            <tr>
                <td class="bold">E-mail</td>
                <td>{{$user->email}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row" style="padding-bottom: 15px;">
    <div class="col-md-5">
        <h4>Scan results</h4>
    </div>
    <div class="col-md-7">
        <div class="text-right">
            <a href="{{route('admin_controller_show_user_results',['user' => $user])}}" class="btn btn-primary">Check all results</a>
        </div>
    </div>
</div>
