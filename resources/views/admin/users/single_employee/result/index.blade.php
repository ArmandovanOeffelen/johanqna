@extends('layout.app')

@section('title', $user->name.' results')

@section('content')
    @include('admin.users.single_employee.result.template.index')
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze gebruiker wilt verwijderen?</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>{{__('messages.loading')}}</span>'
            };

            $('#content')
            //add personal advice

            //Show advice
                .on('click', '#btn-ReadText', function() {
                    var range_text = $(this).data('range_text');
                    console.log(range_text);
                    BootstrapDialog.show({
                        title: "{{__('result.information_show')}}",
                        width: 'modal-lg',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/coach/results/range/{0}/show-info') }}'.format(range_text),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: "{{__('buttons.close')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                })

                .on('click', '#btn-readPersonalAdvice', function() {
                    var advice = $(this).data('advice');
                    BootstrapDialog.show({
                        title: "{{__('result.advice_show')}}",
                        width: 'modal-lg',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/coach/results/result/{0}/show-advice') }}'.format(advice),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: "{{__('buttons.close')}}",
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                })
                /* Delete employee */
                .on('click', '#btn-deleteEmployee', function () {
                    var objectData = $(this).data('employee');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'Verwijder gebruiker',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Verwijderen',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/coach/employees/overview/{0}/delete')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
