@extends('layout.app')

@section('title', 'Werknemers '.$user->name .' - overview')

@section('content')
    @include('admin.users.single_employee.template.index')
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze gebruiker wilt verwijderen?</span>',
                {{--add_credit: `@include('forms.company.add_credits')`,--}}
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
            //sendInvite
                .on('click', '#btn-sendInvite', function() {
                    var objectData1 = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Verstuur werknemer uitnodiging',
                        message: $(templates.loading)
                            .load('{{ url('coach') }}'.format(objectData1.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/manager/company/{0}/employee/invite/send')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        last_name: dialog.getModalContent().find('input[name="last_name"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                /* Delete employee */
                .on('click', '#btn-deleteEmployee', function () {
                    var objectData = $(this).data('employee');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'Verwijder gebruiker',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Verwijderen',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/coach/employees/overview/{0}/delete')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
