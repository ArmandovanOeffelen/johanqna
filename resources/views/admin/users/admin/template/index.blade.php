@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_user_dashboard')}}">User dashboard</a> &nbsp;\&nbsp; Admin overview
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Admin overview</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Name
                </td>
            </tr>
            </thead>
            <tbody>
            @if($users->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'admin'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($users as $index=>$user)
                    <tr>
                        <td>{{ ($users->perPage() * ($users->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$user->name}} {{$user->lastname}} </td>
                        <td>{{$user->email}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $users->links() !!}
        </div>
    </div>
</div>