@extends('layout.app')

@section('title', 'Employee overview')

@section('content')
    @include('admin.users.user.template.index')
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze gebruiker wilt verwijderen?</span>',
                promote: '<span>Weet u zeker dat u deze gebruiker wilt promoveren tot admin?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
            //sendInvite
                .on('click', '#btn-inviteUser', function() {
                    BootstrapDialog.show({
                        title: 'Verstuur uitnodiging',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/users/user/show-invite-form') }}',
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/users/user/send-invite')}}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        company: dialog.getModalContent().find('select[name="company"]').val(),
                                        last_name: dialog.getModalContent().find('input[name="last_name"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function () {
                                        dialog.close();
                                        $('#content').html();
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                /* promote user to admin*/
                .on('click', '#btn-promoteUser', function() {
                    var objectData1 = $(this).data('user');
                    BootstrapDialog.show({
                        title: 'Promoveer gebruiker',
                        message: templates.promote,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Promoveer',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('ajax/template/users/user/{0}/promote')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })


                    .on('click', '#btn-editCompany', function() {
                    var objectData1 = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Bedrijf toevoegen',
                        message: $(templates.loading)
                            .load('{{ url('ajax/template/all-companies/company/{0}/show-update-form') }}'.format(objectData1.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function onshown() {
                        },
                        buttons: [{
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Opslaan',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('ajax/template/all-companies/company/{0}/update')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        code: dialog.getModalContent().find('input[name="code"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),

                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                /* Delete organisation */
                .on('click', '#btn-deleteUser', function () {
                    var objectData1 = $(this).data('user');
                    BootstrapDialog.show({
                        title: 'Verwijder gebruiker',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-info',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('ajax/template/users/{0}/delete')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection