@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"> <a href="{{route('admin_controller_index')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp;
        <a href="{{route('admin_controller_show_user_dashboard')}}">Users dashboard</a> &nbsp;\&nbsp; Employee overview
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <h2>Employee overview</h2>
    </div>
    <div class="col-lg-2 pull-right" style="padding-top:25px;">
        <button id="btn-inviteUser" class="btn btn-primary">Invite employee</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <table class="table-striped table table-responsive">
            <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Name
                </td>
                <td>
                    E-mail
                </td>
                <td class="pull-right">

                </td>
            </tr>
            </thead>
            <tbody>
            @if($users->isEmpty())
                <tr class="warning">
                    <td>{{__('messages.no_data',['model' => 'employees'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($users as $index=>$user)
                    <tr>
                        <td>{{ ($users->perPage() * ($users->currentPage() - 1)) + $index + 1 }}</td>
                        <td>{{$user->name}} {{$user->lastname}} </td>
                        <td>{{$user->email}}</td>
                        <td class="text-right">
                            <a class="btn btn-primary" href="{{route('admin_show_employee',['user' => $user])}}">{{__('buttons.check_model',['model' => 'employee'])}}</a>
                            <button id="btn-promoteUser" class="btn btn-success" data-user="{{$user}}">{{__('buttons.make_something',['model' => 'admin'])}}</button>
                            <button id="btn-deleteUser" class="btn btn-danger" data-user="{{$user}}">{{__('buttons.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $users->links() !!}
        </div>
    </div>
</div>