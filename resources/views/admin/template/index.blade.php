<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-3"> <i class="fa fa-home"></i></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
</div>
<div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <i class="fa fa-list fa-5x"></i>
                        </div>
                        <div class="col-xs-6 text-right">
                            <h4 class="announcement-text">{{__('dashboard.company_dashboard')}}</h4>
                        </div>
                    </div>
                </div>
                <a href="{{route('admin_controller_show_companies')}}">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                {{__('buttons.go')}}
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-cogs fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">{{__('dashboard.screening_dashboard')}}</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_screening_overview')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">{{__('dashboard.user_dashboard')}}</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_controller_show_user_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-question fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">{{__('dashboard.survey_dashboard')}}</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_show_survey_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 class="announcement-text">{{__('dashboard.log_dashboard')}}</h4>
                    </div>
                </div>
            </div>
            <a href="{{route('admin_log_dashboard')}}">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            {{__('buttons.go')}}
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>