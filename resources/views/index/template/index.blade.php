@include('partials.flash-message')
<div class="row">
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <h3> Questionaire</h3>
        <small>Deze tekst moet nog aangeleverd worden.</small>
    </div>
</div>
<div class="row">
    <div id="survey" class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-11 col-lg-11 col-xs-11 col-sm-11">
        @include('index.template.form')
    </div>
</div>