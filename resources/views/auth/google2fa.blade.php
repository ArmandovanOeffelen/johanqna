@extends('layout.app')

@section('content')
    <div class="container">
        @include('partials.flash-message')
        <div class="col-md-8 col-md-offset-2">
            <div style="padding-bottom: 10px;">
                <div class="text-center">
                    <img src="{{asset('storage/logo/logoBarabaz.png')}}"/>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #a51c4b; color: #ed7764;">
                    <h4 class="text-center" style="color: white; padding-top: 10px;">
                        Your secret Authenticator code
                    </h4>
                </div>
                <div class="panel-body" style="text-align: center;">
                    <form class="form-horizontal" method="POST" action="{{ route('2fa') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="one_time_password" class="col-md-4 control-label">One Time Password</label>

                            <div class="col-md-6">
                                <input id="one_time_password" type="number" class="form-control"
                                       name="one_time_password" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn" style="background-color:#a51c4b; color: #ffffff;">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection