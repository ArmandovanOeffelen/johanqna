@extends('layout.register')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.flash-message')
            <div class="col-md-8 col-md-offset-2">
                <div style="padding-bottom: 10px;">
                    <div class="text-center">
                        <img style="width:300px; height: 103px;" src="{{asset('storage/logo/logoBarabaz.png')}}"/>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #a51c4b; color: #ed7764;"><h4
                                class="text-center" style="color: white; padding-top: 10px;">Sign up</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('custom_signup', [$token]) }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">First name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Last name</label>

                                <div class="col-md-6">
                                    <input id="lastname" type="text" class="form-control" name="lastname"
                                           value="{{ old('lastname') }}">

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('phonetype') || $errors->has('phonenumber') ? ' has-error' : '' }}">
                                <label class="col-sm-4 control-label"> Phonenumber</label>
                                <div class="col-sm-6">
                                    <div class="phone-list">
                                        <div class="input-group phone-input" {{$errors->has('phonetype') ? ' has-error' : '' }}>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown"
                                                        aria-expanded="false"><span
                                                            class="type-text"> <a
                                                                style="text-decoration: none; color: #636b6f;"
                                                                class="changeType" id="" href="javascript:;"
                                                                data-type-value="+31">NL +31 (0) </a></span> <span
                                                            class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a class="changeType" id="" href="javascript:;"
                                                           data-type-value="+31">NL +31 (0) </a></li>
                                                    <li><a class="changeType" href="javascript:;" data-type-value="+32">BE +32 (0) </a></li>
                                                    <li><a class="changeType" href="javascript:;" data-type-value="+49">GER +49 (0) </a></li>
                                                    <li><a class="changeType" href="javascript:;" data-type-value="+49">UK +44 (0) </a></li>
                                                </ul>
                                            </span>
                                            <input type="hidden" name="phonetype" class="type-input" value=""/>
                                            <input type="text" name="phonenumber" class="form-control"
                                                   placeholder="6 12 345 678"/>
                                        </div>
                                        @if ($errors->has('phonetype') || $errors->has('phonenumber'))
                                            <span class="help-block">
                                                    <strong>{{$errors->first('phonetype')}}<br/>{{$errors->first('phonenumber')}}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('privacy') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="privacy"><p> I accept the <a href="{{asset("storage/privacy/Privacy policy.pdf")}}" download>Privacy Policy</a>.</p>
                                        </label>
                                        @if ($errors->has('privacy'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('privacy') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-md-8"></div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        <button type="submit" class="btn"
                                                style="background-color:#a51c4b; color: #ffffff;">
                                            Sign up
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(document.body).on('click', '.changeType', function () {
            $(this).closest('.phone-input').find('.type-text').text($(this).text());
            $(this).closest('.phone-input').find('.type-input').val($(this).data('type-value'));

        });
    </script>
@endsection

