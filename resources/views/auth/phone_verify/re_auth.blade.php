@extends('layout.app')

@section('content')
    <div class="container">
        @include('partials.flash-message')
        <div class="col-md-8 col-md-offset-2">
            <div style="padding-bottom: 10px;">
                <div class="text-center">
                    <img style="width:300px; height: 103px;" src="{{asset('storage/logo/logoBarabaz.png')}}"/>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #a51c4b; color: #ed7764;">
                    <h4 class="text-center" style="color: white; padding-top: 10px;">
                        Your secret verification code
                    </h4>
                </div>
                <div class="panel-body" style="text-align: center;">
                    @if(isset($error))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            The verification code you have entered is incorrect. Try again or request a new code.
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('re_authenticate') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="one_time_password" class="col-md-4 control-label">Verification code</label>

                            <div class="col-md-6">
                                <input id="one_time_password" type="number" class="form-control"
                                       name="one_time_password" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn" style="background-color:#a51c4b; color: #ffffff;">
                                    Verify
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <a href="{{route('re_authenticate_new_code')}}">Request new code</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection