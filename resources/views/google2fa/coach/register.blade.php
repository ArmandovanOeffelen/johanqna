@extends('layout.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.flash-message')
            <div class="col-md-8 col-md-offset-2">
                <div style="padding-bottom: 10px;">
                    <div class="text-center">
                        <img src="{{asset('storage/logo/logoBarabaz.png')}}"/>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #a51c4b; color: #ed7764;"><h4
                                class="text-center" style="color: white; padding-top: 10px;">Set up Google 2FA</h4>
                    </div>

                    <div class="panel-body" style="text-align: center;">
                        <div>
                            <p>1. Download the authenticator app for <a href="https://apps.apple.com/nl/app/google-authenticator/id388497605">IOS</a> or
                                <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=nl">ANDROID</a>
                            </p>
                            <p>
                                2. In the app click on the "+" to add a new item.
                            </p>
                            <p>
                                3. Scan the QR code or add the secret key you can find underneath.
                            </p>
                        </div>
                        <div>
                            <img src="{{ $QR_Image }}">
                        </div>
                        <p>Secret key: {{ $secret }}</p>
                        @if (!@$reauthenticating)
                        <p>You must set up your Google Authenticator app before continuing. You will be unable to login otherwise</p>
                            <div>
                                <a class="btn" style="background-color:#a51c4b; color: #ffffff;"
                                   href="{{route('coach_custom_signup_complete',['token' => $token])}}">Complete
                                    registration</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

@endsection