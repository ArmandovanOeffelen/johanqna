<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') - BARABAZ</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('sass/app.sass') }}" rel="stylesheet">


    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    {{--imports--}}
    <link rel="stylesheet" href="{{ asset('library/bootstrap3-dialog/css/bootstrap-dialog.css') }}">
    <script src="{{ asset('library/bootstrap3-dialog/js/bootstrap-dialog.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        console.log("__________                    .___ ________                .__               \n" +
            "\\______   \\ _____   ____    __| _/ \\______ \\   ____   _____|__| ____   ____  \n" +
            " |       _//     \\ /    \\  / __ |   |    |  \\_/ __ \\ /  ___/  |/ ___\\ /    \\ \n" +
            " |    |   \\  Y Y  \\   |  \\/ /_/ |   |    `   \\  ___/ \\___ \\|  / /_/  >   |  \\\n" +
            " |____|_  /__|_|  /___|  /\\____ |  /_______  /\\___  >____  >__\\___  /|___|  /\n" +
            "        \\/      \\/     \\/      \\/          \\/     \\/     \\/  /_____/      \\/");
    </script>
</head>
<body>
@guest
    @include('default.partials.default_nav')
@else
    @if(Auth::user()->isAdmin())
        @include('admin.partials.admin_nav')
    @elseif(Auth::user()->isManager())
        @include('coach.partials.hr_nav')
    @elseif(Auth::user()->isDefault())
        @include('default.partials.default_nav')
    @endif
@endguest

<main>
    <div id="app">
        {{-- --}}
        <div id="page-wrapper" style="height: 100vh; background: url({{asset('storage/logo/background-road.jpg')}})">
            <div class="container-fluid">
                <div class="row" id="main">
                    <div class="container" id="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@yield('templates')
<div id="footer">
    @include('partials.footer')
</div>
@yield('scripts')
</body>

</html>
