<nav>
    <div id="Navigation" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                @guest
{{--                    <a class="navbar-brand" href="{{view('auth.login')}}">--}}
{{--                        <img class="logo-nav" src="{{asset("storage/logo/logoBarabaz.png")}}" height="40"/>--}}
{{--                    </a>--}}
                @else
                    <a href="{{route('default_dashboard')}}">
                        <img class="logo-nav" src="{{asset("storage/company/logo/". Auth::user()->Company->CompanyLogo->logo_name)}}" height="40"/>
                    </a>
                @endguest
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-menubuilder">
                <ul class="nav navbar-nav navbar-right">
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                    @else
                        <li><a href="mailto:info@barabaz.com">Contact</a></li>
                        <li><a href="{{route('default_dashboard')}}">Dashboard</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</nav>