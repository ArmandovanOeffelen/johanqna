@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    <div class="col-md-12"><a href="{{route('default_dashboard')}}"><i class="fa fa-home"></i></a> &nbsp;\&nbsp; My results
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h3>Result comparison</h3>
    </div>
    <div class="col-md-6">
        <div class="text-right btn-padding-top">
            <a class="btn btn-primary" href="{{route('default_my_results',['id' => Auth::user()->id])}}">Back to my Results</a>
        </div>
    </div>
</div>
<div class="row" style="padding-top: 15px;">
    @foreach($results as $index => $result)
        <div class="col-md-4">
            <div class="text-justify">
                <h4>
                    {{$result->visit_date->format('d-m-Y')}}
                </h4>
            </div>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                @foreach($result->ResultValue as $sR)
                        <td>
                            @if(isset($resultsRangeData[$index][$sR->column_name]))
                                {{$resultsRangeData[$index][$sR->column_name]['range']}}
                            @else
                                {{$sR->column_name}}
                            @endif
                        </td>
                        <td>
                            {{$sR->value}}
                        </td>
                        <td>
                            @if(isset($resultsRangeData[$index][$sR->column_name]))
                                <i class="fa fa-map-marker"
                                   style="color: {{ ($resultsRangeData[$index][$sR->column_name]['indicator_color'] ?? '') }};"></i>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endforeach
</div>