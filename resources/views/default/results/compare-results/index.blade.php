@extends('layout.app')

@section('title', 'My Results')

@section('content')
    @include('default.results.compare-results.template.index')
@endsection