@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    {{--    todo:: fix breadcrumb--}}
    <div class="col-md-12"><a href="{{route('default_dashboard')}}"><i style="color: #a51c4b;" class="fa fa-home"></i></a> &nbsp;\&nbsp; My results
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <h4>All results</h4>
    </div>
    <div class="col-md-4">
        <div class="text-right">
            <button id="btn-compareResults" class="btn barabaz-button">Compare results</button>
        </div>
    </div>
</div>
<div class="container btn-padding-top">
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <ul class="nav nav-pills nav-stacked nav-static">
                @foreach($years as $year)
                    <li style="font-size: 17px;">
                        <b>{{$year}}</b>
                    </li>
                    <li><i>Screening results</i></li>
                    @foreach ($allResults as $indexResults => $results)
                        @if($results[0]['visit_date']->format('Y') === $year)

                            <li class="@if($indexResults === 0) active @endif">
                                <a href="{{"#screening-".$results[0]['visit_date']->format('d-m-Y')}}" data-toggle="tab">
                                    {{$results[0]['visit_date']->format('d-m-Y')}}
                                </a>
                            </li>
                        @endif
                    @endforeach

                    <li><i>Survey results</i></li>
                    @foreach($groupByDate as  $index => $date)
                        @if(\Carbon\Carbon::parse($index)->format('Y') === $year)
                        <li>
                            <a href="{{"#survey-".$index}}" data-toggle="tab">
                                {{$index}}
                            </a>
                        </li>
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
        <div class="col-md-10 col-sm-10">
            <div class="tab-content" class="tab-pane">
                @foreach ($allResults as $indexResults => $results)
                    <div id="screening-{{ $results[0]['visit_date']->format('d-m-Y')}}"
                         class="tab-pane @if($indexResults === 0) active in @endif">
                        <table class="table table-responsive">
                            <tbody>
                            @foreach($rangeCategories as $indexCat => $category)
                                    @if($category->name === "Physical vitality")
                                        <tr style="font-size: 17px; background-color: #f3f6e0;">
                                        <td style="color:#76b82a; "><b>{{$category->name}}</b></td>
                                    @elseif ($category->name === "Mental vitality")
                                        <tr style="font-size: 17px; background-color: #fee7d1;">
                                        <td style="color:#ef7d00; "><b>{{$category->name}}</b></td>
                                    @elseif ($category->name === "Emotional vitality")
                                        <tr style="font-size: 17px; background-color: #dfedfa;">
                                        <td style="color:#208cd2; "><b>{{$category->name}}</b></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($results as $indexSR => $result)
                                    @if(!is_null($result['indicator']))
                                        @if($result['range_category']['id'] === $category->id)
                                            <tr style="height: 35px;
                                            @if($category->name === "Physical vitality")
                                                    background-color:#f3f6e0;
                                            @elseif($category->name === "Mental vitality")
                                                    background-color: #fee7d1;
                                            @elseif ($category->name === "Emotional vitality")
                                                    background-color:#dfedfa;
                                            @endif">
                                                <td><i class="fa fa-{{$result['icon']}} fa-2x"></i>
                                                    &nbsp; {{$result['range']}}</td>
                                                @foreach($result['result_value'] as $value)
                                                    @if($result['range_column'] === $value['column_name'])
                                                        <td>{{$value['value']}}</td>
                                                    @endif
                                                @endforeach
                                                <td>{{$result['indicator']['name']}}</td>
                                                <td>
                                                    @if(!is_null($result['indicator_color']))
                                                        <span style="color:{{$result['indicator_color']}}">
                                                            <i class="fa fa-map-marker fa-2x"></i>
                                                        </span>
                                                    @else
                                                        &nbsp;
                                                    @endif
                                                </td>
                                                <td>
                                                    <a id="btn-ReadText" data-range_text="{{$result['range_id']}}"
                                                       style="font-size: 10px; cursor:pointer;">Read
                                                        information...</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                            @if(!is_null($result['personal_advice']))
                                <tfooter>
                                    <tr style="font-size: 17px;">
                                        <td><b>Personal advice</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><a id="btn-readPersonalAdvice"
                                               data-advice="{{$result['result_id']}}"
                                               style="font-size: 10px;">Read information...</a></td>
                                    </tr>
                                </tfooter>
                            @endif
                            </tbody>
                        </table>
                    </div>
                @endforeach
{{--                {{dd($groupByDate)}}--}}
                @foreach($groupByDate as  $index => $date)
                        <div id="survey-{{$index}}"
                             class="tab-pane @if($index === 0) active in @endif">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <td>Question</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Answer 1-10</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($groupByDate as $answers)
                                        @foreach($answers as $indexAnswer => $answer)
                                            @if($answer['created_at']->format('d-m-Y') === $index)
                                                <tr style="font-size: 17px;">
                                                    <td><b>{{$answer->Survey->question}}</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{$answer->answer}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
