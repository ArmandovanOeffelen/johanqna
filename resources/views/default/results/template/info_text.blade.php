<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$range['range_text_title']}}</h3>
            </div>
        </div>
        <div class="col-md-12">
            {!! $range['range_text'] !!}
        </div>
    </div>
    <div class="col-md-1"></div>
</div>