@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    {{--    todo:: fix breadcrumb--}}
    <div class="col-md-12"><a href="{{route('default_dashboard')}}"><i style="color: #a51c4b;" class="fa fa-home"></i></a>
    </div>
</div>
{{--TODO:: LOGO TO BE DECIDED--}}
{{--<div class="row">--}}
{{--    <div class="col-md-2"></div>--}}
{{--    <div class="col-md-8">--}}
{{--        <div style="padding-bottom: 10px;">--}}
{{--            <div class="text-center">--}}
{{--                <img src="{{asset('storage/logo/logoBarabaz.png')}}"/>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="col-md-2"></div>--}}
{{--</div>--}}
@if(isset($resultCount))
    @if($resultCount === 0)
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="panel panel-info panel-border-bottom-primary">
                    <div class="panel-heading">
                        <a class="link-primary" href="{{route('default_show_appointment_screen')}}">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <h3 class="announcement-text">Make appointment</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="panel panel-warning panel-border-bottom-warning">
                    <div class="panel-heading">
                        <a class="link-warning" href="{{route('default_show_survey')}}">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-question fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <h3 class="announcement-text">Questionnaire</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                    <strong>Please don't fill in the questionnaire yet!<br/>You will be asked to complete the
                        questionnaire during the
                        MYFIT@workcheck.</strong>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    @else
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="panel panel-info panel-border-bottom-primary">
                    <div class="panel-heading">
                        <a class="link-primary" href="{{route('default_my_results',[Auth::user()->id])}}">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-list fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <h3 class="announcement-text">My results</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="panel panel-warning panel-border-bottom-warning">
                    <div class="panel-heading">
                        <a class="link-warning" href="{{route('default_show_appointment_screen')}}">
                            <div class="row link-non-warning">
                                <div class="col-xs-6">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right link-non-primary">
                                    <h3 class="announcement-text">Make appointment</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="panel panel-danger panel-border-bottom-danger">
                    <div class="panel-heading">
                        <a class="link-danger" href="{{route('default_show_survey')}}">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-question fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <h3 class="announcement-text">Questionnaire</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                    <strong>Please don't fill in the questionnaire yet!<br/>You will be asked to complete the
                        questionnaire during the
                        MYFIT@workcheck.</strong>
                </div>
            </div>
        </div>
    @endif
@else
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <i class="fa fa-list fa-5x"></i>
                        </div>
                        <div class="col-xs-6 text-right">
                            <h4 class="announcement-text">My results</h4>
                        </div>
                    </div>
                </div>
                <a href="{{route('default_my_results',[Auth::user()->id])}}">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                {{__('buttons.go')}}
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <i class="fa fa-calendar fa-5x"></i>
                        </div>
                        <div class="col-xs-6 text-right">
                            <h4 class="announcement-text">Make appointment</h4>
                        </div>
                    </div>
                </div>
                <a href="{{route('default_show_appointment_screen')}}">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                {{__('buttons.go')}}
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <i class="fa fa-question fa-5x"></i>
                        </div>
                        <div class="col-xs-6 text-right">
                            <h4 class="announcement-text">Questionnaire</h4>
                        </div>
                    </div>
                </div>
                <a href="{{route('default_show_survey')}}">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                {{__('buttons.go')}}
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
            <strong>Please don't fill in the questionnaire yet!<br/>You will be asked to complete the questionnaire
                during the
                MYFIT@workcheck.</strong>
        </div>
    </div>
@endif