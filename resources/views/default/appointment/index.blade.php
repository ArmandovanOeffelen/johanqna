@extends('layout.app')

@section('title', 'Appointment')

@section('content')
    @include('default.appointment.template.index')
@endsection
