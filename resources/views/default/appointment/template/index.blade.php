@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    {{--    todo:: fix breadcrumb--}}
    <div class="col-md-12"><a href="{{route('default_dashboard')}}"><i style="color: #a51c4b;" class="fa fa-home"></i></a> &nbsp;\&nbsp; Make an appointment
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-lg-10 col-xs-10 col-sm-10">
        <h3>Make an appointment</h3>
    </div>
    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
</div>
<div class="row">
    <div class="col-md-10 col-lg-10 col-xs-10 col-sm-10">
        <p>Dear employee of <b>{{$company->name}}</b>,
            <br />
            You decided to participate in the {{$company->appointment_frame_name}}.
            <br />
            To make an appointment for yourself, you can choose a month, day and time.
            At this moment the agenda is on the current month. If you would like to make an appointment in another month, click on the arrows in the right or left corner of the month.
            Then click on a date that is colored blue, these are the available days.
            Finally you choose a time that suits you.
            The following steps speak for themselves.
            <br />
            <br />
            <b>Good luck!</b>
            <br />
            Team BARABAZ
        </p>
    </div>
    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
</div>
<div class="row">
    <div id="survey" class="col-lg-offset-4 col-md-offset-4 col-md-11 col-lg-11 col-xs-11 col-sm-11">
        {!! $company->appointment_frame !!}
    </div>
</div>
