
@if($dates->count() >= 2)
    <div class="row">
        <div class="col-md-9">
            <h3>Select dates to compare your results</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group{{$errors->has('date_1') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Select date #1</span>
                    <select name="date_1" class="form-control">
                        @foreach($dates as $date)
                            <option value="{{$date->id}}">{{$date->visit_date->format('d-m-Y')}}</option>
                        @endforeach
                    </select>
                </label>
                @if ($errors->has('date_1'))
                    <span class="help-block">{{ $errors->first('date_1') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group{{$errors->has('date_2') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Select date #2</span>
                    <select name="date_2" class="form-control">
                        @foreach($dates as $date)
                            <option value="{{$date->id}}">{{$date->visit_date->format('d-m-Y')}}</option>
                        @endforeach
                    </select>
                </label>
                @if ($errors->has('date_2'))
                    <span class="help-block">{{ $errors->first('date_2') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group{{$errors->has('date_3') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Select date #3</span>
                    <select name="date_3" class="form-control">
                        <option value="" selected>None</option>
                        @foreach($dates as $date)
                            <option value="{{$date->id}}">{{$date->visit_date->format('d-m-Y')}}</option>
                        @endforeach
                    </select>
                </label>
                @if ($errors->has('date_3'))
                    <span class="help-block">{{ $errors->first('date_3') }}</span>
                @endif
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-lg-1 col-xs-1 col-sm-1 col-md-1">

        </div>
        <div class="col-lg-10 col-xs-10 col-sm-10 col-md-10">
            <div class="text-center">
                <h4>You only have 1 result, there for you can not compare your results.</h4>
            </div>
        </div>
        <div class="col-lg-1 col-xs-1 col-sm-1 col-md-1">

        </div>
    </div>
@endif