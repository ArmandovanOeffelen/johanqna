<div class="row">
    <form method="POST" action="{{action('SurveyController@finishSurvey',[Auth::user()])}}">
        <table class="form-table table table-responsive table-condensed table-striped">
            <thead>
            <tr>
                <td></td>
                <td><i  style="color:darkred; font-size: 15px;" class="fa fa-frown"></i> </td>
                <td></td>
                <td></td>
                <td></td>
                <td><i style="color:orange; font-size: 15px;"  class="fa fa-meh fa-1x"></i> </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><i style="color:green; font-size: 15px;"  class="fa fa-smile fa-1x"></i> </td>
            </tr>
            <tr>
                <td><h4>{{__('survey.question')}}</h4></td>
                <td><h5>1</h5></td>
                <td><h5>2</h5></td>
                <td><h5>3</h5></td>
                <td><h5>4</h5></td>
                <td><h5>5</h5></td>
                <td><h5>6</h5></td>
                <td><h5>7</h5></td>
                <td><h5>8</h5></td>
                <td><h5>9</h5></td>
                <td><h5>10</h5></td>
            </tr>
            </thead>
            <tbody>
            @if(count($survey) === 0)
                <tr>
                    <td class="danger">{{__('messages.no_data_front_end',['model' => 'questions'])}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @else
                @foreach($survey as $index => $question)
                    <tr {{$errors->has("q.$question->id") ? 'style=color:#a94442; background-color:#a94442;' : '' }}>
                        <td>{{$question->question}}</td>
                        @for ($key = 1; $key <= 10; $key++)
                            <td><input type="radio" class="radio check" name="q[{{$question->id}}]"
                                       value="{{$key}}" {{ old("q.$question->id") == $key ? 'checked='.'"'.'checked'.'"' : '' }}>
                            </td>
                        @endfor
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! csrf_field() !!}
        @if($completed === 'completed')
            <button class="pull-right btn btn-success disabled" type="submit" disabled>{{__('buttons.submit')}}</button>
        @else
            <button class="pull-right btn btn-success" type="submit">{{__('buttons.submit')}}</button>
        @endif


    </form>
</div>