@include('partials.flash-message')
<div class="row" style="padding-bottom: 10px;">
    {{--    todo:: fix breadcrumb--}}
    <div class="col-md-12"><a href="{{route('default_dashboard')}}"><i style="color: #a51c4b;" class="fa fa-home"></i></a> &nbsp;\&nbsp; Questionnaire
    </div>
</div>
<div class="row">
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-9 col-lg-9 col-xs-9 col-sm-9">
        <h3>{{__('survey.survey_title')}}</h3>
        <small><b>{{__('survey.survey_sub_title')}}</b></small>
    </div>
</div>
<div class="row">
    @if($completed === 'completed')
        <div class="row">
            <div id="survey" class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-11 col-lg-11 col-xs-11 col-sm-11">
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                    <strong>You have completed the survey already today! Click <a href="{{route('default_my_results',[Auth::user()->id])}}">here</a> to go to your result page.</strong>
                </div>
            </div>
        </div>
    @endif
</div>
<div class="row">
    <div id="survey" class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-11 col-lg-11 col-xs-11 col-sm-11">
        @if(!$survey->count() >= 12)
            <div class="row">
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert"><b>×</b></button>
                    <p>We have not found 12 active questions in the system. Please contact the administrators.</p>
                </div>
            </div>
        @else
            @include('default.survey.template.form')
        @endif
    </div>
</div>