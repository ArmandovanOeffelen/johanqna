<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h3>{{__('tutorial.title')}}</h3>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Step 1</h4>
        <p>
            Fill in the result name, this is for the title on the result pages.
        </p>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p>
        <h4>Step 2</h4>
        Fill in the Column name (Excel), this is so that we can find the right column in the Excel.
        When you're done press the "Save changes" button underneath of the form. to save the column. You'll need to do
        this in order
        to start adding new range criteria.
        <p style="size : 15px;"><b>NOTE: MAKE SURE THAT YOUR'E FILLING IN THE CORRECT EXCEL COLUMN, PLEASE CONSIDER
                COPYING THE COLUMN NAME!!!
                EXAMPLE: BMI</b></p>
        </p>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p>
        <h4>Step 3 (can be done later)</h4>
        3.1 &nbsp; Select the Range category ( This will be used to categorise the Ranges on the result pages. <br/>
        3.2 &nbsp; Fill in the Font Awesome icon text with only the icon name, we will do the rest. Check the sources
        for all the font awesome icons. <br/>
        3.3 &nbsp; Fill in the range text fields, this is for additional information about the Result name for example
        an informational text about BMI.<br/>
        3.4 &nbsp; Click the button "Save changes" to save your recent work.<br/>
        <p style="size : 15px;"><b>NOTE*: We are not using Font Awesome pro icons, so if you select one that needs the
                pro version it won't be displayed. Click <a target="_blank" href="https://fontawesome.com/icons?d=gallery">here</a> for an overview with all the font awesome icons
            </b>
        </p> <p style="size : 15px;"><b>NOTE**: When you're making changes to the core of the Range and start adding criteria and refresh the page or something else
                Youre core Range change won't be saved!
            </b>
        </p>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p>
        <h4>Step 4</h4>
        When you succesfully have added a Range you can now specify your Range criteria. This can be found at the bottom
        of the page. When you have added multiple Range indicators to show on your result page you'll see multiple (clickable) tabs.
        When you're ready to start making Range criteria click the "Add new criteria" link at the bottom of the page.
        A new row will now appear.
        <br/>
        4.0.1 &nbsp; Adding a Range criteria where the gender and age don't matter for the result leave the gender select on 'Any' and the age min-max on 0. <br/>
        4.0.2 &nbsp; When you're done creating the Range, click the 'check' button at the end of the row. It will now be saved in the database.
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p>
        <h4>Step 4.1</h4>
        When you want to change your previous Range criteria click the 'pen' button in the correct row.
        <br/>
        4.1.1 &nbsp; Make the changes as you wish. <br/>
        4.1.2 &nbsp; When you're done editting the Range, click the 'check' button at the end of the row. It will now be saved in the database.
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p>
        <h4>Step 4.2</h4>
        When you want to delete the Range criteria you just made.
        <br/>
        4.1.1 &nbsp; click the 'X' button <br/>
        </p> <p style="size : 15px;"><b>NOTE: Deleting the Range criteria can not be undone.
            </b>
        </p>
    </div>
    <div class="col-md-1"></div>
</div>