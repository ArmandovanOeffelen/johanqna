@extends('layout.app')

@section('title', 'Vragen toevoegen')

@section('content')
    <div class="row">
        <form method="POST" action="{{action('SurveyController@createMultipleQuestion')}}">
            <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
                        <h3>Meerdere vragen toevoegen</h3>
                        @if($questionCount >= 12)
                            <small class="text-danger">
                                Momenteel zijn er 11 of meer vragen toegevoegd aan het systeem. Als u nieuwe vragen toevoegd dan worden deze automatisch in de wachtrij gezet.
                                Zodra u vragen verwijderd worden de vragen die in de wachtrij staan geactiveerd.
                            </small>
                        @endif
                    </div>
                    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
                        <div class="pull-right btn-padding-top">
                            <a href="{{route('admin_show_survey_dashboard')}}" class="btn btn-primary">back to overview</a>
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_1') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_1" value="{{ old('q_1') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_1'))
                                <span class="help-block"> {{ $errors->first('q_1') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_2') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_2" value="{{ old('q_2') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_2'))
                                <span class="help-block"> {{ $errors->first('q_2') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_3') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_3" value="{{ old('q_3') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_3'))
                                <span class="help-block"> {{ $errors->first('q_3') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_4') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_4" value="{{ old('q_4') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_4'))
                                <span class="help-block"> {{ $errors->first('q_4') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_5') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_5" value="{{ old('q_5') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_5'))
                                <span class="help-block"> {{ $errors->first('q_5') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_6') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_6" value="{{ old('q_6') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_6'))
                                <span class="help-block"> {{ $errors->first('q_6') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_7') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_7" value="{{ old('q_7') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_7'))
                                <span class="help-block"> {{ $errors->first('q_7') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_8') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_8" value="{{ old('q_8') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_8'))
                                <span class="help-block"> {{ $errors->first('q_8') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_9') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_9" value="{{ old('q_9') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_9'))
                                <span class="help-block"> {{ $errors->first('q_9') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_10') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_10" value="{{ old('q_10') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_10'))
                                <span class="help-block"> {{ $errors->first('q_10') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_11') ? 'has-error': '' }}" >
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_11" value="{{ old('q_11') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_11'))
                                <span class="help-block"> {{ $errors->first('q_11') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_12') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_12" value="{{ old('q_12') ?? ''  }}">
                            </label>
                            @if ($errors->has('q_12'))
                                <span class="help-block"> {{ $errors->first('q_12') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>

                {!! csrf_field() !!}
            </div>

            <div class="row">
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                    <div class="col-md-7 col-lg-7 col-xs-7 col-sm-7">
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                        <div class="pull-right" style="padding-top: 15px;">
                            <button class=" btn btn-success" type="submit">{{__('buttons.submit')}}</button>
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
            </div>
        </form>
    </div>
@endsection
