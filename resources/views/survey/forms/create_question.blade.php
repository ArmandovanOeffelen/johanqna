@extends('layout.app')

@section('title', 'Vraag toevoegen')

@section('content')
    <div class="row">
        <form method="POST" action="{{action('SurveyController@createSingleQuestion')}}">
            <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
                        <h3>Vraag toevoegen</h3>
                        @if($questionCount >= 11)
                            <small class="text-danger">
                                At this moment you have 11 questions added.
                                The questions you're adding are going to be added to the queue.
                            </small>
                        @endif
                    </div>
                    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
                        <div class="pull-right btn-padding-top">
                            <a href="{{url('/survey/overview')}}" class="btn btn-primary">Back to survey overview</a>
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-sm-1 col-md-1">
                    </div>
                    <div class="col-xs-9 col-lg-9 col-sm-9 col-md-9">
                        <div class="form-group {{$errors->has('q_1') ? 'has-error': '' }}">
                            <label class="control-label checkbox">
                                <span>Vraag</span>
                                <input class="pull-right form-control" type="text" name="q_1">
                            </label>
                            @if ($errors->has('q_1'))
                                <span class="help-block"> {{ $errors->first('q_1') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-2 col-lg-2 col-sm-2 col-md-2">
                    </div>
                </div>
                {!! csrf_field() !!}
            </div>

            <div class="row">
                <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
                <div class="col-md-7 col-lg-7 col-xs-7 col-sm-7">
                </div>
                <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                    <div class="pull-right" style="padding-top: 15px;">
                        <button class=" btn btn-success" type="submit">{{__('buttons.submit')}}</button>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
            </div>
        </form>
    </div>
@endsection
