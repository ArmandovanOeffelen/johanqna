@include('partials.flash-message')
<div class="row">
    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <h3>Overzicht Enquete vragen</h3>
        @if($questionCount >= 10)
            <small class="text-danger">
                Momenteel zijn er 10 of meer vragen toegevoegd aan het systeem. Als u nieuwe vragen toevoegd dan worden deze automatisch in de wachtrij gezet.
                Zodra u vragen verwijderd worden de vragen die in de wachtrij staan geactiveerd.
            </small>
        @endif
    </div>
    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
        <div class="pull-right btn-padding-top">
            <a href="{{url('/survey/overview')}}" class="btn btn-primary">Terug naar Enquete overzicht</a>
            <a href="{{url('/survey/form/multiple')}}" class="btn btn-primary">Voeg vragen toe (11)</a>
            <a href="{{url('/survey/form/single')}}" class="btn btn-primary">Voeg vragen toe (1)</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"></div>
    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
        <div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr style="font-weight: bold">
                        <td>
                            #
                        </td>
                        <td>
                            Vraag
                        </td>
                        <td>
                            In wachtrij
                        </td>
                        <td class="pull-right">
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($questions) === 0)
                        <tr class="warning">
                            <td>Er zijn momenteel nog geen CSV bestanden gegenereerd.
                            </td>
                            <td></td><td></td><td></td>
                        </tr>
                    @else
                        @foreach($questions as $index => $question)
                            <tr>
                                <td>{{$question->question_id}}</td>
                                <td>{{$question->question}}</td>
                                <td>@if($question->in_queue ==1 )Ja @else Nee @endif</td>
                                <td>
                                    <button data-question="{{$question}}" id="delete_question" class="pull-right btn btn-danger">Verwijder</button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $questions->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"></div>
</div>