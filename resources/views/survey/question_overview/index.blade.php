@extends('layout.app')

@section('title', 'Overzicht alle vragen')

@section('content')
    @include('survey.question_overview.template.index')
@endsection
@section('scripts')
@parent

<script>
    $(function () {

        var templates = {
            {{--createOrganisation: `@include('back.crm.forms.organisation.create')`,--}}
            {{--createCrmProject: `@include('back.crm.forms.createCrmProject')`,--}}
            delete: '<span>Weet u zeker dat u deze vraag wilt verwijderen?</span>',
            loading: '<span>Even geduld...</span>'
        };

        $('#content')

            .on('click', '#delete_question', function () {
                var objectData = $(this).data('question');
                console.log(objectData);
                BootstrapDialog.show({
                    title: 'Verwijder vraag',
                    message: templates.delete,
                    nl2br: false,
                    buttons: [{
                        label: 'Annuleren',
                        action: function action(dialogRef) {
                            dialogRef.close();
                        }
                    }, {
                        id: 'danger',
                        label: 'Verwijder',
                        cssClass: 'btn-danger',
                        autospin: true,
                        action: function action(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            $.ajax({
                                type: 'delete',
                                url: '{{ url('ajax/template/survey/question/{0}/delete')}}'.format(objectData.id),
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function success(data) {
                                    dialog.close();

                                    $('#content').html(data);
                                },
                                error: function error(error) {
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    dialog.getButton('Delete').stopSpin();

                                    if (error.responseJSON.error != null) {
                                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                        dialog.close();
                                    }
                                }
                            });
                        }
                    }]
                });
            })
        //Activate
            .on('click', '#buttonCreateProject', function () {
                BootstrapDialog.show({
                    title: 'Add project',
                    message: templates.createCrmProject,
                    nl2br: false,
                    onshown: function(dialog) {
                        dialog.$modal.find('.js-organisation-select').selectize({});
                        dialog.$modal.find('.js-project-status-select').selectize({});
                        dialog.$modal.find('.js-project-manager-select').selectize({});
                    },
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        id: 'submit',
                        label: 'Save',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function (dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            $.ajax({
                                type: 'post',
                                url: '{{ url('admin/ajax/template/crm/overview')}}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    //project details
                                    name: dialog.getModalContent().find('input[name="name"]').val(),
                                    project_manager: dialog.getModalContent().find('select[name="project_manager"]').val(),
                                    organisation: dialog.getModalContent().find('select[name="organisation"]').val(),
                                    project_status: dialog.getModalContent().find('select[name="project_status"]').val(),
                                    start_date: dialog.getModalContent().find('input[name="start_date"]').val(),
                                    end_date: dialog.getModalContent().find('input[name="end_date"]').val(),
                                    project_summary: dialog.getModalContent().find('textarea[name="project_summary"]').val(),
                                    //contactPerson
                                    contactperson_name: dialog.getModalContent().find('input[name="contactperson_name"]').val(),
                                    contactperson_email: dialog.getModalContent().find('input[name="contactperson_email"]').val(),
                                    contactperson_phone: dialog.getModalContent().find('input[name="contactperson_phone"]').val(),
                                },
                                success: function (data) {
                                    console.log(data);
                                    dialog.close();

                                    $('#content').html(data.html);

                                },
                                error: function (error) {
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    dialog.getButton('submit').stopSpin();

                                    if (error.responseJSON.error != null) {
                                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                        dialog.close();
                                    } else {
                                        dialog.setMessage(error.responseJSON.html);
                                    }

                                }
                            });

                        }
                    }]
                });
            });
    });

    $(function () {

        // Extended disable function
        $.fn.extend({
            disable: function(state) {
                return this.each(function() {
                    var $this = $(this);
                    if($this.is('input, button, textarea, select'))
                        this.disabled = state;
                    else
                        $this.toggleClass('disabled', state);
                });
            }
        });

    });

    // String.format
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined'
                    ? args[number]
                    : match
                    ;
            });
        };
    }
</script>
@endsection
