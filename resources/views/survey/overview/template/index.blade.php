@include('partials.flash-message')
<div class="row">
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <h3>Enquete overview</h3>
    </div>
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <div class="pull-right">
            <button id="generate" class="btn btn-warning">Genereer CSV van vandaag</button>
            <a href="{{url('/survey/form/multiple')}}" class="btn btn-primary">Voeg vragen toe (11)</a>
            <a href="{{url('/survey/form/single')}}" class="btn btn-primary">Voeg vragen toe (1)</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="pull-left">
                    <h4>Laatst 10 toegevoegde vragen</h4>
                    @if($questionCount >= 11)
                        <small class="text-danger">
                            Momenteel zijn er 11 of meer vragen toegevoegd aan het systeem. Als u nieuwe vragen toevoegd dan worden deze automatisch in de wachtrij gezet.
                            Zodra u vragen verwijderd worden de vragen die in de wachtrij staan geactiveerd.
                        </small>
                    @endif
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 45px;">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <td>
                                #
                            </td>
                            <td>
                                Question
                            </td>
                            <td>
                                Active
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questions as $index => $question)
                            <tr>
                                <td>{{$question->question_id}}</td>
                                <td>{{$question->question}}</td>
                                <td>
                                    @if($question->is_active == 1)
                                        Active
                                    @else
                                        Not active
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="pull-right">
                    <a href="{{url('/survey/questions/all')}}" class="btn btn-primary">Bekijk alle vragen</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="pull-left">
                    <h4>Laatste 10 genereerde CSV bestanden</h4>
                    <small class="text-danger" @if($questionCount >= 11) style="padding-top: 25px; " @endif>

                    </small>
                </div>
            </div>
        </div>
        <div class="row responsive-padding">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <td>
                            Datum
                        </td>
                        <td>

                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($csv) === 0)
                        <tr class="warning">
                            <td>Er zijn momenteel nog geen CSV bestanden gegenereerd.
                            </td>
                            <td></td>
                        </tr>
                    @else
                        @foreach($csv as $index2 => $csvs)
                            <tr>
                                <td>
                                    {{$csvs->csv_date}}
                                </td>
                                <td class="pull-right">
                                    <button id="deleteCSV" data-csv="{{$csvs}}" class="pull-right btn btn-danger">Verwijder</button>
                                    <a id="downloadCSV" href="{{route('downloadFile',$csvs)}}" data-csv="{{$csvs}}" class="pull-right btn btn-success">Download</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div><div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="pull-right">
                    <a href="{{url('/survey/csv-overview')}}" class="btn btn-primary">Bekijk alle bestanden</a>
                </div>
            </div>
        </div>
    </div>
</div>