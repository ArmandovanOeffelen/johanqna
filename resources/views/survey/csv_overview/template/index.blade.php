@include('partials.flash-message')
<div class="row">
    <div class="col-md-1 col-lg-1 col-xs-1 col-sm-1"></div>
    <div class="col-md-5 col-lg-5 col-xs-5 col-sm-5">
        <h3>Overzicht gegenereerde CSV bestanden</h3>
    </div>
    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
        <div class="pull-right btn-padding-top">
            <a href="{{url('/survey/overview')}}" class="btn btn-primary">Terug naar Enquete overzicht</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"></div>
    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
        <div class="row">
            <div class=" col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr style="font-weight: bold">
                        <td>
                            #
                        </td>
                        <td>
                            Datum
                        </td>
                        <td class="pull-right">
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($csvs) === 0)
                        <tr class="warning">
                            <td>Er zijn momenteel nog geen CSV bestanden gegenereerd.
                            </td>
                            <td></td>
                        </tr>
                    @else
                        @foreach($csvs as $index => $csv)
                            <tr>
                                <td>{{ ($csvs->perPage() * ($csvs->currentPage() - 1)) + $index + 1 }}</td>
                                <td>{{$csv->csv_date}}</td>
                                <td>
                                    <button id="deleteCSV" data-csv="{{$csv}}" class="pull-right btn btn-danger">Verwijder</button>
                                    <a id="downloadCSV" href="{{route('downloadFile',$csv)}}" data-csv="{{$csv}}" class="pull-right btn btn-success">Download</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $csvs->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"></div>
</div>