Beste {{$invite->first_name}},<br />
<br />
Hier is uw admin registratie formulier.
<br />
<a href="{{url($invite->invite_link)}}">Klik hier</a> om de uitnodiging te accepteren en te registreren.
<br />
<br />
<br />

<i>Heeft u niet gevraagd om deze mail? Neem dan contact op met dennis@pio-barabaz.com</i>

<br />
<br />
<br />
Met vriendelijke groeten,
<br />
BARABAZ Robot.