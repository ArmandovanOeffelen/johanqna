Beste {{$invite->first_name}} {{$invite->last_name}},<br />
<br />
Tijdens het laatste contact moment hebben wij gevraagd of u lid wou worden bij BARABAZ als Coach.<br />
<br />
<a href="{{url($invite->invite_link)}}">Klik hier</a> om de uitnodiging te accepteren en te registreren.
<br />
<br />
<br />

<i>Heeft u niet gevraagd om deze mail? Neem dan contact op met <a href="mailto:info@barabaz.com">info@barabaz.com</a></i>

<br />
<br />
<br />
Met vriendelijke groeten,
<br />
BARABAZ Robot.