<div class="row">
    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <div class="text-left">
            <p style="padding-left: 25px;" class="text-left">© RmndDesign 2019</p>
        </div>
    </div>
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <div class="text-right">
            <a  style="padding-right: 25px;" href="{{asset("storage/privacy/Privacy policy.pdf")}}" download>Privacy Policy</a>
        </div>
    </div>
</div>