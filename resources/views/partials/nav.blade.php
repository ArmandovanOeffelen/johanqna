<header>
    <nav>
        <div class="logo">
            <img src="{{asset("storage/logo/logoBarabaz.png")}}"/>
        </div>
        <div class="menu">
            <ul>
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                @else
                    <li><a href="{{url('/survey/overview')}}">Enquete overzicht</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>
</header>