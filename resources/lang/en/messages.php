<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lines are for the messages we want to display on pages like flashmessages., this file contains
    | messages that we need to display to the user.
    |
    */

    'no_data' => 'We could not find any :model in the system. If you just have added :model we recommend refreshing the page.',
    'no_data_front_end' => 'We could not find any :model in the system.',
    'question_overview' => 'Question overview',
    'add_questions' => 'Add questions',
    'you_dont_belong_here' => 'Hey :user! You do not have access to this page.',
    'succes_added' => 'You have succesfully added :model!',
    'succes_updated' => 'You have succesfully updated :model!',
    'succes_deleted' => 'You have succesfully deleted a :model!',
    'deleted_succes_no_queue' => 'You have succesfully deleted a question. But we couldn\'t find a question in the queue to activate. We recommend adding a new question, so that the survey can be filled in properly.',
    'deleted_succes_moved_queue' => 'You have succesfully deleted a question. We removed a question from the queue, and activated it for you!',
    'delete' => 'Are you sure you want to delete this :model?',
    'delete_user' => 'Are you sure you want to delete this user? This user can have results. This can impact company results!',
    'already_completed' => 'You have already completed this :model today!',
    'survey_succes' => 'You have succesfully Completed the survey. Here are your results!',
    'loading' => 'Loading..',
    'mark_resolved' => 'You have marked the :email as resolved!',
    'range_collection_overlaps' => '#:a overlaps with #:b',
    'no_range_indicators' => 'Currently there are no Range indicators, before you can add a Range you have to add a Range indicator.'
];
