<?php

return [

    /**
        Lang var example.
        before . = filename
        after . = var name.
        {{__('buttons.go')}}
        {{__('dennis.company_overview')}}
        {{__('buttons.check_model', ['model' => 'employee'] )}}
    */

    //Sentence
    'var' => 'English sentence or word.',

    //Buttons
    'check_employees' => 'Check employees',
    'go' => 'Go there',


    //Messages
    'company_overview' => 'Company overview',
    'employee_overview' => 'Employee overview',



    //MISC please note location
    // Admin > company> template > index 

];
