<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lines are for the buttons, this file contains
    | messages that we need to display to the user.
    |
    */

    'add' => 'Add :model',
    'make_something' => 'Make :model',
    'edit' => 'Edit :model',
    'delete' => 'Delete',
    'add_multiple' => 'Add :model',
    'go' => 'Go there',
    'submit' => 'Submit',
    'check' => 'Check',
    'check_model' => 'Check :model',
    'cancel' => 'Cancel',
    'close' => 'Close',
];
