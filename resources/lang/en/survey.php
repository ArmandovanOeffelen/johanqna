<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lines are for the survey, this file contains
    | messages that we need to display to the user.
    |
    */

    'dashboard' => 'Questionnaire dashboard',
    'question_overview' => 'Question overview',
    'add_questions' => 'Add questions (11)',
    'question' => 'Question',
    'is_active' => 'Active',
    'in_queue' => 'In queue',
    'survey_title' => 'Questionnaire',
    'survey_sub_title' => 'Please don\'t fill in the questionnaire yet! You will be asked to complete the questionnaire during the MYFIT@workcheck.',
    'delete_less_then_ten_questions' => 'The Questionnaire question is not deleted, you can\'t have less than 10 questions'

];
