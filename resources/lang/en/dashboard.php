<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lines are for the dashboard, this file contains
    | messages that we need to display to the user.
    |
    */

    'survey_dashboard' => 'Survey dashboard',
    'user_dashboard' => 'User dashboard',
    'screening_dashboard' => 'Screening settings',
    'screening_dashboard_coach' => 'Screening dashboard',
    'company_dashboard' => 'Company overview',
    'log_dashboard' => 'Log Dashboard',
    'sms_dashboard' => 'Sms Log dashboard',
    'name' => 'name',
    'email' => 'email',
    'has_text' => 'contains_text',
    'visit_date' => 'date visited',

];