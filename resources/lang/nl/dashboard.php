<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lines are for the dashboard, this file contains
    | messages that we need to display to the user.
    |
    */

    'survey_dashboard' => 'Survey test',
    'user_dashboard' => 'User dashboard',
    'screening_dashboard' => 'Screening settings',
    'company_dashboard' => 'Company overview',

];
