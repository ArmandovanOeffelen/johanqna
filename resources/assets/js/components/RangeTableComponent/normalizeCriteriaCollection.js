export default function (criterias) {

    const parseValue = (type, options) => {
        if ('gender' === type) {
            return options.value;
        }

        return options.min + '-' + options.max;
    };

    const normalizeCriteria = type => {
        const criteria = criterias[type];

        return {
            type: type,
            value: parseValue(type, criteria)
        }
    };

    return [
        normalizeCriteria('self'),
        normalizeCriteria('gender'),
        normalizeCriteria('age')
    ];
}
