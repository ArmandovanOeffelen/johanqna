export default function (criteriaCollection) {

    const getDefault = type => {
        if ('gender' === type) {
            return {value: 'any'};
        }

        return {min: 0, max: 0};
    };

    const parseValue = (type, value) => {
        if ('gender' === type) {
            return {value: value};
        }

        const split = value.split('-');

        if (split.length !== 2) {
            console.warn('Failed to split value "' + value + '" of type "' + type + '".');

            return getDefault(type);
        }

        return {
            min: Number(split[0]),
            max: Number(split[1]),
        }
    };

    const findCriteria = type => {
        for ([, criteria] of Object.entries(criteriaCollection)) {
            if (criteria.type === type) {
                return criteria;
            }
        }

        return null;
    };

    const denormalizeCriteria = type => {
        const criteria = findCriteria(type);

        const normalized = null === criteria
            ? getDefault(type)
            : parseValue(type, criteria.value);

        return {
            ...normalized,
            valid: true
        };
    };

    return {
        self: denormalizeCriteria('self'),
        gender: denormalizeCriteria('gender'),
        age: denormalizeCriteria('age')
    };
}
