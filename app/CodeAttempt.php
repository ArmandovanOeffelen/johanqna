<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeAttempt extends Model
{
    protected $table = 'code_attempt';
}
