<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendCsv extends Mailable
{
    use Queueable, SerializesModels;

    public $testfilepath;
    public $tempfilename;
    public $filename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($testfilepath,$tempfilename,$filename)    {
        $this->testfilepath = $testfilepath;
        $this->tempFileName = $tempfilename;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $correctFilePath = "$this->testfilepath$this->tempFileName";
        return $this->from("programmamakersjohan@gmail.com","PMJohan")
            ->view('email.sendCSV')
            ->subject("Er is een enquete ingevuld.")
            ->attach($correctFilePath,[
                'as' => $this->filename,
                'mime' => 'text/csv'
            ]);
    }
}
