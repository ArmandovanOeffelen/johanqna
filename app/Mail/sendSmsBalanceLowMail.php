<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendSmsBalanceLowMail extends Mailable
{
    use Queueable, SerializesModels;


    public $balance;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($balance)
    {
        $this->balance = $balance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.sendBalanceLow',[
            'balance' => $this->balance
        ])->from('noreply@pio-barabaz.com','BARABAZ SYSTEM');
    }
}
