<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCompanyEmployeeInviteMail extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $invite;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company,$invite)
    {
        $this->company = $company;
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@pio-barabaz.com','BARABAZ')
            ->subject('U heeft een uitnodiging ontvangen om lid te worden op BARABAZ!')
            ->with([
                'invite' => $this->invite,
                'company' => $this->company
            ])
            ->view('email.sendUserInvite');
    }
}
