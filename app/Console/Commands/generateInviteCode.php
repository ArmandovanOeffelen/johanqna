<?php

namespace App\Console\Commands;

use App\CompanyEmployeeInvite;
use App\User;
use Illuminate\Console\Command;

class generateInviteCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:invite {--email=} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make an invite for the admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->confirm("Are you sure you want to create an invite?")) {
            return $this->info("Command finished, user denied.");
        }
        $email = $this->option('email');
        $name = $this->option('name');

        // if no email was passed to the option, prompt the user to enter the email
        if (!$email) $email = $this->ask('what is the user\'s email?');
        if (!$name) $name = $this->ask('What is the user\'s name?');

        $infoArray = [
            'email' => $email,
            'firstname' => $name,
            'lastname' => null,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendAdminInvite($infoArray);

        $this->info("You will recieve an email with an invite shortly!");
    }
}
