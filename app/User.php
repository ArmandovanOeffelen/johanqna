<?php

namespace App;

use App\Model\Company;
use App\Notifications\MailResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname', 'email', 'password','is_coach','company_id','phonenumber','lastname','type','google2fa_secret','country_code','activated','logged_success'
    ];

    protected $casts = [
        'code_valid' => 'integer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','google2fa_secret'
    ];

    const ADMIN_TYPE = 'admin';
    const MANAGER_TYPE = 'manager';
    const DEFAULT_TYPE = 'default';

    /**
     * Ecrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);
    }

    /**
     * Decrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function getGoogle2faSecretAttribute($value)
    {
        return decrypt($value);
    }


    public function passwordSecurity()
    {
        return $this->hasOne('App\PasswordSecurity');
    }


    public function isAdmin()
    {
        return $this->type === self::ADMIN_TYPE;
    }

    public function isManager()
    {
        return $this->type === self::MANAGER_TYPE;
    }

    public function isDefault(){
        return $this->type === self::DEFAULT_TYPE;
    }

    public function Company()
    {
        return $this->belongsTo(Company::class);
    }


    public function createAdminInvite(Array $infoArray)
    {
        $dateString = Carbon::now()->toDateString();
        $invite_token = $dateString.uniqid();
        $link = '/signup/'.$invite_token.'/show-form/';

        $invite = new CompanyEmployeeInvite();
        $invite->company_id = null;
        $invite->first_name = $infoArray['firstname'];
        $invite->last_name = null;
        $invite->email = $infoArray['email'];
        $invite->invite_token = $invite_token;
        $invite->invite_link = $link;
        $invite->save();

        return $invite;
    }

    public function sendInvite(Array $infoArray)
    {

        $invite  = $this->createInvite($infoArray);

        return Mail::to($infoArray['email'])->send(new sendAdminInviteMail($invite));
    }

    public static function setUserActivated($user)
    {
        $user->activated = 1;
        $user->update();

        return;
    }

    public static function setUserLogged($user){
        $user->logged_success = 1;
        $user->update();

        return;
    }

    public function SmsLog()
    {
        return $this->hasMany(SmsLog::class);
    }


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }
//2c222ea6d13a49a3d813e29cca0bdd6263f54522824e89a8300c79be94f4e42d
}
