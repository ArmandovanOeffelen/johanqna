<?php


namespace App;


use App\Http\Controllers\SmsController;
use App\Http\Controllers\SmsLogController;
use App\Mail\SendCompanyEmployeeInviteMail;
use App\Mail\sendSmsBalanceLowMail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class SMSHelper
{

    public static function sendVerificationCode($user,$action)
    {
        $code = SMSHelper::generateSMSCode();

        $phoneNumber = SMSHelper::createPhoneNumber($user->phonenumber, $user->country_code);

        if (App::environment() === 'production') {
            if (SMSHelper::storeSMSCode($user, $code) === true) {

                //If testing locally, just open the DB. Were not sending a sms because costly reasons.
                $sms = Nexmo::message()->send([
                    'to' => $phoneNumber,
                    'from' => 'BARABAZ',
                    'text' => 'Use this code for BARABAZ verification: ' . $code
                ]);

                $response = $sms->getResponseData();

                $balance = $response['messages'][0]['remaining-balance'];
                SMSHelper::balanceChecker($balance);


                if ($response['messages'][0]['status'] == 0) {
//
                    $data = SMSHelper::createLogArray($user,$action);
//
                    $smsLogger = new SmsLogController();
                    $smsLogger->store($data);
//
                    $ytbn = new SmsController();
                    $ytbn->update($balance);

                    return true;
                } else {

                    return false;
                }

            }
        } else {
            return true;
        }

        return false;
    }

    protected static function storeSMSCode($user, $code)
    {
        $user->code = $code;
        $user->update();

        return true;
    }

    protected static function generateSMSCode()
    {
        $code = rand(111111, 999999);

        return $code;
    }

    protected static function createPhoneNumber($phonenumber, $countryCode)
    {
        return $correctPhoneNumber = $countryCode . $phonenumber;
    }

    public static function compareCodes($code, $code2)
    {
        $code2 = (int)$code2;
        $code = (int)$code;
        if ($code !== $code2) {
            return false;
        }

        return true;
    }

    public static function checkIfLoggedSucces($user)
    {
        if ($user->logged_success === 0) {
            $action = "Login not succesfull, new code send.";
            SMSHelper::sendVerificationCode($user,$action);

            return redirect()->route('login_verify_login', [
                'user' => $user
            ]);
        }

    }

    public static function trimPhoneNumber($inCorrectPhoneNumber)
    {

        $firstNumber = substr($inCorrectPhoneNumber, 0, 1);

        if($firstNumber === "0"){

            $phoneNumber = substr($inCorrectPhoneNumber, 1);

            return $phoneNumber;
        } else {

            $phoneNumber = $inCorrectPhoneNumber;

            return $phoneNumber;
        }
    }


    public static function createLogArray($user,$action){

        return [
            'user_id' =>  $user->id,
            'company_id' => (int)$user->company_id,
            'action' => $action
        ];
    }

    public static function balanceChecker($balance)
    {
        if($balance < 3){
            return Mail::to('arrieality@gmail.com')->send(new sendSmsBalanceLowMail($balance));
        }
        return '';
    }
}