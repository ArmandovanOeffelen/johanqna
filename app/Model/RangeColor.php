<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RangeColor extends Model
{
    protected $table = 'range_color';


    public function RangeIndicator()
    {
        return $this->hasMany(RangeIndicator::class);
    }
}
