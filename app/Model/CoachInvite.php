<?php

namespace App\Model;

use App\Mail\SendCompanyManagerInviteMail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CoachInvite extends Model
{
    protected $table = 'company_coach_invite';



    public function createInvite(Array $infoArray)
    {

        $dateString = Carbon::now()->toDateString();

        $invite_token = $dateString.uniqid();


        $link = '/signup/'.$invite_token."/show-form";

        $invite = new CoachInvite();
        $invite->first_name = $infoArray['firstname'];
        $invite->last_name = $infoArray['lastname'];
        $invite->email = $infoArray['email'];
        $invite->invite_token = $invite_token;
        $invite->invite_link = $link;
        $invite->save();

        return $invite;
    }

    public function sendInvite(Array $infoArray)
    {

        $invite  = $this->createInvite($infoArray);

        return Mail::to($infoArray['email'])->send(new SendCompanyManagerInviteMail($invite));
    }
}
