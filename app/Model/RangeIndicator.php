<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RangeIndicator extends Model
{
    protected $table = 'range_indicator';

    public function RangeColor()
    {
        return $this->belongsTo(RangeColor::class);
    }
}
