<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model
{
    protected $table = 'sms_log';

    protected $fillable = ['user_id','action','company_id'];

    protected $dates = ['created_at'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

}
