<?php

namespace App\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RangeCriteriaCollection extends Model
{
    protected $table = 'range_criteria_collection';

    public function range()
    {
        return $this->belongsTo(Range::class);
    }

    public function rangeIndicator()
    {
        return $this->belongsTo(RangeIndicator::class);
    }

    public function rangeCriterias(): HasMany
    {
        return $this->hasMany(RangeCriteria::class);
    }

    /**
     * @return RangeIndicator
     */
    public function getRangeIndicator(): RangeIndicator
    {
        return $this->rangeIndicator;
    }

    /**
     * @return RangeCriteria[]
     */
    public function getAllCriteria(): iterable
    {
        return $this->rangeCriterias;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function has(string $type): bool
    {
        return null !== $this->get($type);
    }

    /**
     * @param string $type
     *
     * @return RangeCriteria|null
     *
     * @deprecated
     */
    public function get(string $type): ?RangeCriteria
    {
        return $this->getCriteria($type);
    }

    /**
     * @param string $type
     *
     * @return RangeCriteria|null
     */
    public function getCriteria(string $type): ?RangeCriteria
    {
        foreach ($this->rangeCriterias as $rangeCriteria) {
            if ($type === $rangeCriteria->type) {
                return $rangeCriteria;
            }
        }

        return null;
    }
}
