<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'result';

    protected $fillable = ['email', 'company_code', 'visit_date'];
    protected $dates = ['visit_date'];

    public function ResultPersonalAdvice()
    {
        return $this->hasOne(ResultPersonalAdvice::class);
    }

    public function ResultValue()
    {
        return $this->hasMany(ResultValue::class);
    }

    /**
     * @return iterable|ResultValue[]
     */
    public function getValues(): iterable
    {
        return $this->ResultValue;
    }

    /**
     * @param string $columnName
     *
     * @param bool $caseSensitive
     * @return ResultValue|null
     */
    public function getValue(string $columnName, bool $caseSensitive = false): ?ResultValue
    {
        if (! $caseSensitive) {
            $columnName = strtolower($columnName);
        }

        foreach ($this->getValues() as $value) {
            $valueColumnName = $caseSensitive ? $value->column_name : strtolower($value->column_name);

            if ($valueColumnName === $columnName) {
                return $value;
            }
        }

        return null;
    }
}
