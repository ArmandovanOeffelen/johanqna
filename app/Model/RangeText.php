<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RangeText extends Model
{
    protected $table = 'range_text';

    public function Range()
    {
        return $this->belongsTo(Range::class);
    }
}
