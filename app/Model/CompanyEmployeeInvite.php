<?php

namespace App;

use App\Mail\SendCompanyEmployeeInviteMail;
use App\Mail\sendAdminInviteMail;
use App\Model\CoachInvite;
use App\Model\Company;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class CompanyEmployeeInvite extends Model
{
    protected $table = 'company_employee_invite';

    public function createInvite(Company $company,Array $infoArray)
    {

        $dateString = Carbon::now()->toDateString();
        $invite_token = $dateString.uniqid();
        $link = '/signup/'.$company->CompanyToken->token.'/show-form/'.$invite_token;

        $invite = new CompanyEmployeeInvite();
        $invite->company_id = $company->id;
        $invite->first_name = $infoArray['firstname'];
        $invite->last_name = $infoArray['lastname'];
        $invite->email = $infoArray['email'];
        $invite->invite_token = $invite_token;
        $invite->invite_link = $link;
        $invite->save();

        return $invite;
    }

    public function sendInvite(Company $company,Array $infoArray)
    {

        $invite  = $this->createInvite($company,$infoArray);

        return Mail::to($infoArray['email'])->send(new SendCompanyEmployeeInviteMail($company,$invite));
    }

    public function deleteInvite($email)
    {
        $getInvite = CompanyEmployeeInvite::where('email', $email)->first();

        $getInvite->delete();

        return ;
    }

    public function createAdminInvite(Array $infoArray)
    {
        $dateString = Carbon::now()->toDateString();
        $invite_token = $dateString.uniqid();
        $link = '/signup/'.$invite_token.'/show-form/';

        $invite = new CoachInvite();
        $invite->first_name = $infoArray['firstname'];
        $invite->last_name = "";
        $invite->email = $infoArray['email'];
        $invite->invite_token = $invite_token;
        $invite->invite_link = $link;
        $invite->save();

        return $invite;
    }

    public function sendAdminInvite(Array $infoArray)
    {

        $invite  = $this->createAdminInvite($infoArray);

        return Mail::to($infoArray['email'])->send(new sendAdminInviteMail($invite));
    }


}
