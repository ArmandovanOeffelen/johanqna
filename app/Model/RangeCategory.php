<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RangeCategory extends Model
{
    protected $table = 'range_category';

    public function Range()
    {
        return $this->hasMany(Range::class);
    }

}
