<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResultPersonalAdvice extends Model
{
    protected $table = 'result_personal_advice';


    public function Result()
    {
        return $this->belongsTo(Result::class);
    }
}
