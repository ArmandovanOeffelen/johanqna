<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RangeCriteria extends Model
{
    const TYPE_SELF = 'self';
    const TYPE_GENDER = 'gender';
    const TYPE_AGE = 'age';

    protected $table = 'range_criteria';

    public function rangeCriteriaCollection()
    {
        return $this->belongsTo(RangeCriteriaCollection::class);
    }


    public function toString()
    {
        return $this->value;
    }
}
