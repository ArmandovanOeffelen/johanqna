<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Range extends Model
{
    use SoftDeletes;

    protected $table = 'range';

    public function criteriaCollections(): HasMany
    {
        return $this->hasMany(RangeCriteriaCollection::class);
    }

    public function RangeText()
    {
        return $this->hasOne(RangeText::class);
    }

    public function RangeCriteriaCollection()
    {
        return $this->hasMany(RangeCriteriaCollection::class);
    }

    public function RangeCategory()
    {
        return $this->belongsTo(RangeCategory::class);
    }

    /**
     * @return RangeCriteriaCollection[]
     */
    public function getCriteriaCollections(): iterable
    {
        return $this->criteriaCollections;
    }
}
