<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    public function CompanyLogo()
    {
        return $this->hasOne(CompanyLogo::class);
    }

    public function User()
    {
        return $this->hasMany(User::class);
    }

    public function CompanyToken()
    {
        return $this->hasOne(CompanyToken::class);
    }

    public function createCompanyToken($company){
        $rand = rand(00000000,99999999);
        $uniqid = uniqid($rand);
        $uniqidx = uniqid($uniqid);

        $token = substr_replace($rand,$uniqid,3);
        $token = $token.$uniqidx;

        $companyToken = new CompanyToken();
        $companyToken->company_id =  $company->id;
        $companyToken->token = $token;
        $companyToken->save();

        return $companyToken;
    }

}
