<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyLogo extends Model
{
    protected $table = 'company_logo';

    protected $fillable = ['logo_path','logo_name'];

    public function Company()
    {
        return $this->hasOne(Company::class);
    }

}
