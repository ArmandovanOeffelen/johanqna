<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResultUnknownEmail extends Model
{
    protected $table = 'result_unknown_email';
}
