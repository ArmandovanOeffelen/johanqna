<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyToken extends Model
{
    protected $table = 'company_token';


    public function Company(){
        return $this->belongsTo(Company::class);
    }
}
