<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResultValue extends Model
{


    protected $table = 'result_value';

    public function Result()
    {
        return $this->belongsTo(Result::class);
    }
}
