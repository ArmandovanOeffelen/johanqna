<?php

namespace App\Http\Controllers;

use App\Model\Company;
use App\Model\Result;
use App\SmsLog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function showSingleCompany(Company $company)
    {

        $allVisitDates = Result::select('visit_date')->where('company_id', $company->id)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();

        $smsx = SmsLog::where('company_id', $company->id)->orderBy('created_at', 'ASC')->get();

        $smsYears = $smsx->map(function ($value, $key) {
            return (int)$value->created_at->format('Y');
        })->values()->unique();


        $smsPerMonths = $this->smsCostsPerMonth($company);

        return view('admin.company.company.index',[
            'years' => $years,
            'company' => $company,
            'smsYears' => $smsYears,
            'smsPerMonths' => $smsPerMonths,
        ]);
    }

    public function showAllEmployeesForCompany(Company $company)
    {
        $employees = User::where('company_id',$company->id)->simplePaginate(10);

        return view('admin.company.company.employees.index',[
            'company' => $company,
            'employees' => $employees
        ]);
    }

    public function smsCostsPerMonth($company)
    {
        $allSms = SmsLog::where('company_id', $company->id)->orderBy('created_at', 'ASC')->get();

        $r = [];
        foreach ($allSms as $item => $items){

            $r[$items->created_at->format('Y')][$items->created_at->format('M')][] = $items;
        }

        $smsPerMonth = [];
        foreach($r as $year => $years) {
            foreach ($years as $months => $logs) {
                $smsPerMonth[$year][$months] = count($logs);
            }
        }

        return $smsPerMonth;
    }
}
