<?php

namespace App\Http\Controllers;

use App\Sms;
use App\SmsLog;
use Illuminate\Http\Request;

class SmsLogController extends Controller
{
    public function store($data){

        return SmsLog::create([
            'user_id' => $data['user_id'],
            'company_id' => $data['company_id'],
            'action' => $data['action']
        ]);
    }

    public function index()
    {

        $sms = Sms::where('number','adb54ee8')->first();

        $smsLogs = SmsLog::orderBy('created_at','ASC')->get();

        return response()->view('admin.logs.sms.index',[
            'sms' => $sms,
            'smsLogs' => $smsLogs
        ]);
    }
}
