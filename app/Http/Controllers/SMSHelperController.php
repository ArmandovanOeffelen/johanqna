<?php

namespace App\Http\Controllers;

use App\CodeAttempt;
use App\SMSHelper;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SMSHelperController extends Controller
{

    use AuthenticatesUsers{
        logout as performLogout;
    }

    public function completeLogin(Request $request)
    {
        $user = Auth::user();

        $result = SMSHelper::compareCodes($user->code,$request->one_time_password);

        $codeAttempt = CodeAttempt::where('user_id',$user->id)->first();
        if($result === false){
                $amount = $codeAttempt->amount + 1;

                $codeAttempt->amount = $amount;
                $codeAttempt->update();

            if($amount >= 5){

                $codeAttempt->delete();
                $user->logged_success = 0;
                $user->code_valid = 0;
                $user->update();

                $this->performLogout($request);
                session()->flash('warning','You\'re logged out because you have exceed the limit of 5 wrong codes ');
                return redirect('/login');
            }

            return view('auth.phone_verify.login',[
                'user' => $user,
                'error' => "test"
            ]);
        }

        if(!is_null($codeAttempt)){
            $codeAttempt->delete();
        }

        if($user->activated === 0){
            $user->activated = 1;
        }

        $user->code_valid = 1;
        $user->update();

        return HomeController::typeReturn();
    }

    public function requestNewCode($userId)
    {
        $user = User::findOrFail($userId);
        $action = "new code request";
        if(SMSHelper::sendVerificationCode($user,$action) === true){

            session()->flash('success','you will recieve a new verification code shortly!');
            return view('auth.phone_verify.login',[
                'user' => $user
            ]);
        }
    }

    public function showAuthenticationForm()
    {
        $user = Auth::user();
        $action = "Re-Authentication";
        if(SMSHelper::sendVerificationCode($user,$action) === true){

            session()->flash('danger','Something went wrong, please re-authenticate. You will recieve a code shortly.');
            return view('auth.phone_verify.re_auth');
        }

    }

    public function requestCode()
    {
        $user = Auth::user();
        $action = "new code request";
        if(SMSHelper::sendVerificationCode($user,$action) === true){

            session()->flash('success','You will recieve a new code shortly.');
            return view('auth.phone_verify.re_auth');
        }
    }

    public function reAuthenticate(Request $request)
    {
        $user = Auth::user();
        $result = SMSHelper::compareCodes($user->code,$request->one_time_password);

        $user->code_valid = 1;
        $user->update();

        session()->flash('success','Succesfully re-authenticated!');
        return HomeController::typeReturn();
    }

}
