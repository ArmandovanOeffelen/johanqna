<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Model\Company;
use App\Model\Range;
use App\Model\RangeCategory;
use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\RangeIndicatorHelper;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CoachController extends Controller
{
    public function __construct()
    {
        $this->middleware('is_manager');
    }

    public function dashboard(){


        return view('coach.index');
    }

    public function showCompany(Company $company)
    {

        $allVisitDates = Result::select('visit_date')->where('company_id', $company->id)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();



        return view('coach.company.index',[
            'years' => $years,
            'company' => $company
        ]);
    }

    public function showCompanies()
    {
        $companies = Company::where('email','!=','dev@rmnddesign.com')->simplePaginate(10);
        return view('coach.company.companies.index',[
            'companies' => $companies
        ]);
    }

    public function showCompanyEmployees(Company $company)
    {
        $employees = User::where('company_id',$company->id)->where('type','!=','admin')->simplePaginate(10);

        return view('coach.company.employees.index',[
            'company' => $company,
            'employees' => $employees
        ]);
    }

    public function showEmployee(User $user)
    {

        $results = Result::where('email',$user->email)->orderBy('visit_date','DESC')->withCount('ResultPersonalAdvice')->get();

        return view('coach.employee.single_employee.index',[
            'results' => $results,
            'user' => $user
        ]);
    }

    public function showEmployees()
    {

        $employees = User::where('type','default')->simplePaginate(10);
         return view('coach.employee.index',[
             'employees' => $employees
         ]);
    }

    public function showScreeningDashboard()
    {

        return view('coach.screening_dashboard.index');
    }


    public function showAllResults(User $user)
    {

        $ranges = Range::all();

        $results = Result::where('email', $user->email)->orderBy('visit_date', 'ASC')->get();

        $allVisitDates = Result::select('visit_date')->where('email', $user->email)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();

        $surveyAnswers = Answer::all()->where('interviewee_id', $user->id);


        $answerDates = $surveyAnswers->map(function ($value, $key) {
            return $value->created_at;
        })->values()->unique();

        $groupByDate = [];

        foreach ($surveyAnswers as $item) {
            $groupByDate[$item->created_at->format('d-m-Y')][] = $item;
        }

        $rangeCategories = RangeCategory::all();


        $allResults = array();
        foreach ($results as $index => $result) {
            foreach ($ranges as $range) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);
                $allResults[$index][] = [
                    'visit_date' => $result->visit_date,
                    'result' => $result->id . ' (' . $result->email . ')',
                    'result_id' => $result->id,
                    'result_value' => $result->ResultValue,
                    'range' => $range->name,
                    'range_id' => $range->id,
                    'range_category' => $range->RangeCategory,
                    'range_column' => $range->column_name,
                    'personal_advice' => $result->ResultPersonalAdvice,
                    'indicator' => $rangeIndicator ? $rangeIndicator : null,
                    'indicator_color' => $rangeIndicator ? $rangeIndicator->RangeColor->color_code : null,
                    'icon' => $range->icon,

                ];
            }
        }
        return view('coach.employee.single_employee.result.all-results.index', [
            'user' => $user,
            'allResults' => $allResults,
            'years' => $years,
            'answers' => $surveyAnswers,
            'groupByDate' => $groupByDate,
            'answerDates'=> $answerDates,
            'rangeCategories' => $rangeCategories
        ]);
    }


    public function showCompanyResults(Company $company,$year)
    {
        $start = date("$year-01-01");
        $end = date("$year-12-31");

        $ranges = Range::all();
        $results = Result::whereBetween('visit_date', [$start, $end])->where('company_id', $company->id)->get();



        if ($this->getCount($results) < 15) {

            session()->flash('warning',"Sorry there are less then 15 unique results found within the selected period. There for we can not show you the results");
            return view('coach.company.results.index',[
                'year' => $year,
                'company' => $company,
                'chartData' => null,
            ]);

        }

        $chartData = $this->getChartData($ranges->all(), $results->all());

        return view('coach.company.results.index', [
            'company' =>$company,
            'year' => $year,
            'chartData' => [
                'maxTicks' => count($results),
                'data' => $chartData
            ]
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $results
     *
     * @return int
     */
    private function getCount(Collection $results): int
    {
        return $results->map(function (Result $result) {
            return $result->user_id;
        })->values()->unique()->count();
    }

    /**
     * @param Range[] $ranges
     * @param Result[] $results
     *
     * @return array
     */
    private function getChartData(array $ranges, array $results)
    {
        $labels = [];
        $dataSets = [];

        foreach ($ranges as $range) {
            $labels[] = $range->name;
        }

        foreach ($results as $result) {
            foreach ($ranges as $rangeIndex => $range) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);

                if (null === $rangeIndicator) {
                    continue;
                }

                if (! isset($dataSets[$rangeIndicator->id])) {
                    $dataSets[$rangeIndicator->id] = [
                        'label' => $rangeIndicator->name,
                        'data' => array_map(function () {
                            return 0;
                        }, $ranges),
                        'backgroundColor' => $rangeIndicator->RangeColor->color_code,
                    ];
                }

                $dataSets[$rangeIndicator->id]['data'][$rangeIndex]++;
            }
        }

        return [
            'labels' => $labels,
            'datasets' => array_values($dataSets)
        ];
    }


}
