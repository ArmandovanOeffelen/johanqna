<?php

namespace App\Http\Controllers;

use App\Model\CoachInvite;
use App\Model\Company;
use App\SMSHelper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachProcessInviteController extends Controller
{
    public function registrationForm()
    {

        $parameters = \Request::segment(2);


        return view('auth.register_coach', [
            'token' => $parameters
        ]);
    }

    public function register(Request $request, $token)
    {

        $this->validator($request);

        $invite = $this->getInvite($token);

        $phoneNumber = SMSHelper::trimPhoneNumber($request->phonenumber);

        if ($this->tokenCheck($invite, $token) == false) {

            session()->flash('warning', 'De geheime code is niet correct. Gebruik a.u.b. de link die u in uw mail gevonden heeft om te registreren.');
            return response()->redirectToAction('CoachProcessInviteController@registrationForm', [
                'token' => $token
            ]);
        }

        if ($this->emailCheck($invite, $request->email) === false) {
            session()->flash('warning', 'Het email adress wat u probeert te gebruiken is niet correct. Gebruik a.u.b. het email adres waar u de uitnodiging op heeft ontvangen.');
            return response()->redirectToAction('CoachProcessInviteController@registrationForm', [
                'token' => $token
            ]);
        }

        if ($this->inviteCheck($invite) == false) {
            session()->flash('warning', 'De uitnodiging is reeds al geaccepteerd. Was u dit niet? Neem a.u.b. contact op met de administrator.');
            return response()->redirectToAction('CoachProcessInviteController@registrationForm', [
                'token' => $token
            ]);
        }

        $user = $this->create($request,$phoneNumber);

        $action = "coach register code";
        if (SMSHelper::sendVerificationCode($user,$action) === true) {

            return view('auth.phone_verify.verify_coach', [
                'token' => $token,
                'user' => $user
            ]);
        } else {
            $request->flash();
            session()->flash('warning', 'something went horribly wrong.');
            return view('auth.register_coach', [
                'token' => $token
            ]);
        }

    }

    public function validator($request)
    {
        $message = [
            'privacy.required' => 'You must agree with the Privacy Policy when signing up.'

        ];
        return $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phonetype' => 'required',
            'phonenumber' => 'required|digits_between:9,15',
            'privacy' => 'required'
        ],$message);
    }

    public function completeRegistration(Request $request, $userId, $token)
    {
        //Getting invite
        $invite = $this->getInvite($token);

        $user = User::findOrFail($userId);

        if (SMSHelper::compareCodes($user->code, $request->one_time_password) !== true) {
            session()->flash('danger', 'Your verification code is incorrect. Try again or request a new verification code.');
            return view('auth.phone_verify.verify_coach', [
                'user' => $user,
                'token' => $token,
                'error' => 'Your verification code is incorrect. Try again or request a new verification code.',
            ])->with('danger', 'Your verification code is incorrect. Try again or request a new verification code.');
        }

        $this->acceptInvite($invite);


        User::setUserActivated($user);


        if ($user->activated !== 1) {
            session()->flash('danger', 'Something went horribly wrong. Please contact the administrators of this system with the error code: acti-50062');
            return view('auth.phone_verify.verify', [
                'user' => $user,
                'error' => 'Your verification code is incorrect. Try again or request a new verification code.',
            ])->with('danger', 'Your verification code is incorrect. Try again or request a new verification code.');
        }


        $this->guard()->login($user);

        User::setUserLogged($user);

        session()->flash('succes', 'You have succesfully signed up as a coach.');
        return response()->redirectToAction('CoachController@dashboard');

    }

    public function create($request,$phoneNumber)
    {
        return User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'type' => 'manager',
            'is_coach' => 1,
            'password' => bcrypt($request['password']),
            'google2fa_secret' => null,
            'country_code' => $request['phonetype'],
            'phonenumber' => $phoneNumber,
            'activated' => 0,
            'code' => null,
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    /**
     * @param $invite
     * @param $token
     *
     * @return bool
     */
    private function tokenCheck($invite, $token)
    {
        if ($invite->invite_token === $token) {

            return true;
        }

        return false;
    }

    private function inviteCheck($invite)
    {
        if ($invite->invite_accepted === 1) {
            return false;
        }
        return true;
    }

    private function emailCheck($invite, $email)
    {

        if ($invite->email === $email) {

            return true;
        }
        return false;
    }

    private function acceptInvite($invite): CoachInvite
    {
        $invite->invite_accepted = 1;
        $invite->update();
        return $invite;
    }

    private function getInvite($token)
    {

        $invite = CoachInvite::where('invite_token', $token)->first();

        return $invite;
    }

//
//    public function uselessCode()
//    {
//
//
//        // Initialise the 2FA class
//        $google2fa = app('pragmarx.google2fa');
//
//        // Save the registration data in an array
//        $registration_data = $request->all();
//
//        // Add the secret key to the registration data
//        $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();
//
//
//        $request->session()->flash('registration_data', $registration_data);
//
//        // Generate the QR image. This is the image the user will scan with their app
//        // to set up two factor authentication
//        $QR_Image = $google2fa->getQRCodeInline(
//            config('app.name'),
//            $registration_data['email'],
//            $registration_data['google2fa_secret']
//        );
//
//        // Pass the QR barcode image to our view
//        return view('google2fa.coach.register', [
//                'token' => $token,
//                'QR_Image' => $QR_Image,
//                'secret' => $registration_data['google2fa_secret']
//            ]
//        );
//    }
}
