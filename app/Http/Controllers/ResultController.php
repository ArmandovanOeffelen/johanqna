<?php

namespace App\Http\Controllers;

use App\Imports\ResultImport;
use App\Model\Company;
use App\Model\Range;
use App\Model\Result;
use App\Model\ResultUnknownEmail;
use App\Model\ResultValue;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Maatwebsite\Excel\Facades\Excel;

class ResultController extends Controller
{
    public const EMAIL_COLUMN = 'email';
    public const VISIT_DATE_COLUMN = 'Visit Date';
    public const GENDER_COLUMN = 'Gender';
    public const AGE_COLUMN = 'Age';

    public const BASE_COLUMNS = [
        self::EMAIL_COLUMN,
        self::VISIT_DATE_COLUMN,
        self::GENDER_COLUMN,
        self::AGE_COLUMN
    ];

    public function showForm()
    {

        $ranges = Range::count();

        return view('admin.form.screening_dashboard.excel.upload.index', [
            'base_columns' => self::BASE_COLUMNS,
            'ranges' => $ranges
        ]);
    }

    public function showFlaggedResultsOverview()
    {
        $results = ResultUnknownEmail::simplePaginate(10);

        return view('coach.screening_dashboard.flagged_results.index', [
            'results' => $results
        ]);
    }

    public function showFlaggedResultsOverviewAdmin()
    {
        $results = ResultUnknownEmail::simplePaginate(10);

        return view('admin.screening_dashboard.flagged_results.index', [
            'results' => $results
        ]);
    }

    public function showResultsNoAdvice()
    {
        $allResults = Result::withCount('ResultPersonalAdvice')->get();

        $tempResultCollection = collect();

        foreach ($allResults as $index => $item) {
            if ($item->result_personal_advice_count === 0) {
                $tempResultCollection->push($item);
            }
        }
        $results = $tempResultCollection;

        return view('coach.screening_dashboard.results_no_advice.index', [
            'results' => $results
        ]);

    }

    public function importExcel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'excel' => 'required|mimes:xls,xlsx',
        ]);

        if ($validator->fails()) {
            $ranges = Range::count();

            $request->flash();
            return view('admin.form.screening_dashboard.excel.upload.index', [
                'ranges' => $ranges
            ])->withErrors($validator)->render();
        }

        $columnsToRead = array_merge(self::BASE_COLUMNS, $this->getColumnsFromRanges());

        $file = $request->file('excel');

        $pages = Excel::toArray(new ResultImport, $file);

        if (count($pages) !== 1) {
            throw new \RuntimeException('Expected exactly a single page.');
        }

        $rows = $pages[0];

        $headersToRead = $this->getHeader($rows[0], $columnsToRead);
        $rows = $this->getRows(array_slice($rows, 1), $headersToRead);
        $rows = $this->filterRows($rows);

        $unknownEmails = [];

        foreach ($rows as $index => $row) {
            $success = $this->createResult($row);

            if ($success) {
                continue;
            }

            $unknownEmails[] = $row['email'];
        }

        foreach (array_unique($unknownEmails) as $email) {
            $unknownEmail = new ResultUnknownEmail();

            $unknownEmail->excel_name = $file->getClientOriginalName();
            $unknownEmail->email = $email;

            $unknownEmail->save();
        }

        session()->flash('success', 'The excel is imported.');
        return redirect()->back();
    }

    /**
     * @return string[]
     */
    private function getColumnsFromRanges(): array
    {
        $ranges = Range::select('column_name')->get();

        $columns = [];
        foreach ($ranges->toArray() as $range) {
            $columns[] = $range['column_name'];
        }

        return $columns;
    }

    /**
     * @param array $row
     *
     * @return bool
     *
     * @throws \Exception
     */
    private function createResult(array $row)
    {
        $email = $row['email'];

        $user = $this->getUser($email);

        if (null === $user) {
            return false;
        }

        $result = new Result();

        $result->company_id = $user->company_id;
        $result->user_id = $user->id;
        $result->email = $email;
        $result->visit_date = Carbon::make(Date::excelToDateTimeObject($row['Visit Date']));

        $result->save();

        foreach ($row as $column => $value) {
            if ('email' === $column || 'Visit Date' === $column) {
                continue;
            }

            $resultValue = new ResultValue();

            $resultValue->result_id = $result->id;
            $resultValue->column_name = $column;
            $resultValue->value = $value;

            $resultValue->save();
        }

        return true;
    }

    /**
     * @param string $email
     *
     * @return User|null
     */
    private function getUser(string $email): ?User
    {
        $user = User::select('id', 'company_id')->where('email', $email)->first();

        if ($user === null) {
            return null;
        }

        return $user;
    }

    /**
     * @param array $row
     * @param array $columnsToRead
     *
     * @return array
     */
    private function getHeader(array $row, array $columnsToRead): array
    {
        $headers = [];

        // Traverse through the headers and ....
        foreach ($row as $index => $value) {
            if (! in_array($value, $columnsToRead)) {
                continue;
            }

            $headers[] = [
                'index' => $index,
                'value' => $value
            ];
        }

        return $headers;
    }

    /**
     * @param array $rows
     * @param array $headersToRead
     *
     * @return array
     */
    private function getRows(array $rows, array $headersToRead): array
    {
        $result = [];

        foreach ($rows as $rowIndex => $row) {
            foreach ($headersToRead as $header) {
                $column = $header['value'];

                $result[$rowIndex][$column] = $row[$header['index']];
            }
        }

        return array_values($result);
    }

    /**
     * @param array $rows
     *
     * @return array
     */
    private function filterRows(array $rows): array
    {
        $filtered = array_filter($rows, function (array $row) {
            $email = $row[self::EMAIL_COLUMN];
            $visitDate = Carbon::make(Date::excelToDateTimeObject($row[self::VISIT_DATE_COLUMN]));

            if (null === $email) {
                return false;
            }

            $existingResults = Result::select('id')
                ->where('email', $email)
                ->where('visit_date', $visitDate)
                ->limit(1)
                ->get();

            if (! $existingResults->isEmpty()) {
                return false;
            }

            return true;
        });

        return array_values($filtered);
    }
}
