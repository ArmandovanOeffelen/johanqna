<?php

namespace App\Http\Controllers;

use App\CompanyEmployeeInvite;
use App\Model\Company;
use App\Model\CompanyToken;
use App\SMSHelper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OpenInviteController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showForm($companyToken)
    {

        return view('auth.register_no_invite', [
            'companyToken' => $companyToken,
        ]);
    }

    public function register(Request $request, $companyToken)
    {
        $this->validator($request);
        $phoneNumber = SMSHelper::trimPhoneNumber($request->phonenumber);

        $company = $this->findCompany($companyToken);

        $user = $this->create($request,$company,$phoneNumber);

        $action = "Open invite verification code";
        if(SMSHelper::sendVerificationCode($user,$action) === true){

            return view('auth.phone_verify.verify',[
                'company' => $company,
                'user' => $user
            ]);
        } else {
            $request->flash();
            session()->flash('warning','something went horribly wrong.');
            return view('auth.register_no_invite',[
                'companyToken' => $companyToken
            ]);
        }

    }

    public function completeRegistration(Request $request,$company,$userId)
    {

        $user = User::findOrFail($userId);
        if(SMSHelper::compareCodes($user->code,$request->one_time_password) !== true){

            session()->flash('danger','Your verification code is incorrect. Try again or request a new verification code.');
            return view('auth.phone_verify.verify',[
                'company' => $company,
                'user' => $user,
                'error' => 'Your verification code is incorrect. Try again or request a new verification code.',
            ])->with('danger','Your verification code is incorrect. Try again or request a new verification code.');
        }

        User::setUserActivated($user);

        if($user->activated !== 1){
            session()->flash('danger','Something went horribly wrong. Please contact the administrators of this system with the error code: acti-50062');
            return view('auth.phone_verify.verify',[
                'company' => $company,
                'user' => $user,
                'error' => 'Your verification code is incorrect. Try again or request a new verification code.',
            ])->with('danger','Your verification code is incorrect. Try again or request a new verification code.');
        }

        $this->guard()->login($user);


        User::setUserLogged($user);

        session()->flash('succes','Succes! '. $user->name .' U have registered to BARABAZ');

        return response()->redirectToAction('DefaultController@dashboard');

    }

    public function requestNewCode($company,$userId)
    {
        $user = User::findOrFail($userId);
        if(SMSHelper::sendVerificationCode($user) === true){

            session()->flash('success','you will recieve a new verification code shortly!');
            return view('auth.phone_verify.verify',[
                'company' => $company,
                'user' => $user
            ]);
        } else {
            session()->flash('warning','something went horribly wrong.');
            return view('auth.register_no_invite',[
                'company' => $company
            ]);
        }
    }

    public function validator($request)
    {

        $message = [
            'privacy.required' => 'You must agree with the Privacy Policy when signing up.'

        ];
        return $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phonetype' => 'required',
            'phonenumber' => 'required|digits_between:9,15',
            'privacy' => 'required'
        ],$message);
    }

    public function create($request, $company,$phoneNumber)
    {


        return User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'type' => 'default',
            'is_coach' => 0,
            'company_id' => $company->id,
            'google2fa_secret' => null,
            'country_code' => $request['phonetype'],
            'phonenumber' => $phoneNumber,
            'activated' => 0,
            'code'  => null,
            'password' => bcrypt($request['password']),
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    protected function compareCodes($code,$code2){
        $code2 = (int)$code2;
        if($code !== (int)$code2){
            return false;
        }

        return true;
    }


    protected function findCompany($companyToken){
        $companyTokenCompany = CompanyToken::where('token',$companyToken)->first();

        $company = Company::findOrFail((int)$companyTokenCompany->company_id);
        return $company;
    }

}
