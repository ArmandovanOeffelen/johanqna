<?php

namespace App\Http\Controllers;

use App\Model\Range;
use App\Model\RangeColor;
use http\Env\Response;
use Illuminate\Http\Request;

class RangeConfigController extends Controller
{

    public function __construct()
    {
        $this->middleware(['is_admin']);
    }


    public function showForm()
    {

        $rangeIndicators = Range::all();
        if(count($rangeIndicators) == 0){

            return redirect()->route('admin_controller_screening_show_ranges_overview')->with("danger",__('messages.no_range_indicators'));
        }


        return view('admin.form.screening_dashboard.range_config.create.index',[
            'rangeIndicators' => $rangeIndicators
        ]);
    }
}
