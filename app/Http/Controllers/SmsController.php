<?php

namespace App\Http\Controllers;

use App\Sms;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function update($balance){
        $allSms = Sms::all();
        $sms = $allSms->first();

        $sms->balance = $balance;
        $sms->update();

        return $sms;
    }
}
