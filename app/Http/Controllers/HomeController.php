<?php

namespace App\Http\Controllers;

use App\Answer;
use App\CodeAttempt;
use App\Http\Controllers\Auth\LoginController;
use App\SMSHelper;
use App\Survey;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        $action = "Login verification";

        if(SMSHelper::sendVerificationCode($user,$action) === true){

            $codeAttempt = new CodeAttempt();
            $codeAttempt->user_id = $user->id;
            $codeAttempt->amount = 1;
            $codeAttempt->save();

            return view('auth.phone_verify.login',[
                'user' => $user
            ]);
        }
    }

    public function reauthenticate(Request $request)
    {
        // get the logged in user
        $user = \Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $user->google2fa_secret = $google2fa->generateSecretKey();

        // save the user
        $user->save();

        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );

        // Pass the QR barcode image to our view.
        return view('google2fa.register', ['QR_Image' => $QR_Image,
            'secret' => $user->google2fa_secret,
            'reauthenticating' => true
        ]);
    }

    public static function typeReturn(){

        if(Auth::user()){
            if(Auth::user()->isAdmin()){
                return  response()->redirectToRoute('admin_controller_index');
            } elseif (Auth::user()->isManager()){
                return  response()->redirectToRoute('coach_dashboard_view');
            } elseif( Auth::user()->isDefault()){
                return  response()->redirectToRoute('default_dashboard');
            }
        }
        return ;
    }
}
