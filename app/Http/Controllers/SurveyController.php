<?php

namespace App\Http\Controllers;

use App\Answer;
use App\CSV;
use App\Mail\sendCsv;
use App\Survey;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Csv\Writer;

class SurveyController extends Controller
{

    public function showMultipleQuestionsForm()
    {

        $questionCount = Survey::all()->count();
        return view('survey.forms.multiple_questions', [
            'questionCount' => $questionCount
        ]);
    }

    public function showSingleQuestionForm()
    {
        $questionCount = Survey::all()->count();


        return view('survey.forms.create_question',[
            'questionCount' => $questionCount
        ]);
    }

    public function index()
    {

        $questions = Survey::take(12)->get();
        $questionCount = Survey::all()->count();
        $csv =  CSV::latest('created_at')->limit(10)->get();

        return view('survey.overview.index',[
            'csv' => $csv,
            'questions' => $questions,
            'questionCount' => $questionCount
        ]);
    }

    public function indexAllQuestions()
    {
        $questions = Survey::simplePaginate(15);
        $activatedQuestions = Survey::where('is_active','=',1)->count();

        $questionCount = Survey::all()->count();


        return view('survey.question_overview.index',[
            'questionCount' => $questionCount,
            'questions' => $questions,
            'activatedQuestions' => $activatedQuestions
        ]);
    }

    public function createSingleQuestion(Request $request)
    {
        $message = [
            'integer' => 'Dit veld mag alleen nummers bevatten.',
            'required' => 'Dit is een verplicht veld.',
            'unique' => 'De vraag ID moet uniek zijn. '
        ];

        $validator = Validator::make($request->all(), [
            'q_1' => 'required',
        ], $message);



        $questionCount = Survey::all()->count();
        if ($validator->fails()) {
            return view('survey.forms.create_question', [
                'questionCount' => $questionCount,
            ])->withErrors($validator)->render();
        }


        $activeCount = Survey::where('is_active','=','1')->count();
        if($activeCount < 12){
            $shouldactivate = 1;
            $queue = 0;
        }
        elseif ($activeCount > 12) {
            $shouldactivate = 0;
            $queue = 1;

            $q1 = new Survey();
            $q1->question = $request->q_1;
            $q1->is_active = $shouldactivate;
            $q1->in_queue = $queue;
            $q1->save();

            if(Auth::user()->isAdmin()){
                session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
                return response()->redirectToRoute('admin_show_survey_dashboard');
            } else {
                session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
                return response()->redirectToRoute('coach_show_survey_dashboard');
            }
        } else {
            $shouldactivate = 0;
            $queue = 1;
        }
        $q1 = new Survey();
        $q1->question = $request->q_1;
        $q1->is_active = $shouldactivate;
        $q1->in_queue = $queue;
        $q1->save();


        if(Auth::user()->isAdmin()){
            session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
            return response()->redirectToRoute('admin_survey_show_all_questions');
        } else {
            session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
            return response()->redirectToRoute('coach_survey_show_all_questions');
        }


    }

    public function showDashboard()
    {

        return view('admin.survey.index');
    }

    public function showDashboardForCoach()
    {

        return view('coach.survey.index');
    }

    public function showAllQuestions()
    {

        $questions = Survey::where('deleted_at','=',null)->simplePaginate(15);
        return view('admin.survey.survey_overview.index',[
            'questions' => $questions
        ]);
    }

    public function showAllQuestionsForCoach()
    {

        $questions = Survey::simplePaginate(15);
        return view('coach.survey.survey_overview.index',[
            'questions' => $questions
        ]);
    }


    public function createMultipleQuestion(Request $request)
    {

        $message = [
            'integer' => 'Dit veld mag alleen nummers bevatten.',
            'required' => 'Dit is een verplicht veld.',
            'unique' => 'De vraag ID moet uniek zijn. '
        ];

        $validator = Validator::make($request->all(), [
        'q_1' => 'required|min:3',
        'q_2' => 'required|min:3',
        'q_3' => 'required|min:3',
        'q_4' => 'required|min:3',
        'q_5' => 'required|min:3',
        'q_6' => 'required|min:3',
        'q_7' => 'required|min:3',
        'q_8' => 'required|min:3',
        'q_9' => 'required|min:3',
        'q_10' => 'required|min:3',
        'q_11' => 'required|min:3',
        'q_12' => 'required|min:3',
        ], $message);


        $questionCount = Survey::all()->count();

        if ($validator->fails()) {
            $request->flash();
            return view('survey.forms.multiple_questions', [
                'questionCount' => $questionCount,
            ])->withErrors($validator)->render();
        }

        $activeCount = Survey::where('is_active','=','1')->count();
        if($activeCount == 0){
            $shouldactivate = 1;
            $queue = 0;
        }
        elseif ($activeCount == 11) {
            $shouldactivate = 0;
            $queue = 1;

            $q1 = new Survey();
            $q1->question = $request->q_1;
            $q1->is_active = $shouldactivate;
            $q1->in_queue = $queue;
            $q1->save();

            $q2 = new Survey();
            $q2->question = $request->q_2;
            $q2->is_active = $shouldactivate;
            $q2->in_queue = $queue;
            $q2->save();

            $q3 = new Survey();
            $q3->question = $request->q_3;
            $q3->is_active = $shouldactivate;
            $q3->in_queue = $queue;
            $q3->save();

            $q4 = new Survey();
            $q4->question = $request->q_4;
            $q4->is_active = $shouldactivate;
            $q4->in_queue = $queue;
            $q4->save();

            $q5 = new Survey();
            $q5->question = $request->q_5;
            $q5->is_active = $shouldactivate;
            $q5->in_queue = $queue;
            $q5->save();

            $q6 = new Survey();
            $q6->question = $request->q_6;
            $q6->is_active = $shouldactivate;
            $q6->in_queue = $queue;
            $q6->save();

            $q7 = new Survey();
            $q7->question = $request->q_7;
            $q7->is_active = $shouldactivate;
            $q7->in_queue = $queue;
            $q7->save();

            $q8 = new Survey();
            $q8->question = $request->q_8;
            $q8->is_active = $shouldactivate;
            $q8->in_queue = $queue;
            $q8->save();

            $q9 = new Survey();
            $q9->question = $request->q_9;
            $q9->is_active = $shouldactivate;
            $q9->in_queue = $queue;
            $q9->save();

            $q10 = new Survey();
            $q10->question = $request->q_10;
            $q10->is_active = $shouldactivate;
            $q10->in_queue = $queue;
            $q10->save();

            $q11 = new Survey();
            $q11->question = $request->q_11;
            $q11->is_active = $shouldactivate;
            $q11->in_queue = $queue;
            $q11->save();

            $q12 = new Survey();
            $q12->question = $request->q_12;
            $q12->is_active = $shouldactivate;
            $q12->in_queue = $queue;
            $q12->save();



            if(Auth::user()->isAdmin()){
                session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
                return response()->redirectToRoute('admin_survey_show_all_questions');
            } else {
                session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
                return response()->redirectToRoute('coach_survey_show_all_questions');
            }
        } else {
            $shouldactivate = 0;
            $queue = 1;
        }

        $q1 = new Survey();
        $q1->question = $request->q_1;
        $q1->is_active = $shouldactivate;
        $q1->in_queue = $queue;
        $q1->save();

        $q2 = new Survey();
        $q2->question = $request->q_2;
        $q2->is_active = $shouldactivate;
        $q2->in_queue = $queue;
        $q2->save();

        $q3 = new Survey();
        $q3->question = $request->q_3;
        $q3->is_active = $shouldactivate;
        $q3->in_queue = $queue;
        $q3->save();

        $q4 = new Survey();
        $q4->question = $request->q_4;
        $q4->is_active = $shouldactivate;
        $q4->in_queue = $queue;
        $q4->save();

        $q5 = new Survey();
        $q5->question = $request->q_5;
        $q5->is_active = $shouldactivate;
        $q5->in_queue = $queue;
        $q5->save();

        $q6 = new Survey();
        $q6->question = $request->q_6;
        $q6->is_active = $shouldactivate;
        $q6->in_queue = $queue;
        $q6->save();

        $q7 = new Survey();
        $q7->question = $request->q_7;
        $q7->is_active = $shouldactivate;
        $q7->in_queue = $queue;
        $q7->save();

        $q8 = new Survey();
        $q8->question = $request->q_8;
        $q8->is_active = $shouldactivate;
        $q8->in_queue = $queue;
        $q8->save();

        $q9 = new Survey();
        $q9->question = $request->q_9;
        $q9->is_active = $shouldactivate;
        $q9->in_queue = $queue;
        $q9->save();

        $q10 = new Survey();
        $q10->question = $request->q_10;
        $q10->is_active = $shouldactivate;
        $q10->in_queue = $queue;
        $q10->save();

        $q11 = new Survey();
        $q11->question = $request->q_11;
        $q11->is_active = $shouldactivate;
        $q11->in_queue = $queue;
        $q11->save();

        $q12 = new Survey();
        $q12->question = $request->q_12;
        $q12->is_active = $shouldactivate;
        $q12->in_queue = $queue;
        $q12->save();


        if(Auth::user()->isAdmin()){
            session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
            return response()->redirectToRoute('admin_survey_show_all_questions')->with('succes',__('messages.succes_added',['model' => 'questions']));
        } else {
            session()->flash('succes',__('messages.succes_added',['model' => 'questions']));
            return response()->redirectToRoute('coach_survey_show_all_questions')->with('succes',__('messages.succes_added',['model' => 'questions']));
        }

    }


    public function finishSurvey(Request $request,User $user)
    {

        $survey = Survey::all()->where('is_active','=', 1);

        $rules = [
        ];
        foreach($survey as $index => $question){
            $rules['q.'.$question->id.''] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        $completed = 'false';

        if(!is_null($request->name_field)){
            session()->flash("danger","something went horribly wrong, please contact the administrator. error-code:10115");
            return view('default.survey.index', [
                'completed' => $completed,
                'survey' => $survey,
            ])->withErrors($validator)->render();
        }

        if ($validator->fails()) {
            $request->flash();
            return view('default.survey.index', [
                'completed' => $completed,
                'survey' => $survey,
            ])->withErrors($validator)->render();
        }


        foreach($survey as $index => $question){
            $array = [
                'interviewee_id' => $user->id,
                'question_id' => $question->id,
                'answer' => $request->q[$question->id],
                'is_exported' => 0,
                'created_at' => Carbon::now()
            ];
            Answer::insert($array);
        }

        $request->flash();


        return response()->redirectToRoute('default_show_survey',[
            'survey' => $survey,
            session()->flash('succes',__('messages.survey_succes')),
        ]);


//        return redirect()->action('HomeController@index',[
//        ]);


    }
}
