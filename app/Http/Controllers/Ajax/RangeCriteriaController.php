<?php namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Model\Range;
use App\Model\RangeCriteria;
use App\Model\RangeCriteriaCollection;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class RangeCriteriaController extends Controller
{
    /**
     * @param Request $request
     * @param RangeCriteriaCollection|null $criteriaCollection
     *
     * @return Response
     */
    public function saveCollection(Request $request, RangeCriteriaCollection $criteriaCollection = null)
    {
        $validator = Validator::make($request->all(), [
            'range_id' => 'required',
            'range_indicator_id' => 'required',
            'criteria.*.type' => 'required|in:self,gender,age',
            'criteria.*.value' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()], 400);
        }

        if (null === $criteriaCollection) {
            $criteriaCollection = new RangeCriteriaCollection();

            $criteriaCollection->range_id = $request->json('range_id');
            $criteriaCollection->range_indicator_id = $request->json('range_indicator_id');

            $criteriaCollection->save();
        }

        foreach ($request->json('criteria') as $data) {
            $criteria = $criteriaCollection->get($data['type']);

            if (null === $criteria) {
                $criteria = new RangeCriteria();
                $criteria->type = $data['type'];
                $criteria->range_criteria_collection_id = $criteriaCollection->id;
            }

            $criteria->value = $data['value'];

            $criteria->save();
        }

        return response()->json([
            'id' => $criteriaCollection->id
        ]);
    }

    /**
     * @param RangeCriteriaCollection $criteriaCollection
     *
     * @return Response
     *
     * @throws Exception
     */
    public function deleteCollection(RangeCriteriaCollection $criteriaCollection)
    {
        foreach ($criteriaCollection->rangeCriterias as $criteria) {
            $criteria->delete();
        }

        $criteriaCollection->delete();

        return response('', 204);
    }

    /**
     * @param Range $range
     *
     * @return Response
     *
     * @throws \Throwable
     */
    public function getOverlapping(Range $range)
    {
        $overlapping = [];

        foreach ($range->getCriteriaCollections() as $criteriaCollectionA) {
            foreach ($range->getCriteriaCollections() as $criteriaCollectionB) {
                if ($criteriaCollectionA === $criteriaCollectionB) {
                    continue;
                }

                $key = $this->getKey($criteriaCollectionA, $criteriaCollectionB);
                if (isset($overlapping[$key])) {
                    continue;
                }

                if ($this->doCriteriaCollectionsOverlap($criteriaCollectionA, $criteriaCollectionB)) {
                    $overlapping[$key] = [
                        $criteriaCollectionA->id,
                        $criteriaCollectionB->id
                    ];
                } else {
                    $criteriaCollectionA[$key] = null;
                }
            }
        }

        return response()->json([
            'html' => view('admin.screening_dashboard.range.template.overlapping_range_collections', [
                'items' => $overlapping
            ])->render()
        ]);
    }

    /**
     * @param RangeCriteriaCollection $criteriaCollectionA
     * @param RangeCriteriaCollection $criteriaCollectionB
     *
     * @return string
     */
    private function getKey(RangeCriteriaCollection $criteriaCollectionA, RangeCriteriaCollection $criteriaCollectionB): string
    {
        if ($criteriaCollectionA->id <= $criteriaCollectionB->id) {
            return $criteriaCollectionA->id . '-' . $criteriaCollectionB->id;
        }

        return $criteriaCollectionB->id . '-' . $criteriaCollectionA->id;
    }

    /**
     * @param RangeCriteriaCollection $criteriaCollectionA
     * @param RangeCriteriaCollection $criteriaCollectionB
     *
     * @return bool
     */
    private function doCriteriaCollectionsOverlap(RangeCriteriaCollection $criteriaCollectionA, RangeCriteriaCollection $criteriaCollectionB): bool
    {
        foreach ($criteriaCollectionA->getAllCriteria() as $criteriaA) {
            $criteriaB = $criteriaCollectionB->getCriteria($criteriaA->type);

            if (null === $criteriaB) {
                return false;
            }

            if (! $this->doCriteriaOverlap($criteriaA->type, $criteriaA, $criteriaB)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $type
     * @param RangeCriteria $criteriaA
     * @param RangeCriteria $criteriaB
     *
     * @return bool
     */
    private function doCriteriaOverlap(string $type, RangeCriteria $criteriaA, RangeCriteria $criteriaB): bool
    {
        if ('gender' === $type) {
            if ($criteriaA->value === $criteriaB->value) {
                return true;
            }

            return 'any' === $criteriaA->value || 'any' === $criteriaB->value;
        }

        list($minA, $maxA) = explode('-', $criteriaA->value);
        list($minB, $maxB) = explode('-', $criteriaB->value);

        $minA = (float)$minA;
        $minB = (float)$minB;
        $maxA = (float)$maxA;
        $maxB = (float)$maxB;

        if ($minA <= $maxB && $minA >= $minB) {
            return true;
        }

        if ($maxA <= $maxB && $maxA >= $minB) {
            return true;
        }

        return (0.0 === $minA & 0.0 === $maxA) || (0.0 === $minB && 0.0 === $maxB);
    }
}
