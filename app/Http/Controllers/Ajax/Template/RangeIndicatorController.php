<?php
/**
 * Created by PhpStorm.
 * User: Armando
 * Date: 8/23/2019
 * Time: 7:22 PM
 */

namespace App\Http\Controllers\Ajax\Template;


use App\Model\RangeCriteria;
use App\Model\RangeCriteriaCollection;
use App\Model\RangeIndicator;
use App\Model\RangeColor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function foo\func;


class RangeIndicatorController
{

    public function showContent()
    {
        $range = RangeIndicator::simplePaginate(10);


        return view('admin.screening_dashboard.range.template.index',[
            'range' => $range
        ]);
    }

    public function showForm()
    {

        $rangeColor = RangeColor::all();

        return view('admin.form.screening_dashboard.range.create.create',[
            'rangecolors' => $rangeColor,
        ]);
    }

    public function showUpdateForm(RangeIndicator $range)
    {
        $rangecolors = RangeColor::all();
        return view('admin.form.screening_dashboard.range.update.update',[
            'range' => $range,
            'rangecolors' => $rangecolors
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'range_color' => 'required',
            'range_name' => 'required|min:3|max:20',
            'rank' => 'required'
        ]);

        $rangeColor = RangeColor::all();
        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.screening_dashboard.range.create.create',[
                    'rangecolors' => $rangeColor,
                ])->withErrors($validator)->render()
            ), 400);
        }

        $rangeColorArray = json_decode($request->range_color,true);

        $range = new RangeIndicator();
        $range->name = $request->range_name;
        $range->rank = $request->rank;
        $range->range_color_id = $rangeColorArray['id'];
        $range->save();


        session()->flash('success','RangeIndicator: "'. $range->name. '" added!');
        return app()->call('App\Http\Controllers\Ajax\Template\RangeIndicatorController@showContent',[
            'range' => $range
        ]);

    }

    public function update(Request $request, RangeIndicator $range)
    {
        $validator = Validator::make($request->all(), [
            'range_color' => 'required',
            'range_name' => 'required|min:3|max:20',
            'rank' => 'required'
        ]);

        $rangeColor = RangeColor::all();
        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.screening_dashboard.range.create.create',[
                    'rangecolors' => $rangeColor,
                ])->withErrors($validator)->render()
            ), 400);
        }

        $rangeColorArray = json_decode($request->range_color,true);

        $range->name = $request->range_name;
        $range->range_color_id = $rangeColorArray['id'];
        $range->rank = $request->rank;
        $range->update();


        session()->flash('success','RangeIndicator: "'. $range->name. '" changed successfully!');
        return app()->call('App\Http\Controllers\Ajax\Template\RangeIndicatorController@showContent');
    }

    public function delete(Request $request, RangeIndicator $range)
    {

        $rangeCollections = RangeCriteriaCollection::where('range_indicator_id',$range->id)->with('rangeCriterias')->withCount('rangeCriterias')->get();

        if(!$rangeCollections->isEmpty()){
            foreach($rangeCollections as $collections){
                if(!$collections->rangeCriterias->isEmpty()){
                    if($collections->range_criterias_count > 0){

                        foreach($collections->rangeCriterias as $criteria){
                            $criteria->delete();
                        }
                    }
                }
                $collections->delete();
            }
        }



        $range->delete();
        session()->flash('success','RangeIndicator: "'. $range->name. '" deleted.');

        return app()->call('App\Http\Controllers\Ajax\Template\RangeIndicatorController@showContent');
    }
}