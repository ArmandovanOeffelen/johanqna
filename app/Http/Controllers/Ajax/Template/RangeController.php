<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Model\Range;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RangeController extends Controller
{

    public function index()
    {
        $ranges = Range::simplePaginate(10);

        return view('admin.screening_dashboard.range_config.range_config.template.index',[
            'ranges' => $ranges
        ]);
    }
    public function delete(Range $range)
    {

        if($range->trashed() == true){
        } else {
            $range->delete();
        }

        session()->flash('success',__('messages.succes_deleted',['model'=>'Range']));
        return app()->call('App\Http\Controllers\Ajax\Template\RangeController@index');
    }

    public function showTutorial()
    {

        return view('faq.admin.range-config.template.index');
    }
}
