<?php

namespace App\Http\Controllers\Ajax\Template;

use App\CompanyEmployeeInvite;
use App\Model\CoachInvite;
use App\Model\Company;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function showCoachInviteForm()
    {

        return view('admin.form.user.invite.create');
    }
    public function showCoachInviteFormUserDashboard()
    {

        return view('admin.form.user.invite.create');
    }


    public function showEmployeeInviteForm()
    {
        $companies = Company::all();
        return view('admin.form.user.invite.invite_user',[
            'companies' => $companies
        ]);
    }

    public function showCoachDashboard()
    {

        $coaches = User::where('is_coach','=','1')->simplePaginate(10);

        return view('admin.users.coach.template.index',[
            'coaches' => $coaches
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUsersDashboard()
    {

        return view('admin.users.template.index');
    }

    public function showEmployeeDashboard()
    {
        $users = User::where('type','default')->simplePaginate(10);

        return response(view('admin.users.user.template.index',[
            'users' => $users
        ]));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    public static function processInvite(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_coach_invite,email',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.user.invite.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
        ];

        $invite = new CoachInvite();
        $invite->sendInvite($infoArray);

        session()->flash('succes','U heeft: ' . $request->email. " een uitnodiging gestuurd om coach te worden binnen BARABAZ");
        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showCoachDashboard');

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    public static function processEmployeeInvite(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'company' => 'required',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_employee_invite,email',
        ]);

        $companies = Company::all();

        //TODO:: fix this into an object.
        $company = Company::findOrFail($request->company);


        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.user.invite.invite_user',[
                    'companies' => $companies
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'company' => $company,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company,$infoArray);

        session()->flash('succes','U heeft: ' . $request->email. " een uitnodiging gestuurd om coach te worden binnen BARABAZ");
        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showEmployeeDashboard');

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    public static function processEmployeeInviteUserDashboard(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'company' => 'required',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_employee_invite,email',
        ]);

        $companies = Company::all();

        //TODO:: fix this into an object.
        $company = Company::findOrFail($request->company);


        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.user.invite.invite_user',[
                    'companies' => $companies
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'company' => $company,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company,$infoArray);

        Session::flash('success','U heeft een uitnodiging verstuurd');

        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showUsersDashboard');

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    public static function processInviteUserDashboard(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_coach_invite,email',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.user.invite.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
        ];

        $invite = new CoachInvite();
        $invite->sendInvite($infoArray);

        session()->flash('succes','U heeft: ' . $request->email. " een uitnodiging gestuurd om coach te worden binnen BARABAZ");
        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showUsersDashboard');

    }

    /**
     * @param User $user
     *
     * @return mixed
     * @throws \Exception
     */
    public function deleteCoach(User $user)
    {
        $count = CoachInvite::where('email',$user->email)->count();
        $getInvite = CoachInvite::where('email',$user->email)->first();
        if($count !== 0){
            $getInvite->delete();
        }
        $user->delete();


        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showCoachDashboard');
    }

    /**
     * @param User $user
     *
     * @return mixed
     * @throws \Exception
     */
    public function deleteEmployee(User $user)
    {

        $count = CompanyEmployeeInvite::where('email',$user->email)->count();
        $getInvite = CompanyEmployeeInvite::where('email',$user->email)->first();
        if($count !== 0){
            $getInvite->delete();
        }
        $user->delete();


        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showEmployeeDashboard');
    }

    public function promoteUserToAdmin(User $user)
    {
        $user->type = 'admin';
        $user->is_coach = 0;
        $user->company_id = null;
        $user->update();

        session()->flash('succes','U heeft '.$user->name." ".$user->lastname." tot admin gepromoveerd.");
        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showEmployeeDashboard');
    }

    public function promoteCoachToAdmin(User $user)
    {
        $user->type = 'admin';
        $user->is_coach = 0;
        $user->company_id = null;


        session()->flash('succes','U heeft '.$user->name." ".$user->lastname." tot admin gepromoveerd.");


        $user->update();
        return app()->call('App\Http\Controllers\Ajax\Template\AdminController@showCoachDashboard');

    }



}
