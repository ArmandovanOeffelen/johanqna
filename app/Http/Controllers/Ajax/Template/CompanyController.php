<?php

namespace App\Http\Controllers\Ajax\Template;

use App\CompanyEmployeeInvite;
use App\Model\Company;

use App\Model\CompanyLogo;
use App\Model\CoachInvite;
use App\Model\Result;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function showSingleCompany(Company $company)
    {

        $allVisitDates = Result::select('visit_date')->where('company_id', $company->id)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();

        return view('admin.company.company.template.index', [
            'years' => $years,
            'company' => $company
        ]);
    }

    public function showContent()
    {
        $company = Company::simplePaginate(10);


        return view('admin.company.overview.template.index', [
            'company' => $company
        ]);
    }


    public function showForm()
    {

        return view('admin.form.company.create.create');
    }

    public function showUpdateForm(Company $company)
    {

        return view('admin.form.company.update.update', [
            'company' => $company
        ]);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'code' => 'required|min:3|max:10',
            'email' => 'required|email|unique:company,email'
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.company.create.create', [
                ])->withErrors($validator)->render()
            ), 400);
        }

        $newCompany = new company();
        $newCompany->name = $request->name;
        $newCompany->company_code = $request->code;
        $newCompany->email = $request->email;
        $newCompany->save();


        $newCompanyLogo = new CompanyLogo();
        $newCompanyLogo->company_id = $newCompany->id;
        $newCompanyLogo->save();

        $newCompany->createCompanyToken($newCompany);

        $company = Company::simplePaginate(10);
        session()->flash('succes', 'Bedrijf: ' . $newCompany->name . ' succesvol toegevoegd!');
        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent', [
            'company' => $company
        ]);
    }

    public function update(Request $request, Company $company)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'code' => 'required|min:3|max:10',
            'email' => 'required|email|unique:company,email,' . $company->id,
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.company.update.update', [
                    'company' => $company
                ])->withErrors($validator)->render()
            ), 400);
        }


        $company->name = $request->name;
        $company->company_code = $request->code;
        $company->email = $request->email;
        $company->appointment_frame = $request->appointment;
        $company->appointment_frame_name = $request->appointment_frame_name;
        $company->update();

        session()->flash('succes', 'Company: ' . $company->name . ' changed.');
        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent');
    }

    public function delete(Request $request, Company $company)
    {

        $users = User::where('company_id', $company->id)->count();
        if ($users > 0) {
            session()->flash('warning', 'We can\'t delete this company, there are users attached to this company.');
            return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent');
        }

        $companyLogo = CompanyLogo::where('company_id', $company->id)->first();
        $companyLogo->delete();
        $company->delete();

        session()->flash('succes', 'Company succesfully deleted!');
        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent');
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */

    public function sendInvite(Request $request, Company $company)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_employee_invite,email',
        ]);


        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.company.invite.invite', [
                    'company' => $company
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'company' => $company->id,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company, $infoArray);

        session()->flash('succes', 'U have succesfully send an employee invite');
        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showAllEmployeesForCompany', [
            'company' => $company
        ]);


    }

    public function showInviteForm(Company $company)
    {

        return view('admin.form.company.invite.invite', [
            'company' => $company
        ]);
    }

    public function showAllEmployeesForCompany(Company $company)
    {


        $employees = User::where('company_id', $company->id)->simplePaginate(10);

        return view('admin.company.company.employees.template.index', [
            'company' => $company,
            'employees' => $employees
        ]);
    }

    public function deleteEmployeeAdmin(Company $company, User $user)
    {

        //lets check if they have results
        $resultCount = Result::where('user_id', $user->id)->count();
        if ($resultCount > 0) {
            $results = Result::where('user_id', $user->id)->get();

            foreach ($results as $result) {
                if (!is_null($result->ResultPersonalAdvice)) {
                    $result->ResultPersonalAdvice->delete();
                }

                if (!is_null($result->ResultValue)) {

                    foreach ($result->ResultValue as $value) {

                        $value->delete();
                    }
                }

                $result->delete();
            }
            $user->delete();
        } else {
            $user->delete();
        }

        session()->flash('succes', __('messages.succes_deleted', ['model' => 'user']));
        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showAllEmployeesForCompany', [
            'company' => $company
        ]);
    }
}
