<?php

namespace App\Http\Controllers\Ajax\Template;

use App\CSV;
use App\Survey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{

    public function index()
    {
        $questions = Survey::simplePaginate(15);
        $activatedQuestions = Survey::where('is_active','=',1)->count();

        $questionCount = Survey::all()->count();


        return view('survey.question_overview.template.index',[
            'questionCount' => $questionCount,
            'questions' => $questions,
            'activatedQuestions' => $activatedQuestions
        ]);

    }

    public function contentOverview(){

        $questions = Survey::take(10)->get();
        $activatedQuestions = Survey::where('is_active','=',1)->count();
        $questionCount = Survey::all()->count();
        $csv =  CSV::latest('created_at')->limit(10)->get();

        return view('survey.overview.template.index',[
            'csv' => $csv,
            'questions' => $questions,
            'questionCount' => $questionCount,
            'activatedQuestions' => $activatedQuestions
        ]);
    }


    public function delete(Survey $question)
    {
        $questionCount = Survey::where('is_active','=',1)->count();

        if($questionCount <= 12){

            session()->flash("warning","Question not deleted, there need to be atleast 12 active questions");
            return app()->call('App\Http\Controllers\Ajax\Template\SurveyController@index');

        }


        $question->delete();

        $this->findFirstInQueue();

        session()->flash("warning","Question deleted!");
        return app()->call('App\Http\Controllers\Ajax\Template\SurveyController@index');
    }

    public function findFirstInQueue()
    {
        $questionInQueue = Survey::where('in_queue','=','1')->first();

        if (is_null($questionInQueue)){
            return ;
        } else {

        $questionToMoveInQueue = Survey::findOrFail($questionInQueue->id);
        $questionToMoveInQueue->in_queue = 0;
        $questionToMoveInQueue->is_active = 1;
        $questionToMoveInQueue->update();
        }

        return;
    }
}
