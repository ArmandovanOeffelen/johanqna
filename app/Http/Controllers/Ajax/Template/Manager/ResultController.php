<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResultController extends Controller
{

    public function showResultsNoAdvice()
    {
        $allResults = Result::withCount('ResultPersonalAdvice')->get();

        $tempResultCollection = collect();

        foreach ($allResults as $index => $item) {
            if ($item->result_personal_advice_count === 0) {
                $tempResultCollection->push($item);
            }
        }
        $results = $tempResultCollection;


        return view('coach.screening_dashboard.results_no_advice.template.index', [
            'results' => $results
        ]);
    }


    public function showNoAdviceForm(Result $result)
    {


        return view('coach.form.employee.advice.create',[
            'result' => $result
        ]);
    }

    public function createAdvice(Request $request,Result $result)
    {
        {
            $validator = Validator::make($request->all(), [
                'advice' => 'required',
            ]);

            if ($validator->fails()) {
                $request->flash();
                return response()->json(array(
                    'html' => view('coach.form.employee.advice.create',[
                    ])->withErrors($validator)->render()
                ), 400);
            }

            $resultPersonalAdvice = new ResultPersonalAdvice();
            $resultPersonalAdvice->result_id = $result->id;
            $resultPersonalAdvice->coach_id = Auth::user()->id;
            $resultPersonalAdvice->advice = $request->advice;
            $resultPersonalAdvice->save();


            session()->flash('succes',__('messages.succes_added',['model' => 'personal advice']));
            return app()->call('App\Http\Controllers\Ajax\Template\Manager\ResultController@showResultsNoAdvice',[
            ]);
        }
    }
}
