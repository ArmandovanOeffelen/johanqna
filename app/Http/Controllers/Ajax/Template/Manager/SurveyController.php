<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\Survey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SurveyController extends Controller
{

    public function index(Request $request)
    {

        if(Auth::user()->isAdmin()){
            $questions = Survey::simplePaginate(15)->setPath(route('coach_ajax_show_survey_index'));
            return view('admin.survey.survey_overview.template.index',[
                'questions' => $questions
            ]);
        } else {
            $questions = Survey::simplePaginate(15)->setPath(route('coach_ajax_show_survey_index'));
            return view('coach.survey.survey_overview.template.index',[
                'questions' => $questions
            ]);
        }

    }


    public function deleteQuestion(Survey $question)
    {

        $question->delete();

        $questionCount = Survey::where('is_active',1)->count();
        if($questionCount <11){
            $this->findFirstInQueue();
        }
        session()->flash('succes',__('messages.succes_deleted',['model' => 'question']));

        return app()->call('App\Http\Controllers\Ajax\Template\Manager\SurveyController@index');
    }

    public function findFirstInQueue()
    {
        $questionInQueue = Survey::where('in_queue','=','1')->first();

        iF (is_null($questionInQueue)){
            return session()->flash('warning',__('messages.deleted_succes_no_queue')) ;
        } else {

            $questionToMoveInQueue = Survey::findOrFail($questionInQueue->id);
            $questionToMoveInQueue->in_queue = 0;
            $questionToMoveInQueue->is_active = 1;
            $questionToMoveInQueue->update();
        }

        return session()->flash('warning',__('messages.deleted_succes_moved_queue')) ;
    }
}
