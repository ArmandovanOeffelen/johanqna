<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\CompanyEmployeeInvite;
use App\Model\Company;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ManagerEmployeeController extends Controller
{
    public function showInviteForm(Company $company)
    {

        return view('manager.form.employee.create');
    }
    public function dashboard(Company $company)
    {
        $employees = User::where('company_id','=',$company->id)->where('is_coach','=',0)->simplePaginate(10);


        return view('coach.company.employees.template.index',[
            'employees' => $employees,
            'company' => $company
        ]);
    }

    public function inviteDashboard(Company $company)
    {

        $invites = CompanyEmployeeInvite::where('invite_accepted','=',0)->where('company_id','=',$company->id)->simplePaginate(10);


        return view('coach.employee.invite.template.index',[
            'invites' => $invites,
            'company' => $company
        ]);
    }


    public function sendInvite(Request $request, Company $company)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_manager_invite,email',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('coach.form.employee.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company,$infoArray);

        session()->flash('succes','Succesvol een uitnodiging verstuurd naar: ' . $request->email);
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\ManagerEmployeeController@dashboard',[
            'company' => $company
        ]);

    }

    public function sendInviteFromOverviewPage(Request $request, Company $company)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_manager_invite,email',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('coach.form.employee.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company,$infoArray);

        session()->flash('succes','Succesvol een uitnodiging verstuurd naar: ' . $request->email);
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\ManagerEmployeeController@inviteDashboard',[
            'company' => $company
        ]);

    }

    public function deleteInvite(Company $company, CompanyEmployeeInvite $invite)
    {

        $invite->delete();

        session()->flash('succes','De uitnodiging is succesvol ingetrokkken.');
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\ManagerEmployeeController@inviteDashboard',[
            'company' => $company
        ]);
    }

    public function deleteEmployee(Company $company, User $employee)
    {

        $companyInvite = new CompanyEmployeeInvite();
        $companyInvite->deleteInvite($employee->email);

        $employee->delete();


        return app()->call('App\Http\Controllers\Ajax\Template\Manager\ManagerEmployeeController@dashboard',[
            session()->flash('succes',__('messages.succes_deleted',['model' => 'user'])),
            'company' => $company
        ]);

    }

}
