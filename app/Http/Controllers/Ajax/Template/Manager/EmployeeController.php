<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\CompanyEmployeeInvite;
use App\Model\Company;
use App\Model\Range;
use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function delete(User $user)
    {
        $companyInvite = new CompanyEmployeeInvite();
        $companyInvite->deleteInvite($user->email);


        $user->delete();

        $employees = User::where('type', 'default')->simplePaginate(10);
        session()->flash('success', __('messages.succes_deleted', ['model'=> 'user']));


        return app()->call('App\Http\Controllers\Ajax\Template\Manager\EmployeeController@showOverview',[
            'employees' => $employees
        ]);
    }

    public function showOverview()
    {
        $employees = User::where('type', 'default')->simplePaginate(10);

        return view('coach.employee.template.index', [
            'employees' => $employees
        ]);
    }

    public function ShowEmployees()
    {
        $employees = User::where('type', 'default')->simplePaginate(10);

        return view('coach.employee.template.index', [
            'employees' => $employees
        ]);
    }

    public function showInviteForm()
    {

        $companies = Company::all();

        return view('coach.form.employee.send_invite', [
            'companies' => $companies
        ]);
    }

    public function sendInvite(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'company' => 'required',
            'last_name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company_employee_invite,email',
        ]);

        $companies = Company::all();

        $company = Company::findOrFail($request->company);


        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.user.invite.invite_user', [
                    'companies' => $companies
                ])->withErrors($validator)->render()
            ), 400);
        }

        $infoArray = [
            'firstname' => $request->name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'company' => $company,
        ];

        $invite = new CompanyEmployeeInvite();
        $invite->sendInvite($company, $infoArray);

        session()->flash('success', 'U heeft de gebruiker succesvol uitgenodigd!');
        return response()->redirectToRoute('coach_ajax_show_all_employees');

    }

    public function showEmployee(User $user)
    {

        $results = Result::where('email',$user->email)->withCount('ResultPersonalAdvice')->orderBy('visit_date','DESC')->get();

        return view('coach.employee.single_employee.template.index',[
            'results' => $results,
            'user' => $user
        ]);
    }


    public function showText($range)
    {

        $range = Range::select('range_text_title','range_text')->where('id',$range)->first();

        return view('default.results.template.info_text',[
            'range' => $range
        ]);
    }

    public function showAdvice($result)
    {
        $advice = ResultPersonalAdvice::where('result_id',$result)->first();

        return view('default.results.template.info_advice',[
            'advice' => $advice
        ]);
    }
}
