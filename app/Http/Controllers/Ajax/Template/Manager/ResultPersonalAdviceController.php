<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResultPersonalAdviceController extends Controller
{
    public function showForm(User $user,Result $result)
    {


        return view('coach.form.employee.advice.create',[
            'user' => $user,
            'result' => $result
        ]);
    }

    public function showAdvice(User $user, Result $result)
    {
        return view('coach.employee.single_employee.advice.show_advice',[
            'user' => $user,
            'result' => $result
        ]);
    }

    public function saveAdvice(Request $request, User $user, Result $result)
    {
        $validator = Validator::make($request->all(), [
            'advice' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('coach.form.employee.advice.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $resultPersonalAdvice = new ResultPersonalAdvice();
        $resultPersonalAdvice->result_id = $result->id;
        $resultPersonalAdvice->coach_id = Auth::user()->id;
        $resultPersonalAdvice->advice = $request->advice;
        $resultPersonalAdvice->save();


        session()->flash('succes',__('messages.succes_added',['model' => 'personal advice']));
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\EmployeeController@showEmployee',[
            'user' => $user
        ]);
    }

    public function updateAdvice(Request $request, User $user, Result $result)
    {

        $validator = Validator::make($request->all(), [
            'advice' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('coach.form.employee.advice.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $resultPersonalAdvice = ResultPersonalAdvice::findOrFail($result->ResultPersonalAdvice->id);
        $resultPersonalAdvice->result_id = $result->id;
        $resultPersonalAdvice->coach_id = Auth::user()->id;
        $resultPersonalAdvice->advice = $request->advice;
        $resultPersonalAdvice->update();


        session()->flash('succes',__('messages.succes_added',['model' => 'personal advice']));
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\EmployeeController@showEmployee',[
            'user' => $user
        ]);
    }
}
