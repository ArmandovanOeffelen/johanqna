<?php

namespace App\Http\Controllers\Ajax\Template\Manager;

use App\Model\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function showForm(Company $company)
    {

        return view('manager.form.company.update.update',[
            'company' => $company
        ]);
    }

    public function showContent(Company $company)
    {
        return view('manager.company.template.index',[
            'company' => $company
        ]);
    }

    public function update(Request $request,Company $company)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:company,email,'.$company->id,
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('manager.form.company.update.update',[
                ])->withErrors($validator)->render()
            ), 400);
        }


        $company->name = $request->name;
        $company->email = $request->email;
        $company->update();

        session()->flash('succes','Company: '.$company->name.' is changed.');
        return app()->call('App\Http\Controllers\Ajax\Template\Manager\CompanyController@showContent',[
            'company' => $company
        ]);
    }
}
