<?php

namespace App\Http\Controllers\Ajax\Template;

use App\CSV;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CSVController extends Controller
{
    public function deleteOnOverview(CSV $csv)
    {
        $file= $csv->filepath.$csv->filename.".csv";
        Storage::delete($file);


        $csv->delete();


        session()->flash('succes','Het CSV bestand is succesvol verwijderd.');
        return app()->call('App\Http\Controllers\Ajax\Template\SurveyController@contentOverview');
   }

    public function delete(CSV $csv)
    {
        $file= $csv->filepath.$csv->filename.".csv";
        Storage::delete($file);


        $csv->delete();


        session()->flash('succes','Het CSV bestand is succesvol verwijderd.');
        return app()->call('App\Http\Controllers\Ajax\Template\CSVExportController@index');
    }
}
