<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Answer;
use App\CSV;
use App\Survey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;

class CSVExportController extends Controller
{

    public function showMainDashboardContent(){

        $questions = Survey::take(10)->get();
        $activatedQuestions = Survey::where('is_active','=',1)->count();
        $questionCount = Survey::all()->count();
        $csv =  CSV::latest('created_at')->limit(10)->get();

        return view('survey.overview.template.index',[
            'csv' => $csv,
            'questions' => $questions,
            'questionCount' => $questionCount,
            'activatedQuestions' => $activatedQuestions
        ]);
    }


    public function index(){

        $csvs =  CSV::latest('created_at')->simplePaginate(15);

        return view('survey.csv_overview.template.index',[
            'csvs' => $csvs
        ]);
    }

    public function deleteCSV(CSV $csv)
    {


        return ;
    }


    public function ExportCSV()
    {
        $today = Carbon::today()->toDateString();


        $answersToday =  Answer::whereDate('created_at',$today)->orderBy('interviewee_id','DESC')->get();

        if(is_null($answersToday) || $answersToday->isEmpty()){

            session()->flash('warning','CSV bestand niet aangemaakt, er zijn geen resultaten gevonden van ingevulde enquetes.');
            return app()->call('App\Http\Controllers\Ajax\Template\CSVExportController@showMainDashboardContent');
        }


        $checkIfCsvAlreadyExists = CSV::whereDate('created_at',$today)->count();

        if($checkIfCsvAlreadyExists >= 1){


            session()->flash('warning','CSV bestand is reeds gegenereerd, verwijder eerst het oude bestand om deze opnieuw te kunnen genereren.');
            return app()->call('App\Http\Controllers\Ajax\Template\CSVExportController@showMainDashboardContent');
        } else {

        $csvfile = $this->generateCSV($answersToday);


        $csv = new CSV();
        $csv->filename = $csvfile['filename'];
        $csv->filepath = $csvfile['filepathforstorage'];
        $csv->csv_date = Carbon::now()->format('Y-m-d');
        $csv->save();

        session()->flash('succes','De CSV van vandaag is succesvol gegenereed, deze kunt u hieronder downloaden.');
        return app()->call('App\Http\Controllers\Ajax\Template\SurveyController@contentOverview');
        }
    }

    /**
     * @param Collection|Answer[] $answers
     * @throws \League\Csv\CannotInsertRecord
     */
    public function generateCSV(Collection $answers)
    {

        $date = Carbon::now();
        $csvname = $date->format('Y-m-d').'-' . uniqid();
        $filepathForStorage = storage_path().'/app/public/TempCSV/';
        $filepath = $filepathForStorage.$csvname.'.csv';
        $writer = Writer::createFromPath($filepath,'w+');
        $writer->setDelimiter(';');

        // "", vraag1, vraag2, ...
        // <werknemernr>  , <antw>, <antw>, ...
        // <1>  , <antw>, <antw>, ...
        // <2>  , <antw>, <antw>, ...
        // <3>  , <antw>, <antw>, ...


        $answersByInterviewee = [];
        $questionIds = [];

        foreach ($answers as $answer) {
            $answersByInterviewee[$answer->interviewee_id][$answer->question_id] = $answer->answer;
            $questionIds[$answer->question_id] = true;
        }

        $questionIds = array_keys($questionIds);

        // Insert the header record.
        $writer->insertOne(array_merge([''], $questionIds)); // First cell is empty, followed by the question ids.

        // Insert all answers per interviewee to the writer.
        foreach ($answersByInterviewee as $intervieweeId => $answers) {
            $record = [$intervieweeId]; // First cell is the interviewee's id.

            // Following cells are answers to the questions.
            foreach ($questionIds as $questionId) {
                if (isset($answers[$questionId])) {
                    $record[] = $answers[$questionId];
                } else {
                    $record[] = ''; // empty cell
                }
            }

            $writer->insertOne($record);
        }

        $csvdata = ['filename' => $csvname, 'filepath' => $filepath,'filepathforstorage' => $filepathForStorage];
        return $csvdata;
    }
}
