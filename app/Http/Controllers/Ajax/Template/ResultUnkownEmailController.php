<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Model\ResultUnknownEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResultUnkownEmailController extends Controller
{
    public function showFlaggedResultsOverview()
    {
        $results = ResultUnknownEmail::simplePaginate(10);

        return view('coach.screening_dashboard.flagged_results.template.index',[
            'results' => $results
        ]);
    }

    public function deleteResultUnkownEmail(ResultUnknownEmail $resultUnknownEmail)
    {
        session()->flash('success',__('messages.mark_resolved',['email' => $resultUnknownEmail->email]));
        $resultUnknownEmail->delete();


        return app()->call('App\Http\Controllers\Ajax\Template\ResultUnkownEmailController@showFlaggedResultsOverview');
    }
}
