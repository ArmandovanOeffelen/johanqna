<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Model\Company;
use App\Model\CompanyLogo;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class CompanyLogoController extends Controller
{

    public function showForm(Company $company)
    {

        $companyLogo = CompanyLogo::where('company_id','=',$company->id)->firstOrFail();

        return view('admin.form.company.logo.create.create',[
            'company' => $company,
            'companyLogo' => $companyLogo
        ]);
    }

    public function uploadLogo(Request $request,Company $company)
    {

        $validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:jpeg,jpg,png|max:2048',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('admin.form.company.logo.create.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }


        $path = 'public/company/logo/';

        //5d65258e6df03
        $file = $request->file('logo');
        $logoname = 'logo-'. $company->name. '-'.uniqid().'.'.$file->getClientOriginalExtension();
        $file->storeAs($path,$logoname);

        $completePath = $path.$logoname;
        $companyLogo = CompanyLogo::updateOrCreate(['company_id' => $company->id],[
            'logo_path' => 'storage/company/logo/'.$logoname,
            'logo_name' => $logoname
        ]);


        session()->flash('success', 'The logo of ' . $company->name . ' is added or changed' );

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showSingleCompany',[
            'company' => $company
        ]);

    }
}
