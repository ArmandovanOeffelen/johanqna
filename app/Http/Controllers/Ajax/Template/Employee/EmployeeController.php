<?php

namespace App\Http\Controllers\Ajax\Template\Employee;

use App\Model\Range;
use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\RangeIndicatorHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function showText($range)
    {

        $range = Range::select('range_text_title', 'range_text')->where('id', $range)->first();

        return view('default.results.template.info_text', [
            'range' => $range
        ]);
    }

    public function showAdvice($result)
    {
        $advice = ResultPersonalAdvice::where('result_id', $result)->first();

        return view('default.results.template.info_advice', [
            'advice' => $advice
        ]);
    }

    public function showCompareForm()
    {
        $dates = Result::select('id', 'visit_date')->OrderBy('visit_date', 'ASC')->where('user_id', Auth::user()->id)->get();

        return view('default.form.compare.date-selector', [
            'dates' => $dates,
        ]);
    }

    public function ShowComparisons(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date_1' => 'required',
            'date_2' => 'required',
        ]);

        if ($validator->fails()) {
            $dates = Result::select('id', 'visit_date')->OrderBy('visit_date', 'ASC')->where('user_id', Auth::user()->id)->get();
            $request->flash();
            return response()->json(array(
                'html' => view('default.form.compare.date-selector', [
                    'dates' => $dates,
                ])->withErrors($validator)->render()
            ), 400);

        }

        $results = [];

        $results[] = Result::findOrFail($request->date_1);
        $results[] = Result::findOrFail($request->date_2);

        if (! is_null($request->date_3)) {
            $results[] = Result::findOrFail($request->date_3);
        }

        $resultsRangeData = array();
        foreach (Range::all() as $range) {
            foreach ($results as $index => $result) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);
                $resultsRangeData[$index][$range->column_name] = [
                    'visit_date' => $result->visit_date,
                    'result' => $result->id . ' (' . $result->email . ')',
                    'result_id' => $result->id,
                    'result_value' => $result->ResultValue,
                    'range' => $range->name,
                    'range_id' => $range->id,
                    'range_category' => $range->RangeCategory,
                    'range_column' => $range->column_name,
                    'personal_advice' => $result->ResultPersonalAdvice,
                    'indicator' => $rangeIndicator ? $rangeIndicator : null,
                    'indicator_color' => $rangeIndicator ? $rangeIndicator->RangeColor->color_code : null,
                    'icon' => $range->icon,

                ];
            }
        }

        return view('default.results.compare-results.template.index', [
            'results' => $results,
            'resultsRangeData' => $resultsRangeData
        ]);


    }
}
