<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Model\Range;
use App\Model\RangeText;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RangeTextController extends Controller
{
    public function index()
    {
        $ranges = Range::withCount('RangeText')->simplePaginate(10);
        return view('admin.screening_dashboard.range_config.range_config_texts.template.index',[
            'ranges' => $ranges
        ]);
    }

    public function showText(Range $range)
    {

        return view('admin.screening_dashboard.range_config.range_config_texts.show.index',[
            'range' => $range,
        ]);
    }
    public function showForm(Range $range)
    {

        return view('admin.form.screening_dashboard.range_config.range_text.create.index',[
            'range' => $range,
        ]);
    }

    public function showUpdateForm(Range $range)
    {

        return view('admin.form.screening_dashboard.range_config.range_text.update.index',[
            'range' => $range,
        ]);
    }
    public function create(Request $request,Range $range)
    {
        $validator = Validator::make($request->all(), [
            'range_text' => 'required',
            'range_title' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.screening_dashboard.range_config.range_text.create.index',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $rangeText = new RangeText();
        $rangeText->range_id = $range->id;
        $rangeText->title = $request->range_title;
        $rangeText->text = $request->range_text;
        $rangeText->save();

        session()->flash('succes',__('messages.succes_added',['model' => 'range text']));
        return app()->call('App\Http\Controllers\Ajax\Template\RangeTextController@index',[
        ]);
    }

    public function update(Request $request, Range $range)
    {
        $validator = Validator::make($request->all(), [
            'range_text' => 'required',
            'range_title' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return response()->json(array(
                'html' => view('admin.form.screening_dashboard.range_config.range_text.update.index',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $rangeText = RangeText::findOrFail($range->RangeText->id);
        $rangeText->range_id = $range->id;
        $rangeText->title = $request->range_title;
        $rangeText->text = $request->range_text;
        $rangeText->update();

        session()->flash('succes',__('messages.succes_updated',['model' => 'range text']));
        return app()->call('App\Http\Controllers\Ajax\Template\RangeTextController@index',[
        ]);
    }

    public function delete(Range $range)
    {

        $rangeTextId = $range->RangeText->id;

        $rangeText = RangeText::findOrFail($rangeTextId);
        $rangeText->delete();


        session()->flash('succes',__('messages.succes_deleted',['model' => 'range text']));
        return app()->call('App\Http\Controllers\Ajax\Template\RangeTextController@index',[
        ]);
    }
}
