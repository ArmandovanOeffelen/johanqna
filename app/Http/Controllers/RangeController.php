<?php

namespace App\Http\Controllers;

use App\Model\Range;
use App\Model\RangeCategory;
use App\Model\RangeCriteriaCollection;
use App\Model\RangeIndicator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class RangeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['is_admin']);
    }

    public function form(Range $range = null)
    {
        if (0 === count($rangeIndicators = RangeIndicator::all())) {
            return redirect()
                ->route('admin_controller_screening_show_ranges_overview')
                ->with("danger", "Voordat u een range config toevoegd moet u eerst ranges toevoegen.");
        }

        if (null === $range) {
            $range = new Range();
        }

        $rangeCategories = RangeCategory::all();

        return view('admin.form.screening_dashboard.range_config.create.index', [
            'rangeCategories' => $rangeCategories,
            'range' => $range,
            'rangeIndicators' => $rangeIndicators,
            'criteriaCollectionsData' => $this->criteriaCollectionsToArray($range)
        ]);
    }

    public function save(Request $request, Range $range = null)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:50',
            'column_name' => 'required|min:1|max:50',
            'range_category' => null === $range ? 'required' : '',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($validator);
        }

        $shouldCreate = null === $range;

        if (null === $range) {
            $range = new Range();
        }

        $range->name = $request->get('name');
        $range->column_name = $request->get('column_name');
        $range->range_text_title = $request->get('range_text_title');
        $range->range_text = $request->get('range_text');
        $range->icon = $request->get('icon');

        if ($shouldCreate) {
            $range->range_category_id = $request->get('range_category');
        }

        $range->save();

        return redirect()
            ->route('admin_range_form_update', [
                'range' => $range->id
            ])
            ->with('success', 'Changes were successfully saved.');
    }

    /**
     * @param Range $range
     *
     * @return array
     */
    private function criteriaCollectionsToArray(Range $range): array
    {
        $result = [];

        foreach ($range->criteriaCollections as $criteriaCollection) {
            $result[$criteriaCollection->range_indicator_id][] = $this->criteriaCollectionToArray($criteriaCollection);
        }

        return $result;
    }

    /**
     * @param RangeCriteriaCollection $criteriaCollection
     *
     * @return array
     */
    private function criteriaCollectionToArray(RangeCriteriaCollection $criteriaCollection): array
    {
        $result = [
            'id' => $criteriaCollection->id,
            'criteria' => []
        ];

        foreach ($criteriaCollection->rangeCriterias as $criteria) {
            $result['criteria'][] = [
                'type' => $criteria->type,
                'value' => $criteria->value,
            ];
        }

        return $result;
    }
}
