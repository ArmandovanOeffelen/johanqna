<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Model\Company;
use App\Model\Range;
use App\Model\RangeCategory;
use App\Model\RangeIndicator;
use App\Model\Result;
use App\RangeIndicatorHelper;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function foo\func;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        return view('admin.index');
    }

    public function showCompanyDashboard()
    {
        $company = Company::simplePaginate(10);
        return view('admin.company.overview.index',[
            'company' => $company,
        ]);
    }

    public function showScreeningDashboard()
    {

        return view('admin.screening_dashboard.index');
    }

    public function showRangeOverview()
    {
        $range = RangeIndicator::orderBy('rank', 'ASC')->simplePaginate(10);

        return view('admin.screening_dashboard.range.index',[
            'range' => $range
        ]);
    }

    public function showRangeConfigOverview()
    {

        $ranges = Range::simplePaginate(10);

        return view('admin.screening_dashboard.range_config.range_config.index',[
            'ranges' => $ranges
        ]);
    }

    public function showRangeConfigDashboard()
    {
        return view('admin.screening_dashboard.range_config.index');
    }

    public function showRangeConfigTextsOverview()
    {
        $ranges = Range::withCount('RangeText')->simplePaginate(10);
        return view('admin.screening_dashboard.range_config.range_config_texts.index',[
            'ranges' => $ranges
        ]);
    }


    public function showUserDasboard()
    {

        return view('admin.users.index');
    }

    public function showEmployeeDashboard()
    {
        $users = User::where('type','default')->simplePaginate(10);

        return response(view('admin.users.user.index',[
            'users' => $users
        ]));
    }

    public function showAdminDashboard()
    {
        $users = User::where('type','admin')->where('email','!=','info@rmnddesign.nl')->simplePaginate(10);

        return response(view('admin.users.admin.index',[
            'users' => $users
        ]));
    }


    public function showCoachDashboard()
    {

        $coaches = User::where('is_coach','1')->simplePaginate(10);

        return view('admin.users.coach.index',[
            'coaches' => $coaches
        ]);
    }

    public function showEmployee(User $user)
    {


        return view('admin.users.single_employee.index',[

            'user'=>$user
        ]);
    }

    public function showCompanyResults(Company $company,$year)
    {

        $start = date("$year-01-01");
        $end = date("$year-12-31");

        $ranges = Range::all();
        $results = Result::whereBetween('visit_date', [$start, $end])->where('company_id', $company->id)->get();

        if ($this->getCount($results) < 15) {

            session()->flash('warning',"Sorry there are less then 15 unique results found within the selected period. There for we can not show you the results");
            return view('admin.company.company.results.index',[
                'year' => $year,
                'company' => $company,
                'chartData' => null,
            ]);

        }

        $chartData = $this->getChartData($ranges->all(), $results->all());

        return view('admin.company.company.results.index', [
            'company' =>$company,
            'year' => $year,
            'chartData' => [
                'maxTicks' => count($results),
                'data' => $chartData
            ]
        ]);

    }

    /**
     * @param Collection $results
     *
     * @return int
     */
    private function getCount(Collection $results): int
    {
        return $results->map(function (Result $result) {
            return $result->user_id;
        })->values()->unique()->count();
    }

    /**
     * @param Range[] $ranges
     * @param Result[] $results
     *
     * @return array
     */
    private function getChartData(array $ranges, array $results)
    {
        $labels = [];
        $dataSets = [];

        foreach ($ranges as $range) {
            $labels[] = $range->name;
        }

        foreach ($results as $result) {
            foreach ($ranges as $rangeIndex => $range) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);

                if (null === $rangeIndicator) {
                    continue;
                }

                if (! isset($dataSets[$rangeIndicator->id])) {
                    $dataSets[$rangeIndicator->id] = [
                        'label' => $rangeIndicator->name,
                        'data' => array_map(function () {
                            return 0;
                        }, $ranges),
                        'backgroundColor' => $rangeIndicator->RangeColor->color_code,
                        'rank' => $rangeIndicator->rank
                    ];
                }

                $dataSets[$rangeIndicator->id]['data'][$rangeIndex]++;
            }
        }

        //TODO::sorting on rank testing.
        usort($dataSets,function ($a,$b){

            return $a['rank'] > $b['rank'];
        });

        return [
            'labels' => $labels,
            'datasets' => array_values($dataSets)
        ];
    }

    public function showAllResults(User $user)
    {
        $ranges = Range::all();

        $results = Result::where('email', $user->email)->orderBy('visit_date', 'ASC')->get();

        $allVisitDates = Result::select('visit_date')->where('email', $user->email)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();

        $surveyAnswers = Answer::all()->where('interviewee_id', $user->id);


        $answerDates = $surveyAnswers->map(function ($value, $key) {
            return $value->created_at;
        })->values()->unique();

        $groupByDate = [];

        foreach ($surveyAnswers as $item) {
            $groupByDate[$item->created_at->format('d-m-Y')][] = $item;
        }

        $rangeCategories = RangeCategory::all();


        $allResults = array();
        foreach ($results as $index => $result) {
            foreach ($ranges as $range) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);
                $allResults[$index][] = [
                    'visit_date' => $result->visit_date,
                    'result' => $result->id . ' (' . $result->email . ')',
                    'result_id' => $result->id,
                    'result_value' => $result->ResultValue,
                    'range' => $range->name,
                    'range_id' => $range->id,
                    'range_category' => $range->RangeCategory,
                    'range_column' => $range->column_name,
                    'personal_advice' => $result->ResultPersonalAdvice,
                    'indicator' => $rangeIndicator ? $rangeIndicator : null,
                    'indicator_color' => $rangeIndicator ? $rangeIndicator->RangeColor->color_code : null,
                    'icon' => $range->icon,

                ];
            }
        }

        return view('admin.users.single_employee.result.index', [
            'user' => $user,
            'allResults' => $allResults,
            'years' => $years,
            'answers' => $surveyAnswers,
            'groupByDate' => $groupByDate,
            'answerDates'=> $answerDates,
            'rangeCategories' => $rangeCategories
        ]);
    }


}
