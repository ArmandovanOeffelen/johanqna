<?php

namespace App\Http\Controllers;

use App\CompanyEmployeeInvite;
use App\Model\Company;
use App\Model\CoachInvite;
use App\User;
use Illuminate\Http\Request;

class ManagerEmployeeController extends Controller
{
    public function dashboard(Company $company)
    {
        $employees = User::where('company_id','=',$company->id)->where('is_company_manager','=',0)->simplePaginate(10);


        return view('manager.employee.index',[
            'employees' => $employees,
            'company' => $company
        ]);
    }


    public function showAllInvites(Company $company)
    {

        $invites = CompanyEmployeeInvite::where('invite_accepted','=',0)->where('company_id','=',$company->id)->simplePaginate(10);


        return view('manager.employee.invite.index',[
            'invites' => $invites,
            'company' => $company
        ]);
    }

}
