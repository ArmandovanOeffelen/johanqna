<?php

namespace App\Http\Controllers;

use App\CSV;
use Illuminate\Http\Request;

class CSVExportController extends Controller
{
    public function index()
    {
        $csvs =  CSV::latest('created_at')->simplePaginate(15);

        return view('survey.csv_overview.index',[
            'csvs' => $csvs
        ]);
    }

    public function downloadCSV(CSV $csv)
    {

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$csv->filename.".csv",
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];


        return \response()->download(storage_path("app/public/TempCSV/$csv->filename.csv"),$csv->filename.".csv",$headers);
    }
}
