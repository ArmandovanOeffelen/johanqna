<?php

namespace App\Http\Controllers;

use App\Model\Company;
use App\Model\CompanyManager;
use App\Model\CoachInvite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use RedirectsUsers;


class CompanyManagerInviteController extends Controller
{
    public function registrationForm(Company $company)
    {

        return view('auth.register_manager',[
            'company' => $company
        ]);
    }

    public function register(Request $request, Company $company)
    {

        $this->validator($request);
        if($this->checkIfInviteIsValid($request,$company) == false){

            session()->flash('warning','Invite is already accepted by someone else/You\'re not invited!');
            return response()->redirectToAction('CompanyManagerInviteController@registrationForm',[$company]);
        }

        $type = "manager";

        $user = $this->create($request,$company,$type);


        $this->guard()->login($user);

        session()->flash('succes','Hey '. $user->name .'! You are registered as a coach. You can now use the dashboard to navigate through the application');
        return response()->redirectToAction('CoachController@dashboard',['company' => $company]);
    }

    public function validator($request)
    {
        return $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function create($request,$company,$type)
    {
        return User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'type' => $type,
            'is_company_manager' => 1,
            'company_id' => $company->id,
            'password' => bcrypt($request['password']),
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    public function checkIfInviteIsValid($request,Company $company)
    {

        $invite = CoachInvite::where('company_id','=',$company->id)->get();
        if($invite->first()->email === $request['email']) {

            return true ?: false;

        }
        if($invite->first()->invite_accepted === 1){
            return false;
        }

    }
}
