<?php

namespace App\Http\Controllers;

use App\CompanyEmployeeInvite;
use App\Model\CoachInvite;
use App\Model\Company;
use App\Model\CompanyManager;
use App\Model\CompanyToken;
use App\SMSHelper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyManagerEmployeeInviteController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }


    public function registrationForm($companyToken, $token)
    {


        return view('auth.register_default', [
            'companyToken' => $companyToken,
            'token' => $token
        ]);
    }

    public function register(Request $request, $companyToken, $token)
    {

        //Getting invite
        $invite = $this->getInvite($token);
        //Getting token
        $this->validator($request);

        $phoneNumber = SMSHelper::trimPhoneNumber($request->phonenumber);

        $company = $this->findCompany($companyToken);

        //Checking token
        if ($this->tokenCheck($invite, $token) == false) {
            session()->flash('warning', 'Your token is not correct. If you have recieved an email with this registration link please contact us.');
            return response()->redirectToAction('CompanyManagerEmployeeInviteController@registrationForm', [$companyToken, $token]);
        }
        //checking email
        if ($this->emailCheck($invite, $request->email) === false) {
            session()->flash('warning', 'Your email is not correct. Please sign up with the email adress you were invited with.');
            return response()->redirectToAction('CompanyManagerEmployeeInviteController@registrationForm', [$companyToken, $token]);
        }
        //checking invite
        if ($this->inviteCheck($invite) == false) {
            session()->flash('warning', 'The invite you\'re trying to accept, has been accepted already. If this was you try to log in, if this was not you. Please contact us.');
            return response()->redirectToAction('CompanyManagerEmployeeInviteController@registrationForm', [$companyToken, $token]);
        }


        $user = $this->create($request,$company,$phoneNumber);

        $action = "register user via invite";
        if (SMSHelper::sendVerificationCode($user,$action) === true) {

            return view('auth.phone_verify.verify_default', [
                'companyToken' => $companyToken,
                'token' => $token,
                'user' => $user
            ]);
        } else {
            $request->flash();
            session()->flash('warning', 'something went horribly wrong.');
            return view('auth.register_default', [
                'companyToken' => $companyToken,
                'token' => $token
            ]);
        }

    }

    public function completeRegistration(Request $request,User $user, $companyToken, $token)
    {


        if (SMSHelper::compareCodes($user->code, $request->one_time_password) !== true) {
            session()->flash('danger', 'Your verification code is incorrect. Try again or request a new verification code.');
            return view('auth.phone_verify.verify_default', [
                'user' => $user,
                'token' => $token,
                'companyToken' => $companyToken,
                'error' => 'Your secret code is incorrect. Try again or request a new verification code.',
            ])->with('danger', 'Your secret code is incorrect. Try again or request a new verification code.');
        }
        $invite = $this->getInvite($token);

        $this->acceptInvite($invite);

        User::setUserActivated($user);

        if ($user->activated !== 1) {
            session()->flash('danger', 'Something went horribly wrong. Please contact the administrators of this system with the error code: acti-50062');
            return view('auth.phone_verify.verify', [
                'user' => $user,
                'error' => 'Your verification code is incorrect. Try again or request a new verification code.',
            ])->with('danger', 'Your verification code is incorrect. Try again or request a new verification code.');
        }


        $this->guard()->login($user);

        User::setUserLogged($user);

        session()->flash('succes','Succes! '. $user->name .' U have registered to BARABAZ');

        return response()->redirectToAction('DefaultController@dashboard');

    }

    public function validator($request)
    {

        $message = [
            'privacy.required' => 'You must agree with the Privacy Policy when signing up.'

        ];
        return $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phonetype' => 'required',
            'phonenumber' => 'required|digits_between:9,15',
            'privacy' => 'required'
        ],$message);
    }

    public function create($request, $company,$phoneNumber)
    {
        return User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'type' => 'default',
            'is_coach' => 0,
            'company_id' => $company->id,
            'google2fa_secret' => null,
            'password' => bcrypt($request['password']),
            'country_code' => $request['phonetype'],
            'phonenumber' => $phoneNumber,
            'activated' => 0,
            'code'  => null,
            'password' => bcrypt($request['password']),
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    public function requestNewCode(User $user,$companyToken,$token)
    {
        $action = "request new code user registration";
        if(SMSHelper::sendVerificationCode($user,$action) === true){

            session()->flash('success','you will recieve a new verification code shortly!');
            return view('auth.phone_verify.verify_default',[
                'companyToken' => $companyToken,
                'user' => $user,
                'token' => $token
            ]);
        } else {
            session()->flash('warning','something went horribly wrong.');
            return view('auth.register_default',[
                'companyToken' => $companyToken,
                'token' => $token
            ]);
        }
    }

    private function tokenCheck($invite, $token)
    {
        if ($invite->invite_token === $token) {

            return true;
        }

        return false;
    }

    private function inviteCheck($invite)
    {
        if ($invite->invite_accepted === 1) {
            return false;
        }
        return true;
    }

    private function emailCheck($invite, $email)
    {

        if ($invite->email === $email) {

            return true;
        }
        return false;
    }

    private function acceptInvite($invite): CompanyEmployeeInvite
    {

        $invite->invite_accepted = 1;
        $invite->update();
        return $invite;
    }

    protected function getInvite($token)
    {

        return $invite = CompanyEmployeeInvite::where('invite_token', $token)->first();
    }


    protected function findCompany($companyToken){
        $companyTokenCompany = CompanyToken::where('token',$companyToken)->first();

        $company = Company::findOrFail((int)$companyTokenCompany->company_id);
        return $company;
    }

}