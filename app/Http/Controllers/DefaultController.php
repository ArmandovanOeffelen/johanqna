<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Model\Company;
use App\Model\Range;
use App\Model\RangeCategory;
use App\Model\Result;
use App\Model\ResultPersonalAdvice;
use App\RangeIndicatorHelper;
use App\Survey;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use function foo\func;

class DefaultController extends Controller
{

    public function __construct()
    {
        $this->middleware( '2fa');
    }

    public function dashboard()
    {
        $resultCount = DefaultController::countResults(Auth::user());

        return view('default.index',[
            'resultCount' => $resultCount
        ]);
    }

    public function showSurvey()
    {
       
        $survey = Survey::all()->where('is_active', '=', 1);

        $surveyCompletedToday = Answer::whereDate('created_at', Carbon::today())->where('interviewee_id', Auth::user()->id)->count();
        if ($surveyCompletedToday !== 0) {
            $completed = 'completed';
            session()->flash('succes', __('messages.already_completed_today', ['model' => 'survey']));
        } else {
            $completed = 'false';
        }


        return view('default.survey.index', [
            'completed' => $completed,
            'survey' => $survey
        ]);
    }

    public function showResults($id)
    {
        $user = User::where('id', $id)->first();

        $ranges = Range::all();

        $results = Result::where('email', $user->email)->orderBy('visit_date', 'ASC')->get();

        $allVisitDates = Result::select('visit_date')->where('email', $user->email)->orderBy('visit_date', 'ASC')->get();
        $years = $allVisitDates->map(function ($value, $key) {
            return $value->visit_date->format('Y');
        })->values()->unique();
        $answersTest = $this->getAllSurveyAnswers();

        $groupByDate = [];

        foreach ($answersTest as $item) {
            $groupByDate[$item->created_at->format('d-m-Y')][] = $item;
        }

        $rangeCategories = RangeCategory::all();

        $allResults = array();
        foreach ($results as $index => $result) {
            foreach ($ranges as $range) {
                $rangeIndicator = RangeIndicatorHelper::getRangeIndicatorByResult($result, $range);
                $allResults[$index][] = [
                    'visit_date' => $result->visit_date,
                    'result' => $result->id . ' (' . $result->email . ')',
                    'result_id' => $result->id,
                    'result_value' => $result->ResultValue,
                    'range' => $range->name,
                    'range_id' => $range->id,
                    'range_category' => $range->RangeCategory,
                    'range_column' => $range->column_name,
                    'personal_advice' => $result->ResultPersonalAdvice,
                    'indicator' => $rangeIndicator ? $rangeIndicator : null,
                    'indicator_color' => $rangeIndicator ? $rangeIndicator->RangeColor->color_code : null,
                    'icon' => $range->icon,

                ];
            }
        }

        return view('default.results.index', [
            'groupByDate' => $groupByDate,
            'user' => $user,
            'allResults' => $allResults,
            'years' => $years,
            'answers' => $this->getAllSurveyAnswers(),
            'rangeCategories' => $rangeCategories
        ]);
    }

    protected function getAllSurveyAnswers()
    {
        $surveyAnswers = Answer::all()->where('interviewee_id', Auth::user()->id);

        return $surveyAnswers;
    }

    /**
     * @param User $user
     *
     * @return int
     */
    public static function countResults(User $user) :int
    {
        $resultCount = Result::where('user_id',$user->id)->count();

        return $resultCount;
    }

    public function showAppointment()
    {
        $company =  Company::where('id',Auth::user()->company_id)->first();

        return view('default.appointment.index',[
            'company' => $company
        ]);

    }
}