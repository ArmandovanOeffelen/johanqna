<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsProfileOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()){
            if (auth()->user()->id == $request->id) {

                return $next($request);
            }
        } else {

            return response(view('auth.login'));
        }

        session()->flash('info',__('messages.you_dont_belong_here',['user' => auth()->user()->name]) );
        return redirect()->back();
    }
}
