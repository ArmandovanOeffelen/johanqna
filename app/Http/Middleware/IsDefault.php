<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsDefault
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if(auth()->user()->isDefault() && auth()->user()->code_valid === 1) {
                return $next($request);
            }
            else {
                session()->flash('warning','You are not authenticated properly. Please fill in the verification code you just recieved to continue.');

                return response()->redirectToRoute('re_authenticate_show');
            }
        } else {
            return response(view('auth.login'));
        }

        session()->flash('info', 'Hey! You do not belong there.');
        //TODO::Detirmine what or where he was adn send him back.
        return response(view('default.content.index'));
    }
}
