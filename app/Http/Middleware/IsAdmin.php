<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    use AuthenticatesUsers {
        logout as performLogout;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if(auth()->user()->isAdmin() && auth()->user()->code_valid === 1) {
                return $next($request);
            }
            else {
                    session()->flash('warning','You are not authenticated properly. Please fill in the verification code you just recieved to continue.');
                    return response()->redirectToRoute('re_authenticate_show');
            }
        } else {
            return view('auth.login');
        }

        session()->flash('info',__('messages.you_dont_belong_here',['user' => auth()->user()->name]) );
        //TODO::Detirmine what or where he was adn send him back.
        return redirect()->back();
    }
}
