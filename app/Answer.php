<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'question_answered';

    protected $fillable = ['interviewee_id','answer','is_exported','question_id'];

    protected $dates = ['created_at'];

    public function Survey(){

        return $this->belongsTo(Survey::class,'question_id');
    }
}
