<?php

namespace App;

use App\Model\Range;
use App\Model\RangeCriteria;
use App\Model\RangeCriteriaCollection;
use App\Model\RangeIndicator;
use App\Model\Result;
use App\Model\ResultValue;

class RangeIndicatorHelper
{
    const GENDER_MAP = [
        'male' => [
            'mannelijk',
            'man',
            'male',
            'm'
        ],
        'female' => [
            'vrouwelijk',
            'vrouw',
            'v',
            'female',
            'f',
            'woman',
        ]
    ];

    const COLUMN_NAME_MAP = [
        'age' => [
            'leeftijd',
            'age'
        ],
        'gender' => [
            'geslacht',
            'gender',
        ]
    ];

    /**
     * Get the matching range indicator for a result.
     *
     * @param Result $result
     * @param Range $range
     *
     * @return RangeIndicator|null
     */
    public static function getRangeIndicatorByResult(Result $result, Range $range): ?RangeIndicator
    {
        foreach ($range->getCriteriaCollections() as $criteriaCollection) {
            if (self::matchesCriteriaCollection($result, $criteriaCollection, $range)) {
                return $criteriaCollection->getRangeIndicator();
            }
        }

        return null;
    }

    /**
     * @param Result $result
     * @param RangeCriteriaCollection $criteriaCollection
     * @param Range $range
     *
     * @return bool
     */
    public static function matchesCriteriaCollection(Result $result, RangeCriteriaCollection $criteriaCollection, Range $range): bool
    {
        foreach ($criteriaCollection->getAllCriteria() as $criteria) {
            $resultValue = null;

            if ($criteria->type === RangeCriteria::TYPE_SELF) {
                $resultValue = $result->getValue($range->column_name);
            } else {
                $columnNames = self::COLUMN_NAME_MAP[$criteria->type] ?? null;
                if (null === $columnNames) {
                    return false;
                }

                foreach ($columnNames as $columnName) {
                    if (null !== ($resultValue = $result->getValue($columnName))) {
                        break;
                    }
                }
            }

            if (! self::matchesValue($resultValue, $criteria)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ResultValue|null $resultValue
     * @param RangeCriteria $criteria
     *
     * @return bool
     */
    public static function matchesValue(?ResultValue $resultValue, RangeCriteria $criteria): bool
    {
        $value = null !== $resultValue ? $resultValue->value : null;
        $criteriaValue = $criteria->value;
        $criteriaType = $criteria->type;

        if (RangeCriteria::TYPE_GENDER === $criteriaType) {
            if ('any' === $criteriaValue) {
                return true;
            }

            if (null === $value) {
                return false;
            }

            if (! array_key_exists($criteriaValue, self::GENDER_MAP)) {
                return false;
            }

            return in_array(strtolower($value), self::GENDER_MAP[$criteriaValue], true);
        }

        $pieces = explode('-', $criteriaValue);
        if (count($pieces) !== 2) {
            return false;
        }

        $min = (float)$pieces[0];
        $max = (float)$pieces[1];

        if (0.0 === $min && 0.0 === $max) {
            return true;
        }

        if (null === $value) {
            return false;
        }

        return (float)$value >= $min && (float)$value <= $max;
    }
}
