<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    protected $table = 'question';

    use SoftDeletes;


    public function Answer()
    {
        return $this->hasMany(Answer::class);
    }
}
